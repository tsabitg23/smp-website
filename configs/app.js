let config = {
    dashboard_color : 'green',
    project_name : 'Sistem Management Project',
    apiUrl : 'http://localhost:6767/v1',
    appUrl : 'http://localhost:6768/',
    secret : '6e8a2402-9887-4964-9653-638e527e20c2',
    useCaptcha : true
};

if(window.location.href.includes("ardland")) {
	config.apiUrl = 'https://smp-backend.ardland.co.id/v1';
	config.appUrl = 'https://smp-test.ardland.co.id/'
}

if(window.location.href.includes("appsdps")) {
	config.apiUrl = 'https://smp-backend.appsdps.com/v1';
	config.appUrl = 'https://smp.appsdps.com/'
}

if(window.location.href.includes("localhost")){
	config.useCaptcha = false	
}

export const appConfig = {
	...config
};