export const constant = {
    ROLES : {
        ADMIN : '8a61b18f-6b53-45bb-973e-d68782935512',
        HRD : '7b975e63-fcbd-438c-a154-4b23848e22f1',
        MR : '59312132-14b6-4739-a9ea-dfda6688a1b4',
        SUPERVISOR : 'a7927fd2-9e09-4927-8d03-9bd6aac12dd8',
        ACCOUNTING : 'c03577cb-fe28-47a8-9323-8447d5e03c96',
        MARKETING : '2b58b458-b765-498a-8316-e76dacbc5653',
        SUPERADMIN : 'b110ce84-cc41-422f-86bd-94876fe06c6c',
        OWNER : '8e060aaa-a7c9-426d-b667-767b49d6337e'
    }
};