import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch, BrowserRouter,Redirect} from 'react-router-dom';
import Provider from 'react-redux-thunk-store';
import ExceptionHandler from './layouts/ExceptionHandler';
// import { GetBaseUrl } from './commons/commonFuncs';
import {LINKS} from 'routes/index';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
//Sample to import LESS or SCSS
//Style-sheets
import './assets/scss/material-dashboard-react.scss';
//import './assets/less/material-dashboard-react.less';
//Sample to import LESS or SCSS

import reducers from './reducers';
import indexRoutes from 'routes/index.jsx';

import {Provider as ProviderMOBX} from 'mobx-react';
// import * as firebase from "firebase";
import AppState from './stores/appstate';

//create store
const appstate = new AppState({
  token: localStorage.getItem('id_token') || '',
  userData: {
  }
});


// firebase.messaging().onMessage((payload) => {
//   console.log("Message received cok.", payload);
// });

//indicate whether application is running on PRD or not
const isPrd = process.env.NODE_ENV === 'production';

//Update for Reserved proxy
// const base = GetBaseUrl();
const hist = createBrowserHistory({ basename: '/' });

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) => appstate.token
            ? (<Component {...props}/>)
            : (<Redirect to={{pathname : '/login'}}/>
            )}/>
);

const createRouter = () => {
  return (
    <ProviderMOBX appstate={ appstate }>
      <BrowserRouter basename={'/'}>
        <Router history={hist}>
          <Switch>
              <Route exact path={'/'} render={props => (<Redirect
                  to={{
                      pathname: !!appstate.token ? LINKS.DASHBOARD : LINKS.LOGIN
                  }}/>)}/>
            {indexRoutes.map((prop, key) => {
                if(prop.type === 'private'){
                    return (
                        <PrivateRoute path={prop.path} component={prop.component} key={key} />
                    );
                }else{
                    return (
                        <Route path={prop.path} component={prop.component} key={key} />
                    );
                }

            })}
          </Switch>
        </Router>
      </BrowserRouter>
    </ProviderMOBX>
  );
};
const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#23a950'
        }
    },
    typography: {
        useNextVariants: true,
    },
});
//The Global ExceptionHandler will be disabled when running on development mode.
const renderComponent = () => {
  ReactDOM.render(
    <Provider reducers={reducers}>
        <MuiThemeProvider theme={theme}>
          <ExceptionHandler global disabled={!isPrd}>
            {createRouter()}
          </ExceptionHandler>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
  );
};

renderComponent();

/* devblock:start */
// Hot Module Replacement API
// if (module.hot) {
//   module.hot.accept('./layouts/Dashboard', () => {
//     renderComponent();
//   });
// }
if (module.hot) {
  module.hot.accept(renderComponent);
}
/* devblock:end */
