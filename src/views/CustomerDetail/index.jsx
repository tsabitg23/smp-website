import React from 'react';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import Fab from '@material-ui/core/Fab';
import ArrowBack from '@material-ui/icons/ArrowBack';
import PrintIcon from '@material-ui/icons/Print';
import {inject, observer} from 'mobx-react';
import {LINKS} from '../../routes/index';
import EditIcon from '@material-ui/icons/Edit';
import Table from '../../components/Table/index';
import TableHorizontal from '../../components/Table/TableHorizontal';
import Dialog from '../../components/Dialog/DialogOnly';
import {appConfig} from '../../../configs/app';
import moment from 'moment';
import CustomerForm from '../Customer/customerForm';
import {cloneDeep} from 'lodash'
import CustomerUnits from './CustomerUnits';

@inject('appstate')
@observer
export default class CustomerDetail extends React.Component{
	constructor(props){
        super(props);
        this.customerStore = props.appstate.customer;
        this.global_ui = props.appstate.global_ui;
        this.state = {
        	openEditDialog : false
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.customerStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
    }


	onClickBack = ()=>{
        this.props.history.push(LINKS.CUSTOMER);
    };

    get dataPribadi(){
        let column = [
            {
                key : 'name',
                label : 'Name',
                type : 'text'
            },
            {
                key : 'home_address',
                label : 'Alamat Rumah',
                type : 'text'
            },
            {
                key : 'home_status',
                label : 'Status Rumah',
                type : 'text'
            },
            {
                key : 'id_card_number',
                label : `Nomor ${get(this.customerStore.selectedData,'id_card_type','-')}`,
                type : 'text'
            },
            {
                key : 'birthplace+dob',
                label : 'Tempat, Tanggal Lahir',
                type : 'text',
                type2 : 'date'
            },
            {
                key : 'gender',
                label : 'Jenis Kelamin',
                type : 'text'
            },
            {
                key : 'marital_status',
                label : 'Status Perkawinan',
                type : 'text'
            },
            {
                key : 'religion',
                label : 'Agama',
                type : 'text'
            },
            {
                key : 'phones_data.home',
                label : 'No Telepon Rumah',
                type : 'text'
            },
            {
                key : 'phones_data.handphone',
                label : 'No HP',
                type : 'text'
            },
            {
                key : 'phones_data.whatsapp',
                label : 'No Whatsapp',
                type : 'text'
            },
            {
                key : 'email',
                label : 'Email',
                type : 'text'
            },
            {
                key : 'last_degree',
                label : 'Pendidikan Terakhir',
                type : 'text'
            },
        ];

        let data = cloneDeep(this.customerStore.selectedData);
        if(typeof data.phones_data === 'string'){
            data.phones_data = JSON.parse(data.phones_data);
        }
        return column.map(it=>{
            if(it.key.includes('+')){
                it.value = get(data,it.key.split('+')[0],'')+', '+moment(get(data,it.key.split('+')[1],'')).format('DD-MM-YYYY');
            }else{
                it.value = get(data,it.key,'') || "-";
            }
            return it;
        });
    };

    get dataPekerjaan(){
        let column = [
            {
                key : 'company_data.occupation',
                label : 'Pekerjaan',
                type : 'text'
            },
            {
                key : 'company_data.company_name',
                label : 'Nama Perusahaan',
                type : 'text'
            },
            {
                key : 'company_data.company_field',
                label : 'Bidang Usaha',
                type : 'text'
            },
            {
                key : 'company_data.company_address',
                label : 'Alamat Kantor',
                type : 'text'
            },
            {
                key : 'company_data.company_phone',
                label : 'No. Telepon',
                type : 'text'
            },
            {
                key : 'company_data.work_duration',
                label : 'Lama Bekerja',
                type : 'text'
            }
        ];
        let data = cloneDeep(this.customerStore.selectedData);
        if(typeof data.company_data === 'string'){
            data.company_data = JSON.parse(data.company_data);
        }
        return column.map(it=>{
            if(it.key.includes('+')){
                it.value = get(data,it.key.split('+')[0],'')+', '+moment(get(data,it.key.split('+')[1],'')).format('DD-MM-YYYY');
            }else{
                it.value = get(data,it.key,'') || "-";
            }
            return it;
        });
    };

    get dataKontak(){
        let column = [
            {
                key : 'contactable_data.name',
                label : 'Nama',
                type : 'text'
            },
            {
                key : 'contactable_data.relationship',
                label : 'Hubungan',
                type : 'text'
            },
            {
                key : 'contactable_data.phone_home',
                label : 'No Telepon Rumah',
                type : 'text'
            },
            {
                key : 'contactable_data.handphone',
                label : 'No Handphone',
                type : 'text'
            },
            {
                key : 'contactable_data.whatsapp',
                label : 'No Whatsapp',
                type : 'text'
            },
            {
                key : 'contactable_data.email',
                label : 'Email',
                type : 'text'
            },
            {
                key : 'contactable_data.mail_address',
                label : 'Alamat Surat Menyurat',
                type : 'text'
            },
            {
                key : 'contactable_data.address_status',
                label : 'Status Alamat',
                type : 'text'
            },
            {
                key : 'contactable_data.address',
                label : 'Alamat',
                type : 'text'
            },
        ];

        let data = cloneDeep(this.customerStore.selectedData);
        if(typeof data.contactable_data === 'string'){
            data.contactable_data = JSON.parse(data.contactable_data);
        }

        return column.map(it=>{
            if(it.key.includes('+')){
                it.value = get(data,it.key.split('+')[0],'')+', '+moment(get(data,it.key.split('+')[1],'')).format('DD-MM-YYYY');
            }else{
                it.value = get(data,it.key,'') || "-";
            }
            return it;
        });
    }

    get dataFinansial(){
        let column = [
            {
                key : 'financial_data.monthly_income',
                label : 'Pendapatan/Bulan',
                type : 'text'
            },
            {
                key : 'financial_data.spouse_monthly_income',
                label : 'Pendapatan Pasangan/Bulan',
                type : 'text'
            },
            {
                key : 'financial_data.monthly_expense',
                label : 'Pendapatan/Bulan',
                type : 'text'
            },
            {
                key : 'financial_data.other_installment_pay',
                label : 'Angsuran Lain. Ke',
                type : 'text'
            },
            {
                key : 'financial_data.other_installment_total',
                label : 'Angsuran Lain. Dari',
                type : 'text'
            },
            {
                key : 'financial_data.bank_name+financial_data.bank_account_number+financial_data.bank_on_behalf',
                label : 'Nomor Rekening',
                type : 'text',
                type2 : 'text'
            },
            {
                key : 'financial_data.dependents',
                label : 'Jumlah Tanggungan',
                type : 'text'
            }
        ];

        let data = cloneDeep(this.customerStore.selectedData);
        if(typeof data.financial_data === 'string'){
            data.financial_data = JSON.parse(data.financial_data);
        }

        return column.map(it=>{
            if(it.key.includes('+')){
                it.value = get(data,it.key.split('+')[0],'')+' '+get(data,it.key.split('+')[1],'')+ " a/n "+get(data,it.key.split('+')[2],'');
            }else{
                it.value = get(data,it.key,'') || "-";
            }
            return it;
        });
    }

    closeDialogEdit = async ()=>{
        this.global_ui.openLoader();
        await this.customerStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
        this.setState({openEditDialog : false})
    }

	render(){
		const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>,
            <Fab onClick={()=>this.setState({openEditDialog : true})} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <EditIcon />
            </Fab>
        ];

        let partOneHeader = [
            {
                key : 'category',
                label : "Category"
            },
            {
                key : 'value',
                label : "Score"
            },
            {
                key : 'desc',
                label : "Description"
            }
        ]

        let partOneData = get(this.customerStore.selectedData,'verification.scores[0].data',[]);

		return (
			<Grid container spacing={24}>
                <Grid item xs={12}>
	                <Dialog
	                    open={this.state.openEditDialog}
	                    title={'Edit Customer'}
	                    onClose={this.closeDialogEdit}
                    >
                        <CustomerForm onClose={()=>this.setState({openEditDialog : false})} openMode={'edit'} onSave={this.save} defaultValue={this.customerStore.selectedData}/>
	                </Dialog>
					<GenericHorizontal
					    title={`Customer ${get(this.customerStore.selectedData,'id','asd22i-sdjk').split('-')[0]}`}
					    subtitle={`detail customer data`}
                        caption={'Data Pribadi'}
					    data={this.dataPribadi}
					    tools={tools}
					>
						<div>
						    <h5>Data Pekerjaan</h5>
						    <TableHorizontal
						        data={this.dataPekerjaan}/>
                            <h5>Kontak Yang Bisa Dihubungi</h5>
                            <TableHorizontal
                                data={this.dataKontak}/>
                            <h5>Data Finansial</h5>
                            <TableHorizontal
                                data={this.dataFinansial}/>
                            {/*<Table
                                actionColumns={[]}
                                tableHeaderColor="primary"
                                tableHead={partOneHeader}
                                tableData={partOneData}   
                                pagination={false}
                            />
						    <h5>Pertanyaan Bagian 2</h5>
						    <TableHorizontal
						        data={this.partTwoSum}/>*/}
						</div>
					</GenericHorizontal>
                    <CustomerUnits customerId={this.props.match.params.id}/>
                </Grid>
            </Grid>
		)
	}
}