import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get, omit} from 'lodash';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class CustomerUnit extends React.Component{
    constructor(props){
        super(props);
        this.customerUnitStore = props.appstate.customer_unit;
        this.unitStore = props.appstate.unit;
        this.unitAkadStore = props.appstate.unit_akad_data;
        this.customerStore = props.appstate.customer;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.paymentTypeStore = props.appstate.payment_type;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            // project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('customers')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitStore.getAll();
        await this.paymentTypeStore.getAll();
        this.global_ui.closeLoader();
    }

    // async componentDidUpdate(prev){
    //     if(prev.appstate.project.selectedProject !== this.state.project_id){
    //         this.setState({project_id : prev.appstate.project.selectedProject});
    //         this.global_ui.openLoader();
    //         await this.customerUnitStore.getAll();
    //         this.global_ui.closeLoader();
    //     }
    // }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData, unit_akad_id : rowData.id});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.customerUnitStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.customerStore.getDetail(this.props.customerId);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.customerUnitStore.selectedData.permissions);
        // return;
        let reqData = omit(data,['id','unit','agent']);
        // let res = this.state.openMode === 'create' ? this.customerUnitStore.create(reqData) : this.customerUnitStore.update(reqData.id,reqData,false);
        // res.then(async res=>{
        //     this.onDialogClose();
        //     await this.customerStore.getDetail(this.props.customerId);
        //     this.global_ui.closeLoader();
        //     this.props.actions.showMessage(
        //         MessageBoxType.SUCCESS,
        //         `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
        //         ()=>console.log('OK Clicked')
        //     );
        // }).catch(err=>{
        //     this.global_ui.closeLoader();
        //     this.props.actions.showMessage(
        //         MessageBoxType.DANGER,
        //         err.message,
        //         ()=>console.log('OK Clicked')
        //     );
        // });

        let res = this.state.openMode === 'create' ? this.unitAkadStore.create(reqData) : this.unitAkadStore.update(this.state.unit_akad_id,reqData,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.customerStore.getDetail(this.props.customerId);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            // { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            // <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
            //     <Add />
            // </Fab>
        ];

        const unitData = get(this.customerStore.selectedData,'akad',[])

        let form_items = [
            {
                key : 'unit_id',
                label : 'Unit',
                type: 'autocomplete',
                items: this.unitStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.kavling_number
                    };
                }),
            },
            {
                key : 'payment_type_id',
                label : 'Payment Type',
                type: 'autocomplete',
                items: this.paymentTypeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                })
            },
            {
                key : 'downpayment',
                label : 'Downpayment (DP)',
                type: 'number'
            },
            {
                key : 'installment_per_month',
                label : 'Cicilan Perbulan',
                type: 'number'
            },
        ];

        return  (
            <div>
                {/*<Dialog*/}
                {/*    open={this.state.isDialogOpen}*/}
                {/*    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Unit`}*/}
                {/*    onClose={this.onDialogClose}*/}
                {/*    forms={form_items}*/}
                {/*    onSave={this.save}*/}
                {/*    value={this.state.defaultValue}*/}
                {/*/>*/}
                {/*<GenericPage*/}
                {/*    title={'Customer Unit'}*/}
                {/*    subtitle={'List of all unit own by customer'}*/}
                {/*    // project_id={this.props.appstate.project.selectedProject}*/}
                {/*    headerColumn={[*/}
                {/*        {*/}
                {/*            key : 'unit.kavling_number',*/}
                {/*            label : 'Unit',*/}
                {/*            type : 'text'   */}
                {/*        },*/}
                {/*        {*/}
                {/*            key : 'payment_type.name',*/}
                {/*            label : 'Payment type',*/}
                {/*            type : 'text'*/}
                {/*        },*/}
                {/*        {*/}
                {/*            key : 'downpayment',*/}
                {/*            label : 'Downpayment',*/}
                {/*            type : 'text'*/}
                {/*        },*/}
                {/*        {*/}
                {/*            key : 'installment_per_month',*/}
                {/*            label : 'Cicilan',*/}
                {/*            type : 'text'*/}
                {/*        },*/}
                {/*    ]}*/}
                {/*    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}*/}
                {/*    data={get(this.customerStore.selectedData,'units',[])}*/}
                {/*    tools={tools}*/}
                {/*    maxData={(get(this.customerStore.selectedData,'units',[])).length}*/}
                {/*/>*/}

                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Unit`}
                    onClose={this.onDialogClose}
                    forms={[
                        {
                            key : 'unit.kavling_number',
                            label : 'Unit',
                            type : 'text',
                            disabled : true,
                            required : false
                        },
                        {
                            key : 'unit.land_area',
                            label : 'Luas Tanah',
                            type : 'text',
                            disabled : true,
                            required : false
                        },
                        {
                            key : 'unit.building_area',
                            label : 'Luas Bangunan',
                            type : 'text',
                            disabled : true,
                            required : false
                        },
                        {
                            key : 'agent.name',
                            label : 'Nama Agent',
                            type : 'text',
                            disabled : true,
                            required : false
                        },
                        {
                            key : 'booking_date',
                            label : 'Booking Date',
                            type : 'date'
                        },
                        {
                            key : 'verification_date',
                            label : 'Verification Date',
                            type : 'date'
                        },
                        {
                            key : 'akad_date',
                            label : 'Akad Date',
                            type : 'date'
                        },
                        {
                            key : 'payment_method',
                            label : 'Skema Pembelian',
                            type: 'select',
                            items : [
                                {
                                    value : 'cash',
                                    label : 'Cash'
                                },
                                {
                                    value : 'cash_bertahap',
                                    label : 'Cash Bertahap'
                                },
                                {
                                    value : 'angsuran',
                                    label : 'Angsuran'
                                }
                            ],
                        },
                        {
                            key : 'downpayment',
                            label : 'Nilai DP',
                            type : 'number'
                        },{
                            key : 'tenor',
                            label : 'Tahun angsuran',
                            type : 'number'
                        }
                    ]}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <GenericPage
                    title={'Customer Unit'}
                    subtitle={'List of all unit own by customer'}
                    // project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key : 'unit.kavling_number',
                            label : 'Unit',
                            type : 'text'
                        },
                        {
                            key : 'unit.land_area',
                            label : 'Luas Tanah',
                            type : 'text'
                        },
                        {
                            key : 'unit.building_area',
                            label : 'Luas Bangunan',
                        },
                        {
                            key : 'agent.name',
                            label : 'Nama Agent',
                        },
                        {
                            key : 'booking_date',
                            label : 'Booking Date',
                            type : 'date'
                        },
                        {
                            key : 'verification_date',
                            label : 'Verification Date',
                            type : 'date'
                        },
                        {
                            key : 'akad_date',
                            label : 'Akad Date',
                            type : 'date',
                            required : false
                        },
                        {
                            key : 'payment_method',
                            label : 'Skema Pembelian',
                            type: 'select',
                        },
                        {
                            key : 'downpayment',
                            label : 'Nilai DP',
                            type : 'number',
                            required : false
                        },{
                            key : 'tenor',
                            label : 'Tahun angsuran',
                            type : 'number',
                            required : false
                        }
                    ]}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={unitData}
                    tools={tools}
                    maxData={unitData.length}
                    customWidth={'100vw'}
                />
            </div>
        );
    }

}

export default CustomerUnit;
