import React from 'react';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import Fab from '@material-ui/core/Fab';
import ArrowBack from '@material-ui/icons/ArrowBack';
import PrintIcon from '@material-ui/icons/Print';
import {inject, observer} from 'mobx-react';
import {LINKS} from '../../routes/index';
import EditIcon from '@material-ui/icons/Edit';
import Table from '../../components/Table/index';
import TableHorizontal from '../../components/Table/TableHorizontal';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import {appConfig} from '../../../configs/app';
import moment from 'moment';
import AssessementForm from '../Penilaian/form';
import {cloneDeep} from 'lodash'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { constant } from '../../../configs/const';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
export default class PenilaianDetail extends React.Component{
	constructor(props){
        super(props);
        this.employeeScoreStore = props.appstate.employee_score;
        this.assessmentStore = props.appstate.assessment_aspect;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openEditDialog : false,
            openEditCommentDialog: false
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.employeeScoreStore.getDetail(this.props.match.params.id);
        await this.assessmentStore.getAll();
        this.global_ui.closeLoader();
    }


	onClickBack = ()=>{
        this.props.history.push(LINKS.EMPLOYEE_SCORES);
    };

    get dataComment(){
        let column = [
            {
                key : 'employee_note',
                label : 'Komentar Karyawan',
                type : 'text'
            },
            {
                key : 'boss_note',
                label : 'Komentar Atasan Langsung',
                type : 'text'
            },
            {
                key : 'hrd_note',
                label : 'Tindakan HRD',
                type : 'text'
            }
        ];

        let data = this.employeeScoreStore.selectedData;
        return column.map(it=>{
            it.value = get(data,it.key,'') || "-";
            return it;
        });
    };

    get dataEmployee(){
        let column = [
            {
                key : 'employee.name',
                label : 'Nama',
                type : 'text'
            },
            {
                key : 'employee.position',
                label : 'Jabatan',
                type : 'text'
            },
            {
                key : 'employee.division',
                label : 'Departemen',
                type : 'text'
            },
            {
                key : 'periode_start+periode_end',
                label : 'Periode Penilaian',
                type : 'text',
                type2 : 'date'
            },
            {
                key : 'total_score',
                label : 'Total Nilai',
                type : 'text',
                labelBold : true,
                valueBold : true,
            },
        ];

        let data = this.employeeScoreStore.selectedData;
        return column.map(it=>{
            if(it.key.includes('+')){
                it.value = moment(get(data,it.key.split('+')[0],'')).format('DD-MM-YYYY')+' - '+moment(get(data,it.key.split('+')[1],'')).format('DD-MM-YYYY');
            }else{
                it.value = get(data,it.key,'') || "-";
            }
            return it;
        });
    };

    closeDialogEdit = async ()=>{
        this.global_ui.openLoader();
        await this.employeeScoreStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
        this.setState({openEditDialog : false})
    }

    saveComment = async (data)=>{
        this.global_ui.openLoader();
        let res = this.employeeScoreStore.update(data.id, data, false);
        res.then(async res=>{
            this.setState({
                openEditCommentDialog : false
            });
            await this.employeeScoreStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} project`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

	render(){
		const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>,
            <Fab onClick={()=>{this.setState({openEditDialog : true})}} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <EditIcon />
            </Fab>
        ];

        const commentTools = [
            <Fab onClick={()=>this.setState({openEditCommentDialog : true})} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <EditIcon />
            </Fab>
        ];

        let detailPenilaianHeader = [
            {
                key : 'number',
                label : "No"
            },
            {
                key : 'name',
                label : "Kriteria"
            },
            {
                key : 'score',
                label : "Score"
            }
        ]

        let detailPenilaianData = get(this.employeeScoreStore.selectedData,'question_list',[]);
        const allowedChangeComment = this.props.appstate.userData.role_id === constant.ROLES.MR;
        // console.log(this.props.appstate.userData);
        let form_comment = [
            {
                key : 'employee_note',
                label : 'Komentar Karyawan',
                type : 'multiline',
            },
            {
                key : 'boss_note',
                label : 'Komentar Atasan Langsung',
                type : 'multiline',
                disabled: !allowedChangeComment
            },
            {
                key : 'hrd_note',
                label : 'Tindakan HRD',
                type : 'multiline',
                disabled: !allowedChangeComment
            }
        ];

		return (
			<Grid container spacing={24}>
                <Grid item xs={12}>
                    <DialogForm
                        open={this.state.openEditCommentDialog}
                        title={'Edit Komentar'}
                        onClose={()=>this.setState({openEditCommentDialog: false})}
                        forms={form_comment}
                        onSave={this.saveComment}
                        value={this.employeeScoreStore.selectedData}
                    />
                    <Dialog
	                    open={this.state.openEditDialog}
	                    title={'Edit Penilaian'}
	                    onClose={this.closeDialogEdit}
                    >
                        <AssessementForm
                            onClose={()=>this.setState({openEditDialog : false})}
                            openMode={'edit'}
                            onSave={()=>{}}
                            defaultValue={this.employeeScoreStore.selectedData}/>
	                </Dialog>
					<GenericHorizontal
					    title={`Penilaian ${get(this.employeeScoreStore.selectedData,'id','asd22i-sdjk').split('-')[0]}`}
					    subtitle={`detail data penilaian`}
					    data={this.dataEmployee}
					    tools={tools}
					>
						<div>
						    <h5>Penilaian Kinerja Karyawan</h5>
                            <span>Kriteria Penilaian : 1 = Buruk, 2 = Sedang, 3 = Baik, 4 = Sangat Baik</span><br/>
                            <span>Range Pilihan Score hanya dari 1-4 setiap aspek</span>
						    <Table
                                actionColumns={[]}
                                tableHeaderColor="primary"
                                tableHead={detailPenilaianHeader}
                                tableData={detailPenilaianData}
                                pagination={false}
                                dataHeaderActive={true}
                                dataHeaderColor={"#a5c7ff"}
                            />
						</div>
					</GenericHorizontal>
                    <GenericHorizontal
					    title={'Tanggapan Dan Komentar'}
					    subtitle={`Komentar penilai dan karyawan`}
					    data={this.dataComment}
					    tools={commentTools}
					></GenericHorizontal>
                </Grid>
            </Grid>
		)
	}
}
