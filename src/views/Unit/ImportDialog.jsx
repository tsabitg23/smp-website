import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import CloudUpload from '@material-ui/icons/CloudUpload';
import CloudDownload from '@material-ui/icons/CloudDownload';
import classNames from 'classnames';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {inject, observer} from 'mobx-react';
import {appConfig} from '../../../configs/app';

const styles = theme => ({
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  flexCenter : {
  	display:'flex',justifyContent:'center',alignItems:'center'
  },
  fileName : {
  	borderRadius : 10,
  	backgroundColor : 'rgba(198, 198, 198,0.4)'
  },
  input : {
  	display : 'none'
  }
});

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class UnitImport extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			open : this.props.open,
			fileName : null,
			file : null
		}
		this.globalUI = props.appstate.global_ui;
		this.unitStore = props.appstate.unit;
	}

	componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    let change = {};
	    if (this.props.open !== prevProps.open) {
	      change.open = this.props.open;
	    }

	    if(Object.keys(change).length > 0){
	        this.setState(change);
	    }
	}

	downloadCSV = async ()=>{
		let tempLink = document.createElement('a');
        tempLink.href = appConfig.apiUrl + '/download_template/template_unit.csv';
        tempLink.setAttribute('download', 'template_unit.csv');
        tempLink.click();
	};

	
	onUploadFile = (event, index, value) => {
		const file = event.nativeEvent.target.files[0];
	    const allowedFile = ['csv'];

	    const [ext] = file.name.split('.').reverse();

	    if (!allowedFile.includes(ext.toLowerCase())) {
	      return this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'File type must be .csv',
                ()=>console.log('OK Clicked')
            );
	    }

	    this.setState({
	    	fileName : file.name,
	    	file : file
	    })

	};

	import = async ()=>{
		this.globalUI.openLoader();
		await this.unitStore.uploadFile(this.state.file).catch(err=>{
			return this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Error import',
                ()=>console.log('OK Clicked')
            );
		});
        this.props.actions.showMessage(
            MessageBoxType.SUCCESS,
            'Success import data',
            ()=>console.log('OK Clicked')
        );
		await this.unitStore.getAll();
		this.globalUI.closeLoader();
		this.props.handleClose();
	}

	render(){
		const {classes} = this.props;
		return (
			<Dialog
	          open={this.state.open}
	          onClose={this.props.handleClose}
	          aria-labelledby="dialog-import"
	        >
	          <DialogTitle id="dialog-import">Import Data</DialogTitle>
	          <DialogContent>
	            <DialogContentText>
	              To import unit data, download template csv file below and edit the file to fill the data according to format then upload it in form below and press import. 
	            </DialogContentText>
	            <Grid container spacing={24} className={classes.flexCenter} style={{marginTop : 10}}>
                    <Grid item xs={6}>
                    		<Button color="primary" aria-label="download" style={{textTransform: "none"}} onClick={this.downloadCSV}>
                    			<CloudDownload className={classes.extendedIcon}/>
                    			Download template_unit.csv
                    		</Button>
                    </Grid>
                    <Grid item xs={6}>
	                    		{/*<Button variant="contained" color="primary" aria-label="raised-button-file" style={{width:'100%'}}>
	                			<input type="file" style={{display : 'none'}} id="raised-button-file"/>
	                    			<CloudUpload className={classes.extendedIcon}/>
	                    			Upload file
	                    		</Button>*/}
	                    <input
	                        accept=".csv"
	                        className={classes.input}
	                        id="contained-button-file"
	                        type="file"
	                        onChange={this.onUploadFile}
	                      />
	                      <label htmlFor="contained-button-file">
	                        <Button variant="contained" component="span" color="primary" style={{width:'100%',textTransform: "none"}}>
	                          <CloudUpload className={classes.extendedIcon}/>
                    			Upload file
	                        </Button>
	                      </label>
                    </Grid>
                    <Grid item xs={12} className={classNames(classes.flexCenter,classes.fileName)}>
                    	{this.state.fileName || 'No file selected'}	
                    </Grid>
	            </Grid>
	          </DialogContent>
	          <DialogActions>
	            <Button onClick={this.props.handleClose} color="primary">
	              Cancel
	            </Button>
	            <Button onClick={this.import} color="primary">
	              Import
	            </Button>
	          </DialogActions>
	        </Dialog>
		)
	}
}

UnitImport.defaultProps = {
	open : false
};

export default withStyles(styles)(UnitImport);