import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import CloudUpload from '@material-ui/icons/CloudUpload';
import CloudDownload from '@material-ui/icons/CloudDownload';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import UnitImportDialog from './ImportDialog';
import TopStatus from '../../components/TopStatus';
import DefaultChart from '../../components/Chart';
import { appConfig } from '../../../configs/app';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Unit extends React.Component{
    constructor(props){
        super(props);
        this.unitStore = props.appstate.unit;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('units'),
            importDialogOpen : false,
            numberOfMonthChart : 4
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitStore.getAll();
        await this.unitStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.unitStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.unitStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
            this.global_ui.closeLoader();  
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onViewDetail = async (rowData)=>{
        // this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
        // alert(rowData)
        this.props.history.push(`/app/unit/${rowData.id}`);
        // console.log(rowData,'iniiii')
        // console.log(123213)
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this unit?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.unitStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.unitStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete agency',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.unitStore.selectedData.permissions);
        // return;

        let res = this.state.openMode === 'create' ? this.unitStore.create(data) : this.unitStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} unit`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    closeImportDialog = ()=>this.setState({importDialogOpen : false});
    openImportDialog = ()=>this.setState({importDialogOpen : true});
    downloadData = ()=>{
        this.global_ui.openLoader();
        this.unitStore.exportData().then(res=>{
            this.global_ui.closeLoader();
            let tempLink = document.createElement('a');
            tempLink.href = appConfig.apiUrl + '/download_template/'+ res.file;
            tempLink.setAttribute('download', res.file);
            tempLink.click();
        })
    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    refreshChart = async (value, closeLoader=true) => {
        this.global_ui.openLoader();
        await this.unitStore.getChart(value);
        this.global_ui.closeLoader();
    }

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'read'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.openImportDialog} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudUpload />
            </Fab>,
            <Fab onClick={this.downloadData} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudDownload />
            </Fab>,
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'kavling_number',
                label : 'No. Kavling',
                type: 'text'
            },
            {
                key : 'land_area',
                label : 'Luas tanah (m2)',
                type: 'number'
            },
            {
                key : 'building_area',
                label : 'Luas bangunan (m2)',
                type : 'number'
            },
            {
                key : 'status',
                label : 'Status',
                type : 'select',
                items : [
                    {
                        value : 'available',
                        label : 'Available'
                    },
                    {
                        value : 'booking_fee_paid',
                        label : 'Booking fee paid'
                    },
                    {
                        value : 'sold',
                        label : 'Sold'
                    }
                ]
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Unit",
            icon : "home",
            subtitle : "Total units",
            subtitle_icon : "assignment",
            value : this.unitStore.data.length
        }]

        const status_komisi = [
            {
                key : 'commission_status',
                label : 'Status Komisi',
                type : 'branch',
                branch : [{
                    value : null,
                    text : 'Belum Cair'
                },{
                    value : false,
                    text : 'Belum Cair'
                },{
                    value : true,
                    text : 'Cair'
                }]
            }
        ]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Unit`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <DefaultChart 
                    subtitle={'Grafik Data Unit'} 
                    changeFilter={this.refreshChart}
                    data={this.unitStore.chart}/>
                <GenericPage
                    title={'Unit'}
                    subtitle={'list of house unit'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items.concat(status_komisi)}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.unitStore.data}
                    tools={tools}
                />
                <UnitImportDialog
                    open={this.state.importDialogOpen}
                    handleClose={this.closeImportDialog}
                    />
            </div>
        );
    }

}

export default Unit;
