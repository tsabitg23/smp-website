import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';
import { LINKS } from '../../routes';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MarketingPlan extends React.Component{
    constructor(props){
        super(props);
        this.marketingStore = props.appstate.marketing_plan;
        this.employeeStore = props.appstate.employee;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('marketing_plans')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.employeeStore.setRequestQuery({show_all : true});
        await this.marketingStore.getAll();
        await this.employeeStore.getAll();
        this.global_ui.closeLoader();
    }

    // componentWillUnmount(){
    //     this.employeeStore.setRequestQuery({});
    // }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            this.employeeStore.setRequestQuery({show_all : true});
            await this.marketingStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.date = moment(data.date).format('YYYY-MM-DD');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        if(this.employeeStore.data.length == 0){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Employee data not found, please add employee data first before insert marketing plan',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this plan?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.marketingStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.marketingStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete plan',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        let res = this.state.openMode === 'create' ? this.marketingStore.create(data) : this.marketingStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.marketingStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} plan`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    onViewDetail = (rowData)=>{
        this.props.history.push(LINKS.MARKETING_PLAN + "/" + rowData.id);
    }
    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'free'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete'},
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'date',
                label : 'Date',
                type: 'date'
            },
            {
                key : 'type',
                label : 'Kegiatan',
                type: 'text'
            },
            {
                key : 'employee_id',
                label : 'PIC',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.name} - ${it.nik}`
                    };
                })
            },
            {
                key : 'place',
                label : 'Tempat',
                type: 'text'
            },
            {
                key : 'cost',
                label : 'Biaya teknis',
                type: 'number'
            },
            {
                key : 'status',
                label : 'Status',
                type : 'select',
                items : [
                    {
                        value : 'open',
                        label : 'Open'
                    },
                    {
                        value : 'close',
                        label : 'Close'
                    }
                ]
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Plan",
            icon : "description",
            subtitle : "Total plan this month",
            subtitle_icon : "assignment",
            value : this.marketingStore.data.filter(it=>moment(it.date).format('MM') === moment().format('MM')).length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Plan`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Marketing Plan'}
                    subtitle={'Marketing planning in the future'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key : 'date',
                            label : 'Date',
                            type: 'date'
                        },
                        {
                            key : 'type',
                            label : 'Kegiatan',
                            type: 'text'
                        },
                        {
                            key : 'employee.name',
                            label : 'PIC',
                            type: 'select'
                        },
                        {
                            key : 'place',
                            label : 'Tempat',
                            type: 'text'
                        },
                        {
                            key : 'cost',
                            label : 'Biaya teknis',
                            type: 'money'
                        },
                        {
                            key : 'employee.name',
                            label : 'PIC',
                            type: 'select'
                        },
                        {
                            key : 'status',
                            label : 'Status',
                            type : 'select'
                        },
                        {
                            key : 'manager_approval_status',
                            label : 'Approve Pimpro',
                            type : 'branch',
                            branch : [{
                                value : null,
                                text : 'Pending'
                            },{
                                value : "",
                                text : 'Pending'
                            },{
                                value : false,
                                text : 'Revisi'
                            },{
                                value : true,
                                text : 'Diterima'
                            }]
                        }
                ]}
                    actionColumn={configActionColumns.filter(it=>(it.key === 'free') ? true : this.state.permission[it.key])}
                    data={this.marketingStore.data}
                    tools={tools}
                    customWidth={'100vw'}
                />
            </div>
        );
    }

}

export default MarketingPlan;
