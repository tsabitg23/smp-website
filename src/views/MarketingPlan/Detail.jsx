import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../MRProjectJob/styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Edit from '@material-ui/icons/Edit';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import Typography from '@material-ui/core/Typography';
import FormBuilder from '../../components/FormBuilder';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MarketingPlanDetail extends React.Component {
    constructor(props){
        super(props);
        this.planStore = props.appstate.marketing_plan;
        this.employeeStore = props.appstate.employee;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            formErrors : [],
            defaultValue : {},
            dialogEditOpen : false,
            pimpro_manager_permission : {},
            permission : {},
            rejectedKey : null,
            formData : {
              desc : ''
            },
            openRejectDialog : false
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.employeeStore.setRequestQuery({show_all : true});
        await this.planStore.getDetail(this.props.match.params.id);
        this.setState({
            defaultValue : this.planStore.selectedData,
            pimpro_manager_permission : this.props.appstate.getPermission('manager_pimpro'),
            permission : this.props.appstate.getPermission('marketing_plans'),
        });
        this.global_ui.closeLoader();
    }

    componentWillUnmount(){
        this.employeeStore.setRequestQuery({});
    }

    getValueColor(status){
        if(status === 'approved' || status){
            return 'success';
        }
        else if(status === 'rejected' || status === false){
            return 'danger';
        }
        else{
            return '';
        }
    }

    get detailData(){

        let column = [
            {
                key : 'date',
                label : 'Date',
                type: 'date'
            },
            {
                key : 'type',
                label : 'Kegiatan',
                type: 'text'
            },
            {
                key : 'employee.name',
                label : 'PIC',
                type: 'select'
            },
            {
                key : 'place',
                label : 'Tempat',
                type: 'text'
            },
            {
                key : 'cost',
                label : 'Biaya teknis',
                type: 'money'
            },
            {
                key : 'employee.name',
                label : 'PIC',
                type: 'text'
            },
            {
                key : 'status',
                label : 'Status',
                type : 'text'
            },
            {
                key : 'manager_approval_status',
                label : 'Approve Pimpro',
                type : 'text',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.planStore.selectedData,'manager_approval_status',''))
            },
            {
                key : 'manager_desc',
                label : 'Keterangan Pimpro',
                type : 'text',
            },
        ];

        const managerApproval = get(this.planStore.selectedData,'manager_approval_status','');
        
        if(managerApproval !== false){
            column = column.filter(prop=>prop.key !== 'manager_desc');
        }
        return column.map(it=>{
            if(it.key.includes('status')){
                let val = it.value = get(this.planStore.selectedData,it.key,'');
                if(val){
                    it.value = 'Approved';
                } else if(val === false){
                    it.value = 'Revisi';
                } else if(val === null || val === ""){
                    it.value = 'Waiting';
                } 
            }
            else{
                it.value = get(this.planStore.selectedData,it.key,'');
            }
            return it;
        });
    }

    saveEdit =  (data)=>{
        this.global_ui.openLoader();
        let res = this.planStore.update(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.planStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.setState({
                dialogEditOpen : false
            })
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.setState({
                dialogEditOpen : false
            })
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.planStore.update(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.planStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }


    onClickBack = ()=>{
            this.props.history.push(LINKS.MARKETING_PLAN);
    };

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    onClickApprove = key=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to approved this plan?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.save({[key] : true});
                }
            }
        );
    };

    onClickReject = key => {
        this.setState({
            rejectedKey : key,
            openRejectDialog : true
        });
    };

    reject = () =>{
        this.setState({
            openRejectDialog: false
        });
        this.save({
            [this.state.rejectedKey+'_approval_status'] : false,
            [this.state.rejectedKey+'_desc'] : this.state.formData.desc
        });
    };

    closeDialogReject = ()=>this.setState({openRejectDialog : false});

    onDialogClose = ()=>this.setState({isDialogOpen : false});
    
    editClick = async (val) => {
        this.global_ui.openLoader();
        await this.employeeStore.getAll();
        this.setState({
            dialogEditOpen : true
        })
        this.global_ui.closeLoader();
    }

    render(){
        const {classes} = this.props;

        let form_edit = [
            {
                key : 'date',
                label : 'Date',
                type: 'date'
            },
            {
                key : 'type',
                label : 'Kegiatan',
                type: 'text'
            },
            {
                key : 'employee_id',
                label : 'PIC',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.name} - ${it.nik}`
                    };
                })
            },
            {
                key : 'place',
                label : 'Tempat',
                type: 'text'
            },
            {
                key : 'cost',
                label : 'Biaya teknis',
                type: 'number'
            },
            {
                key : 'status',
                label : 'Status',
                type : 'select',
                items : [
                    {
                        value : 'open',
                        label : 'Open'
                    },
                    {
                        value : 'close',
                        label : 'Close'
                    }
                ]
            }
        ];
        

        let tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];
        if(this.state.permission.update){
            tools = tools.concat([
                <Fab onClick={this.editClick} style={{display : 'inherit'}}  color="default" aria-label="Edit">
                    <Edit />
                </Fab>
            ])
        }

        let actions = [
            <Button color="inherit" onClick={this.reject}>
                save
            </Button>
        ];

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <GenericHorizontal
                            title={`Marketing Plan ${get(this.planStore.selectedData,'id','cbc6c7f0').split('-')[0]}`}
                            subtitle={'Rencana Kegiatan Marketing'}
                            data={this.detailData}
                            tools={tools}
                        >
                            <Grid container spacing={24} style={{marginTop:'20px'}}>
                            
                                {
                                    (this.state.pimpro_manager_permission.update && get(this.planStore.selectedData,'manager_approval_status','') !== true )  &&

                                    <Grid item xs={12} >
                                        <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                            Pimpinan Project
                                        </Typography>
                                        <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('manager_approval_status')} className={classes.buttonApprove}>
                                            Approve
                                        </Button>
                                        {
                                            (get(this.planStore.selectedData,'manager_approval_status','') === null) &&
                                            <Button color="secondary" onClick={()=>this.onClickReject('manager')} className={classes.button}>
                                                Reject
                                            </Button>
                                        }
                                    </Grid>
                                }
                            </Grid>
                        </GenericHorizontal>
                    </Grid>

                </Grid>
                <Dialog
                    open={this.state.openRejectDialog}
                    title={'Isi Revisi'}
                    onClose={this.closeDialogReject}
                    actions={actions}
                >
                    <FormBuilder forms={[{
                        key : 'desc',
                        label : 'Keterangan',
                        type : 'multiline',
                        placeholder : 'Alasan ditolak atau revisi'
                    }]} value={{}} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                </Dialog>
                <DialogForm
                    open={this.state.dialogEditOpen}
                    title={'Edit Marketing Plan'}
                    onClose={()=>this.setState({dialogEditOpen : false})}
                    forms={form_edit}
                    onSave={this.saveEdit}
                    value={this.state.defaultValue}
                />
            </div>
        );
    }
}

export default withStyles(styles)(MarketingPlanDetail);