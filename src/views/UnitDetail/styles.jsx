const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper : {
        padding : 20
    },
    changePasswordSave : {
        marginTop : '10px'
    }
});

export default styles;
