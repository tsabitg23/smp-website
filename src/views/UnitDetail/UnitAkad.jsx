import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class UnitAkad extends React.Component{
    constructor(props){
        super(props);
        this.unitAkadStore = props.appstate.unit_akad;
        this.agentStore = props.appstate.agent;
        this.unitStore = props.appstate.unit;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            // project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('units')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.agentStore.getAll();
        this.global_ui.closeLoader();
    }

    // async componentDidUpdate(prev){
    //     if(prev.appstate.project.selectedProject !== this.state.project_id){
    //         this.setState({project_id : prev.appstate.project.selectedProject});
    //         this.global_ui.openLoader();
    //         await this.unitAkadStore.getAll();
    //         this.global_ui.closeLoader();  
    //     }
    // }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data ?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.unitAkadStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.unitStore.getDetail(this.props.unitId);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.unitAkadStore.selectedData.permissions);
        // return;
        let reqData = data;
        if(!reqData.akad_date){
            reqData.akad_date = null
        }
        let res = this.state.openMode === 'create' ? this.unitAkadStore.create(reqData) : this.unitAkadStore.update(reqData.id,reqData);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitStore.getDetail(this.props.unitId);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'customer',
                label : 'Customer',
                type: 'text'
            },
            {
                key : 'agent_id',
                label : 'Agent',
                type: 'autocomplete',
                items: this.agentStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : false
            },
            {
                key : 'booking_date',
                label : 'Booking Date',
                type : 'date',
                required: false
            },
            {
                key : 'verification_date',
                label : 'Verification Date',
                type : 'date',
                required: false
            },
            {
                key : 'akad_date',
                label : 'Akad Date',
                type : 'date',
                required : false
            },{
                key : 'payment_method',
                label : 'Skema Pembelian',
                type: 'select',
                items : [
                    {
                        value : 'cash',
                        label : "Cash"
                    },
                    {
                        value : 'cash_bertahap',
                        label : "Cash Bertahap"
                    },
                    {
                        value : 'angsuran',
                        label : "Angsuran"
                    }
                ],
            },
            {
                key : 'downpayment',
                label : 'Nilai DP %(jika ada)',
                type : 'number',
                placeholder: 'contoh: 30',
                required : false
            },{
                key : 'tenor',
                label : 'Tahun angsuran (jika angsuran)',
                type : 'number',
                placeholder: 'contoh : 12',
                required : false
            }
        ];

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Verification`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <GenericPage
                    title={'Unit Verification'}
                    subtitle={'History of unit verification data and akad'}
                    // project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key : 'customer',
                            label : 'Customer',
                            type : 'text'
                        },
                        {
                            key : 'agent.name',
                            label : 'Agent',
                            type : 'text'   
                        },
                        {
                            key : 'booking_date',
                            label : 'Booking Date',
                            type : 'date'
                        },
                        {
                            key : 'verification_date',
                            label : 'Verification Date',
                            type : 'date'
                        },
                        {
                            key : 'akad_date',
                            label : 'Akad Date',
                            type : 'date',
                            required : false
                        },
                        {
                            key : 'payment_method',
                            label : 'Skema Pembelian',
                            type: 'select',
                        },
                        {
                            key : 'downpayment',
                            label : 'Nilai DP',
                            type : 'number',
                            required : false
                        },{
                            key : 'tenor',
                            label : 'Tahun angsuran',
                            type : 'number',
                            required : false
                        }
                    ]}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={get(this.unitStore.selectedData,'akad',[])}
                    tools={tools}
                    maxData={this.unitAkadStore.maxData}
                    customWidth={'100vw'}
                />
            </div>
        );
    }

}

export default UnitAkad;
