import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import UnitAkad from './UnitAkad';
import Edit from '@material-ui/icons/Edit';
import Dialog from '../../components/Dialog';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class UnitDetail extends React.Component {
    constructor(props){
        super(props);
        this.unitStore = props.appstate.unit;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            defaultValue : {}
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
    }

    get detailData(){
        let column = [
            {
                key : 'kavling_number',
                label : 'Kavling Number'
            },
            {
                key : 'land_area',
                label : 'Luas Tanah (m2)'
            },
            {
                key : 'building_area',
                label : 'Luas Bangunan (m2)'
            },
            {
                key : 'status',
                label : 'Status'
            },
        ];
        return column.map(it=>{
            it.value = get(this.unitStore.selectedData,it.key,'');
            return it;
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.unitStore.update(data.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitStore.getDetail(data.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit unit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }


    onClickBack = ()=>{
        this.props.history.push(LINKS.UNIT);
    };

    onClickEdit = ()=>{
        this.setState({isDialogOpen : true});
    };

    onDialogClose = ()=>this.setState({isDialogOpen : false});

    render(){
        const {classes} = this.props;

        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>,
            <Fab onClick={this.onClickEdit} style={{display : 'inherit'}}  color="default" aria-label="Edit">
                <Edit />
            </Fab>
        ];

        let form_items = [
            {
                key : 'kavling_number',
                label : 'No. Kavling',
                type: 'text'
            },
            {
                key : 'land_area',
                label : 'Luas tanah (m2)',
                type: 'number'
            },
            {
                key : 'building_area',
                label : 'Luas bangunan (m2)',
                type : 'number'
            },
            {
                key : 'status',
                label : 'Status',
                type : 'select',
                items : [
                    {
                        value : 'available',
                        label : 'Available'
                    },
                    {
                        value : 'booking_fee_paid',
                        label : 'Booking fee paid'
                    },
                    {
                        value : 'sold',
                        label : 'Sold'
                    }
                ]
            }
        ];

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Unit`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.unitStore.selectedData}
                />
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                            <GenericHorizontal
                                title={`Unit ${get(this.unitStore.selectedData,'kavling_number','x0')}`}
                                subtitle={`Detail information of unit ${get(this.unitStore.selectedData,'kavling_number','x0')}`}
                                data={this.detailData}
                                tools={tools}
                            />
                            <UnitAkad unitId={this.props.match.params.id}/>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(UnitDetail);