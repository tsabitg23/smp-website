import React from 'react';
import { constant } from '../../../configs/const';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import EventFormComponent from './EventFormComponent';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class EventsView extends React.Component{
    constructor(props){
        super(props);
        this.eventStore = props.appstate.events;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('events');
        if(this.userData.role_id === constant.ROLES.MR){
            permission.read = true;
        }
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : permission
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.eventStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.eventStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();  
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this event?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.eventStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.eventStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete event',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.eventStore.selectedData.permissions);
        // return;
        const reqBody = {
            ...data,
            ...this.eventStore.form
        };
        let res = this.state.openMode === 'create' ? this.eventStore.create(reqBody) : this.eventStore.update(data.id,reqBody);
        res.then(async res=>{
            this.onDialogClose();
            await this.eventStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} event`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    onViewDetail = async (rowData)=>{
        this.props.history.push('/app/events/'+rowData.id);
    };

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'read'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'name',
                label : 'Nama Event',
                type: 'text'
            },
            {
                key : 'date',
                label : 'Tanggal',
                type: 'date'
            },
            {
                key : 'place',
                label : 'Lokasi',
                type : 'text'
            }
        ];

        let table_header = [
            {
                key : 'name',
                label : 'Nama Event',
                type: 'text'
            },
            {
                key : 'date',
                label : 'Tanggal',
                type: 'date'
            },
            {
                key : 'place',
                label : 'Lokasi',
                type : 'text'
            },
            {
                key : 'total',
                label : 'Biaya Teknis',
                type : 'number'
            },
            {
                key : 'status',
                label : 'Approve Pimpro',
                type : 'text'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Event",
            icon : "event",
            subtitle : "Jumlah event",
            subtitle_icon : "assignment",
            value : this.eventStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Event`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                >
                    <EventFormComponent/>
                </Dialog>
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Events'}
                    subtitle={'Daftar event marketing'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={table_header}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.eventStore.data}
                    tools={tools}
                    maxData={this.eventStore.maxData}
                />
            </div>
        );
    }

}

export default EventsView;
