import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import TableInput from '../../components/TableInput';
import TableHorizontal from '../../components/Table/TableHorizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';

@inject('appstate')
@observer
export default class EventFormComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            crew : props.defaultCrew,
            funds : props.defaultFunds
        }
        this.eventStore = props.appstate.events;
    }

    changeText = (val, key) => {
        this.setState({
            [key] : val
        });
        this.eventStore.updateForm(key, val);
    };

    getTextarea = (key, placeholder) =>{
        return (
            <TextField
                id={key}
                value={get(this.state,key,undefined)}
                onChange={(e)=>this.changeText(e.target.value,key)}
                margin="normal"
                multiline
                fullWidth
                rows={4}
                required={false}
                placeholder={placeholder}
            />
        )
    }

    changeFunds = (data)=>{
        this.eventStore.updateForm('funds', data);
    }

    changeCrew = (data)=>{
        this.eventStore.updateForm('crew', data);
    }
    render(){
        const form_items = [
            {
                key : 'name',
                placeholder: 'Masukan nama barang*',
                label : 'Nama Barang',
                type: 'text'
            },
            {
                key : 'amount',
                placeholder: 'masukan jumlah biaya*',
                label : 'Biaya',
                type: 'number'
            },
            {
                key : 'desc',
                placeholder: 'masukan keterangan*',
                label : 'Keterangan',
                type: 'text'
            },
        ];

        const form_item_crew = [
            {
                key : 'name',
                placeholder: 'Masukan nama*',
                label : 'Nama Petugas',
                type: 'text'
            },
            {
                key : 'job',
                placeholder: 'Masukan tugas*',
                label : 'Tugas',
                type: 'text'
            },
            {
                key : 'desc',
                placeholder: 'masukan keterangan',
                label : 'Keterangan',
                type: 'text',
                required: false
            },
        ];
        return(
            <div style={{marginTop:20}}>
                <TableInput
                    title={'A. Biaya Teknis'}
                    form={form_items}
                    defaultData={this.props.defaultFunds}
                    readonly={this.props.readonly}
                    onChangeData={this.changeFunds}
                    />
                <div style={{marginBottom : 20}}/>
                <TableInput
                    title={'B. Petugas Acara'}
                    defaultData={this.props.defaultCrew}
                    readonly={this.props.readonly}
                    form={form_item_crew}
                    onChangeData={this.changeCrew}
                />
                <div style={{marginBottom : 20}}/>
                {
                    !this.props.hideEvaluation && (
                        <div>
                        <Typography variant="h6" id="tableTitle" style={{marginBottom:20}}>
                            Target dan Evaluasi
                        </Typography>
                        <TableHorizontal
                            data={[
                                {
                                    key: 'target',
                                    label : "Target Pencapaian",
                                    value : this.getTextarea('target', 'Masukan target pencapaian')
                                },
                                {
                                    key: 'achievement',
                                    label : "Pencapaian Aktual",
                                    value : this.getTextarea('achievement', 'Masukan Pencapaian aktual')
                                },
                                {
                                    key: 'evaluasi',
                                    label : "Evaluasi",
                                    value : this.getTextarea('evaluation', 'Masukan Evaluasi')
                                },
                            ]}
                        />
                        </div>
                    )
                }
            </div>
        )
    }
}

EventFormComponent.defaultProps = {
    hideEvaluation : false,
    defaultFunds : [],
    defaultCrew : [],
    readonly: false
}