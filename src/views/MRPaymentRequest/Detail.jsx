import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../MRProjectJob/styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Edit from '@material-ui/icons/Edit';
import Dialog from '../../components/Dialog';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MRPaymentRequestDetail extends React.Component {
    constructor(props){
        super(props);
        this.projectPaymentRequestStore = props.appstate.project_payment_request;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            defaultValue : {},
            permission : props.appstate.getPermission('approve_project_payment_request')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.projectPaymentRequestStore.getDetail(this.props.match.params.id,{['task.status'] : status});
        this.global_ui.closeLoader();
    }

    getValueColor(status){
        if(status === 'approved'){
            return 'success'
        }
        else if(status === 'rejected'){
            return 'danger'
        }
        else{
            return ''
        }
    }

    get detailData(){
        
        let column = [
            {
                key : 'name',
                label : 'Kebutuhan',
                type: 'text'
            },
            {
                key : 'deadline',
                label : 'Batas maximal pembayaran',
                type: 'date'
            },
            {
                key : 'description',
                label : 'Keterangan',
                type: 'multiline'
            },
            {
                key : 'task.status',
                label : 'Status',
                type : 'select',
                labelBold : true,
                valueBold : true,
                valueColor : this.getValueColor(get(this.projectPaymentRequestStore.selectedData,'task.status',''))
            }
        ];
        return column.map(it=>{
            if(it.key.includes('status')){
                let val = it.value = get(this.projectPaymentRequestStore.selectedData,it.key,'');
                it.value = val === 'created' ? 'Waiting' : val;
            }
            else{
                it.value = get(this.projectPaymentRequestStore.selectedData,it.key,'');
            }
            return it;
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.projectPaymentRequestStore.updateTask(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.projectPaymentRequestStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit unit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }


    onClickBack = ()=>{
        if(this.state.permission.create){
            this.props.history.push(LINKS.MR_PROJECT_PAYMENT_REQUEST);
        }
        else{
            this.props.history.push(LINKS.PROJECT_PAYMENT_REQUEST);
        }
    };

    onClickSave = status=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            `Are you sure you want to ${status === 'approved' ? 'approve' : 'reject'} this data?`,
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.save({['task.status'] : status});
                }
            }
        );
    };

    onDialogClose = ()=>this.setState({isDialogOpen : false});

    render(){
        const {classes} = this.props;

        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                            <GenericHorizontal
                                title={`Pembiayaaan Project ${get(this.projectPaymentRequestStore.selectedData,'id','cbc6c7f0-564c-493a-ae16-2e0cbedf70a6').split('-')[0]}`}
                                subtitle={`Permintaan pembiayaan project untuk ditinjau`}
                                data={this.detailData}
                                tools={tools}
                            >
                            {
                                (this.state.permission.update && get(this.projectPaymentRequestStore.selectedData,'task.status','-')) === "created" && 
                                <Grid container spacing={24} style={{marginTop:'20px'}}>
                                    <Grid item xs={12} >
                                        <Button variant="contained" color="primary" onClick={()=>this.onClickSave("approved")} className={classes.buttonApprove}>
                                            Approve
                                        </Button>
                                        <Button color="secondary" onClick={()=>this.onClickSave("rejected")} className={classes.button}>
                                            Reject
                                        </Button>
                                    </Grid>
                                </Grid>
                            }
                            </GenericHorizontal>
                    </Grid>
                    
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(MRPaymentRequestDetail);