import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class NotificationPage extends React.Component{
    constructor(props){
        super(props);
        this.notifStore = props.appstate.notification;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.notifStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.notifStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();
        }
    }

    onViewDetail = (rowData)=>{
        this.notifStore.read(rowData.notification_id);
        this.props.history.push(this.props.appstate.notification.getRouteDestination(rowData.notification.type,rowData.notification.data))
    }



    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View', Color: 'primary', Callback: this.onViewDetail, SecondaryColor : 'gray', useSecondaryColor : ['read_at',true]}
        ];

        const tools = [];

        let form_items = [
            {
                key : 'notification.title',
                label : 'Notif',
                type: 'text'
            },
            {
                key : 'notification.description',
                label : 'Description',
                type : 'text'
            },
            {
                key : 'created_at',
                label : 'Tanggal',
                type : 'date'
            }

        ];

        let topStatusData = [{
            type : 'count',
            title : "Unread notif",
            icon : "notifications",
            subtitle : "New notification that hasn't been read",
            subtitle_icon : "assignment",
            value : this.notifStore.data.filter(it=>!it.read_at).length
        }]

        return  (
            <div><TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Notification'}
                    subtitle={'all your notification'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items}
                    actionColumn={configActionColumns}
                    data={this.notifStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default NotificationPage;