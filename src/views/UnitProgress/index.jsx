import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class UnitProgress extends React.Component{
    constructor(props){
        super(props);
        this.unitProgressStore = props.appstate.unit_progress;
        this.unitStore = props.appstate.unit;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('unit_progress')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitProgressStore.getAll();
        await this.unitStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            this.unitStore.setRequestQuery({show_all : true});
            await this.unitProgressStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.unitStore.getAll();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.date = moment(data.date).format('YYYY-MM-DD');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        if(this.unitStore.data.length == 0){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Unit data not found, please add unit data first before insert this data',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.unitProgressStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.unitProgressStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        let res = this.state.openMode === 'create' ? this.unitProgressStore.create(data) : this.unitProgressStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitProgressStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete'},
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'unit_id',
                label : 'Nomor Kavling',
                type: 'autocomplete',
                items: this.unitStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.kavling_number}`
                    };
                })
            },
            {
                key : 'percentage',
                label : 'Persentase Progress (%)',
                type: 'percentage'
            },
            {
                key : 'description',
                label : 'Deskripsi',
                type: 'multiline'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Progres Unit",
            icon : "trending_up",
            subtitle : "Jumlah data dari progres unit",
            subtitle_icon : "assignment",
            value : this.unitProgressStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} progress`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Unit Progress'}
                    subtitle={'Data progress pengerjaan unit'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key: 'no',
                            label : 'No',
                            type: 'text'
                        },
                        {
                            key : 'unit.kavling_number',
                            label : 'No Kavling',
                            type: 'text'
                        },
                        {
                            key : 'percentage',
                            label : 'Persentase (%)',
                            type: 'text'
                        },
                        {
                            key : 'description',
                            label : 'Deskripsi',
                            type: 'text'
                        }
                ]}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.unitProgressStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default UnitProgress;