import React from 'react';
import { constant } from '../../../configs/const';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class CommissionView extends React.Component{
    constructor(props){
        super(props);
        this.commissionStore = props.appstate.commission;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('komisi');
        
        
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : permission
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.commissionStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.commissionStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();  
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this event?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.commissionStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.commissionStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete event',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.commissionStore.selectedData.permissions);
        // return;
        const reqBody = {
            ...data,
            ...this.commissionStore.form
        };
        let res = this.state.openMode === 'create' ? this.commissionStore.create(reqBody) : this.commissionStore.update(data.id,reqBody);
        res.then(async res=>{
            this.onDialogClose();
            await this.commissionStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} event`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    onViewDetail = async (rowData)=>{
        this.props.history.push('/app/events/'+rowData.id);
    };

    render(){
        const configActionColumns = [
        ];

        const tools = [
        ];

        let form_items = [
            {
                key : 'agent.name',
                label : 'Agent',
                type: 'text'
            },
            {
                key : 'agent.agency.name',
                label : 'Agensi',
                type: 'text'
            },
            {
                key : 'unit_commission_approved',
                label : 'Unit Komisi Cair',
                type : 'text'
            },
            {
                key : 'unit_commission_pending',
                label : 'Unit Komisi Pending',
                type : 'text'
            },
            {
                key : 'total_commission_approved',
                label : 'Total Komisi Cair',
                type : 'money'
            },
            {
                key : 'total_commission_pending',
                label : 'Total Komisi pending',
                type : 'money'
            }
        ];


        let topStatusData = [{
            type : 'count',
            title : "Komisi",
            icon : "attach_money",
            subtitle : "Jumlah agent penerima komisi",
            subtitle_icon : "assignment",
            value : this.commissionStore.data.length
        }]

        return  (
            <div>
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Komisi'}
                    subtitle={'Daftar komisi dari agen yang unit nya telah akad'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.commissionStore.data}
                    tools={tools}
                    maxData={this.commissionStore.maxData}
                />
            </div>
        );
    }

}

export default CommissionView;
