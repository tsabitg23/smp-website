import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Projects extends React.Component{
    constructor(props){
        super(props);
        this.projectStore = props.appstate.project;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            permission : props.appstate.getPermission('projects')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.projectStore.getAll();
        this.global_ui.closeLoader();
    }

    // onEditClick = async (rowData) => {
        // console.log(rowData);

    // };

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this project?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.projectStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.projectStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete project',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.projectStore.selectedData.permissions);
        // return;

        let res = this.state.openMode === 'create' ? this.projectStore.create(data) : this.projectStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.projectStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} project`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick, key : 'update' },
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick, key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'name',
                label : 'Name',
                type: 'text'
            },
            {
                key : 'location',
                label : 'Location',
                type: 'text'
            },
        ];

        let topStatusData = [{
            type : 'count',
            title : "Projects",
            icon : "work",
            subtitle : "Total project",
            subtitle_icon : "assignment",
            value : this.projectStore.data.length
        }]

        return  (
            <div>
                <Dialog 
                    open={this.state.isDialogOpen} 
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Project`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Projects'}
                    subtitle={'Your current projects'}
                    headerColumn={[
                        {label : 'Name',key : 'name'},
                        {label : 'Location',key : 'location'}]
                    }
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.projectStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default Projects;
