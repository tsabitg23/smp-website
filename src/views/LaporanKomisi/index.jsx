import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import CloudUpload from '@material-ui/icons/CloudUpload';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import DefaultChart from '../../components/Chart';
import * as moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class CommissionReport extends React.Component{
    constructor(props){
        super(props);
        this.commissionReportStore = props.appstate.commission_report;
        this.commissionRequestStore = props.appstate.commission_request;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            importDialogOpen: false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission(['komisi','manager_marketing']),
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.commissionRequestStore.setRequestQuery({show_all : true, distribution_status : false});
        await this.commissionReportStore.getAll();
        await this.commissionRequestStore.getAll();
        this.global_ui.closeLoader();
    }

    componentWillUnmount(){
        this.commissionRequestStore.setRequestQuery({});
    }
    
    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.commissionReportStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.commissionRequestStore.getAll();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.survei_date = moment(data.survei_date).format('YYYY-MM-DD');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this entry?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.commissionReportStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.commissionReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete entry',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.commissionReportStore.selectedData.permissions);
        // return;

        let res = this.state.openMode === 'create' ? this.commissionReportStore.create(data) : this.commissionReportStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.commissionReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} entry guest`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});


    closeImportDialog = ()=>{
        this.setState({
            importDialogOpen : false
        })
    }

    openImportDialog = ()=>{
        this.setState({
            importDialogOpen : true
        })
    };

    onChangeAgentData = async (val)=>{
        this.global_ui.openLoader();
        // await this.unitStore.getUnitByAgentIdKomisi(val);
        this.global_ui.closeLoader();
    }

    onViewDetail = async (rowData)=>{
        this.props.history.push('/app/laporan_komisi/'+rowData.id);
    };

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'free'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            // <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
            //     <Add />
            // </Fab>
        ];

        let table_header = [
            {
                key : 'commission_request.agent.name',
                label : 'Nama Agent',
                type : 'text'
            },
            {
                key : 'commission_request.agent.agency.name',
                label : 'Nama Agency',
                type: 'text'
            },
            {
                key : 'commission_request.unit.kavling_number',
                label : 'Unit',
                type : 'text'
            },
            {
                key : 'commission_request.amount',
                label : 'Total Komisi',
                type: 'money'
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type : 'text'
            },
            {
                key : 'marketing_approval_status',
                label : 'Approve Manager Marketing',
                type : 'branch',
                branch : [{
                    value : null,
                    text : 'Pending'
                },{
                    value : false,
                    text : 'Revisi'
                },{
                    value : true,
                    text : 'Diterima'
                }]
            },
            {
                key : 'finance_approval_status',
                label : 'Approve Manager Keuangan',
                type : 'branch',
                branch : [{
                    value : null,
                    text : 'Pending'
                },{
                    value : false,
                    text : 'Revisi'
                },{
                    value : true,
                    text : 'Diterima'
                }]
            },
        ];

        let form_items = [
            {
                key : 'commission_request_id',
                label : 'No Pengajuan',
                type: 'autocomplete',
                items: this.commissionRequestStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.code,
                        agent_name : (it.agent) ? it.agent.name : '-',
                        agency_name : (it.agent.agency) ? it.agent.agency.name : '-',
                        unit_number : (it.unit) ? it.unit.kavling_number : '-',
                        amount : (it.amount) ? it.amount : '-',
                        code : (it.code) ? it.code : '-'
                    };
                }),
                // empty_key : ['unit_id','unit_id-autocomplete'],
                // onChange : this.onChangeAgentData
            },
            {
                key : 'code',
                label : 'Nama Pengajuan',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'kode pengajuan',
                data_key : 'code'
            },
            {
                key : 'agent_id',
                label : 'Agent',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Nama Agent',
                data_key : 'agent_name'
            },
            {
                key : 'agency_id',
                label : 'Agency',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Nama Agency',
                data_key : 'agency_name'
            },
            {
                key : 'amount',
                label : 'Total Nilai',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Total nilai komisi',
                data_key : 'amount'
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type : 'multiline'
            },
        ];

        let topStatusData = [{
            type : 'count',
            title : "Laporan Pendistribusian",
            icon : "book",
            subtitle : "Total jumlah laporan",
            subtitle_icon : "assigment",
            value : this.commissionReportStore.data.length
        }];

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Laporan`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Laporan Pendistribusian Komisi'}
                    subtitle={'daftar laporan pendistribusian komisi'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[{key:'no',label:'No',type:'text'}].concat(table_header)}
                    actionColumn={configActionColumns.filter(it=>(it.key === 'free') ? true : this.state.permission[it.key])}
                    data={this.commissionReportStore.data.map((prop, index)=>{
                        return {
                            ...prop,
                            no: index+1
                        }
                    })}
                    tools={tools}
                    customWidth={'100vw'}
                />
            </div>
        );
    }

}

export default CommissionReport;