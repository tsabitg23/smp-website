import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import {get} from 'lodash';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {inject, observer} from 'mobx-react';
import FormBuilder from '../../components/FormBuilder';
import {appConfig} from '../../../configs/app';
import classNames from 'classnames';
import FileCopy from '@material-ui/icons/FileCopy';
const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    flexCenter : {
        display:'flex',justifyContent:'center',alignItems:'center'
    },
    fileName : {
        borderRadius : 10,
        backgroundColor : 'rgba(198, 198, 198,0.4)'
    },
    input : {
        display : 'none'
    },
    link : {
        color : '#6772e5',
    }
});

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class EmailDialog extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            open : this.props.open,
            fileName : null,
            file : null,
            formErrors : [],
            formData : {}
        };
        this.globalUI = props.appstate.global_ui;
        this.unitVerifyStore = props.appstate.unit_verification;
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        let change = {};
        if (this.props.open !== prevProps.open) {
            change.open = this.props.open;
        }

        if(Object.keys(change).length > 0){
            this.setState(change);
        }
    }

    sendEmail = async ()=>{
        // if(this.state.formData.email && this.state.formData.email){
        //
        // }
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(get(this.state.formData,'email_to',''))){
            this.globalUI.openLoader();
            await this.unitVerifyStore.sendEmail(this.props.id,this.state.formData.email_to).catch(err=>{
                return this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    err,
                    ()=>console.log('OK Clicked')
                );
            });
            await this.unitVerifyStore.getAll();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success send email',
                ()=>console.log('OK Clicked')
            );
            this.globalUI.closeLoader();
            this.props.handleClose();
        }
        else{
            return this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Email not valid',
                ()=>console.log('OK Clicked')
            );
        }
    };

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    get getLink(){
        return appConfig.appUrl+'verification_user/'+this.props.id
    }

    copyLink = ()=>{
        navigator.clipboard.writeText(this.getLink);
    }

    render(){
        const {classes} = this.props;
        let form_items = [
            {
                key : 'email_to',
                label : 'Email',
                type: 'email'
            }
        ];
        return (
            <Dialog
                open={this.state.open}
                onClose={this.props.handleClose}
                aria-labelledby="dialog-import"
            >
                <DialogTitle id="dialog-import">Link Verification</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Enter email in the form below, the verification data from customer will be send to that email after customer done filling the form.
                    </DialogContentText>
                    <Grid container spacing={24} className={classes.flexCenter}>
                        <Grid item xs={12}>
                            <FormBuilder forms={form_items} value={this.props.defaultValue} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                        </Grid>
                        <Grid item xs={8} className={classNames(classes.flexCenter,classes.fileName,classes.link)}>
                            {this.getLink} 
                        </Grid>
                        <Grid item xs={4}>
                            <Button color="primary" variant="contained" aria-label="copy" style={{textTransform: "none"}} fullWidth onClick={this.copyLink}>
                                <FileCopy className={classes.extendedIcon}/>
                                Copy
                            </Button>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.sendEmail} color="primary">
                        Send
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

EmailDialog.defaultProps = {
    open : false,
    defaultValue : {}
};

export default withStyles(styles)(EmailDialog);