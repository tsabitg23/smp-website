import React from 'react';
import GenericPage from '../../components/GenericPage';
import Send from '@material-ui/icons/Send';
import Button from '@material-ui/core/Button';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import EmailDialog from './emailDialog';
import TopStatus from '../../components/TopStatus';
import Dialog from '../../components/Dialog';
import {appConfig} from '../../../configs/app';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class UnitVerification extends React.Component{
    constructor(props){
        super(props);
        this.unitVerifyStore = props.appstate.unit_verification;
        this.userData = props.appstate.userData;
        this.agentStore = props.appstate.agent;
        this.global_ui = props.appstate.global_ui;
        this.unitStore = props.appstate.unit;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            selectedId : null,
            defaultValue : {},
            project_id : this.userData.selected_project,
            openSendDialog : false,
            permission : props.appstate.getPermission('customer_verification')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitVerifyStore.getAll();
        await this.agentStore.getAll();
        await this.unitStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.unitVerifyStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();
        }
    }

    // onEditClick = async (rowData)=>{
    //     this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    // };

    onSendClick = async (rowData)=>{
        this.setState({openSendDialog : true,selectedId : rowData.id,defaultValue : {email_to : rowData.email_to}});
    };

    onViewDetail = async (rowData)=>{
      this.props.history.push('/app/customer_verify/'+rowData.id);
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
        // window.open(appConfig.appUrl+'verification_user/new', "_blank")
    };
    //
    // onDeleteClick = rowData => {
    //     this.props.actions.showMessage(
    //         MessageBoxType.CONFIRM,
    //         'Are you sure you want to delete this data ?',
    //         (event)=>{
    //             const target = event.currentTarget || event.target;
    //             if(target.value === 'OK'){
    //                 this.delete(rowData.id);
    //             }
    //         }
    //     );
    // };
    //
    // delete = async (id)=>{
    //     this.global_ui.openLoader();
    //     this.unitAkadStore.delete(id).then(async res=>{
    //         this.onDialogClose();
    //         await this.unitVerifyStore.getDetail(this.props.unitId);
    //         this.global_ui.closeLoader();
    //         this.props.actions.showMessage(
    //             MessageBoxType.SUCCESS,
    //             'Success delete data',
    //             ()=>console.log('OK Clicked')
    //         );
    //     }).catch(err=>{
    //         this.global_ui.closeLoader();
    //         this.props.actions.showMessage(
    //             MessageBoxType.DANGER,
    //             err.message,
    //             ()=>console.log('OK Clicked')
    //         );
    //     });
    // };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.unitAkadStore.selectedData.permissions);
        // return;
        let reqData = data;
        if(!reqData.akad_date){
            reqData.akad_date = null
        }
        let res = this.state.openMode === 'create' ? this.unitVerifyStore.saveNewData(reqData) : this.unitAkadStore.update(reqData.id,reqData);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitVerifyStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    
    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});
    handleDialogSend = ()=>this.setState({openSendDialog : false});
    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'read'},
            { Icon: Send, Tooltip: 'Send email', Color: 'primary', Callback: this.onSendClick ,key : 'create'},
        ];
        // const configActionColumns = [];

        const tools = [
            <Button onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  variant="fab" color="default" aria-label="Add">
                <Add />
            </Button>
        ];
        // const tools = [];

        let form_items = [
            {
                key : 'name',
                label : 'Customer',
                type: 'text'
            },
            {
                key : 'unit_id',
                label : 'Unit',
                type: 'autocomplete',
                items: this.unitStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.kavling_number
                    };
                }),
                required : true
            },
            {
                key : 'agent_id',
                label : 'Agent',
                type: 'autocomplete',
                items: this.agentStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : false
            },
            {
                key : 'booking_date',
                label : 'Booking Date',
                type : 'date'
            },
            {
                key : 'verification_date',
                label : 'Verification Date',
                type : 'date'
            },
            {
                key : 'akad_date',
                label : 'Akad Date',
                type : 'date',
                required : false
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Verification",
            icon : "assignment_turned_in",
            subtitle : "Total verification",
            subtitle_icon : "assignment",
            value : this.unitVerifyStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Verification`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Customer Verification'}
                    subtitle={'Schedule of customer verification date'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key : 'customer',
                            label : 'Customer',
                            type : 'text'
                        },
                        {
                            key : 'unit.kavling_number',
                            label : 'Unit Kavling NO',
                            type : 'text'
                        },
                        {
                            key : 'unit.status',
                            label : 'Unit Status',
                            type : 'text'
                        },
                        {
                            key : 'agent.name',
                            label : 'Agent',
                            type : 'text'
                        },
                        {
                            key : 'booking_date',
                            label : 'Booking Date',
                            type : 'date'
                        },
                        {
                            key : 'verification_date',
                            label : 'Verification Date',
                            type : 'date'
                        },
                        {
                            key : 'akad_date',
                            label : 'Akad Date',
                            type : 'date',
                            required : false
                        }
                    ]}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.unitVerifyStore.data}
                    tools={tools}
                    maxData={this.unitVerifyStore.maxData}
                />
                <EmailDialog defaultValue={this.state.defaultValue} open={this.state.openSendDialog} handleClose={this.handleDialogSend} id={this.state.selectedId}/>
            </div>
        );
    }

}

export default UnitVerification;
