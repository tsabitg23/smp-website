import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import CloudUpload from '@material-ui/icons/CloudUpload';
import CloudDownload from '@material-ui/icons/CloudDownload';
import classNames from 'classnames';
import { connect } from 'react-redux';
import {appConfig} from '../../../configs/app';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {inject, observer} from 'mobx-react';

const styles = theme => ({
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  flexCenter : {
  	display:'flex',justifyContent:'center',alignItems:'center'
  },
  fileName : {
  	borderRadius : 10,
  	backgroundColor : 'rgba(198, 198, 198,0.4)'
  },
  input : {
  	display : 'none'
  }
});

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class GuestBookImport extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			open : this.props.open,
			fileName : null,
			file : null
		}
		this.globalUI = props.appstate.global_ui;
		this.fileStore = props.appstate.file;
		this.guestBookStore = props.appstate.guest_book;
	}

	componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    let change = {};
	    if (this.props.open !== prevProps.open) {
	      change.open = this.props.open;
	    }

	    if(Object.keys(change).length > 0){
	        this.setState(change);
	    }
	}

	downloadCSV = async ()=>{
        let tempLink = document.createElement('a');
        tempLink.href = appConfig.apiUrl + '/download_template/template_guest_book.xlsx';
        tempLink.setAttribute('download', 'template_guest_book.xlsx');
        tempLink.click();
	};

	
	onUploadFile = (event, index, value) => {
		const file = event.nativeEvent.target.files[0];
	    const allowedFile = ['xlsx','xls'];

	    const [ext] = file.name.split('.').reverse();

	    if (!allowedFile.includes(ext.toLowerCase())) {
	      return this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'File type must be excel file',
                ()=>console.log('OK Clicked')
            );
	    }

	    this.setState({
	    	fileName : file.name,
	    	file : file
	    })

	};

	import = async ()=>{
		this.globalUI.openLoader();
		return this.guestBookStore.uploadFile(this.state.file).then(async res=>{
			if(res.message === 'success'){
				this.props.actions.showMessage(
					MessageBoxType.SUCCESS,
					'Success import data',
					()=>console.log('OK Clicked')
				);
				await this.guestBookStore.getAll();
				this.globalUI.closeLoader();
				this.props.handleClose();
			} else {
				throw res;
			}
		}).catch(err=>{
			this.globalUI.closeLoader();
			return this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Error import file',
                ()=>console.log('OK Clicked')
            );
		});
	}

	render(){
		const {classes} = this.props;
		return (
			<Dialog
	          open={this.state.open}
	          onClose={this.props.handleClose}
	          aria-labelledby="dialog-import"
	        >
	          <DialogTitle id="dialog-import">Import Data</DialogTitle>
	          <DialogContent>
	            <DialogContentText>
	              untuk import data, download contoh template berikut kemudian isi data sesuai contoh kemudian upload kembali. 
	            </DialogContentText>
	            <Grid container spacing={24} className={classes.flexCenter} style={{marginTop : 10}}>
                    <Grid item xs={6}>
                    		<Button color="primary" aria-label="download" style={{textTransform: "none"}} onClick={this.downloadCSV}>
                    			<CloudDownload className={classes.extendedIcon}/>
                    			Download template_guest_book.xlsx
                    		</Button>
                    </Grid>
                    <Grid item xs={6}>
	                    		{/*<Button variant="contained" color="primary" aria-label="raised-button-file" style={{width:'100%'}}>
	                			<input type="file" style={{display : 'none'}} id="raised-button-file"/>
	                    			<CloudUpload className={classes.extendedIcon}/>
	                    			Upload file
	                    		</Button>*/}
	                    <input
	                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
	                        className={classes.input}
	                        id="contained-button-file"
	                        type="file"
	                        onChange={this.onUploadFile}
	                      />
	                      <label htmlFor="contained-button-file">
	                        <Button variant="contained" component="span" color="primary" style={{width:'100%',textTransform: "none"}}>
	                          <CloudUpload className={classes.extendedIcon}/>
                    			Upload file
	                        </Button>
	                      </label>
                    </Grid>
                    <Grid item xs={12} className={classNames(classes.flexCenter,classes.fileName)}>
                    	{this.state.fileName || 'No file selected'}	
                    </Grid>
	            </Grid>
	          </DialogContent>
	          <DialogActions>
	            <Button onClick={this.props.handleClose} color="primary">
	              Cancel
	            </Button>
	            <Button onClick={this.import} color="primary">
	              Import
	            </Button>
	          </DialogActions>
	        </Dialog>
		)
	}
}

GuestBookImport.defaultProps = {
	open : false
};

export default withStyles(styles)(GuestBookImport);