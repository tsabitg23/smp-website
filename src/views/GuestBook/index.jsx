import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import CloudUpload from '@material-ui/icons/CloudUpload';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import DefaultChart from '../../components/Chart';
import * as moment from 'moment';
import ImportDialog from './ImportData';
import { appConfig } from '../../../configs/app';
import CloudDownload from '@material-ui/icons/CloudDownload';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class GuestBook extends React.Component{
    constructor(props){
        super(props);
        this.guestBookStore = props.appstate.guest_book;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            importDialogOpen: false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('guest_book'),
            numberOfMonthChart : 4
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.guestBookStore.getAll();
        await this.guestBookStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
        this.global_ui.closeLoader();
    }
    
    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.guestBookStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.guestBookStore.getChart();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.survei_date = moment(data.survei_date).format('YYYY-MM-DD');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this entry?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.guestBookStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.guestBookStore.getAll();
            await this.guestBookStore.getChart();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete entry',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (dataReturn)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.guestBookStore.selectedData.permissions);
        // return;
        const data = dataReturn;
        if(!data.survei_date){
            data.survei_date = null;
        }
        let res = this.state.openMode === 'create' ? this.guestBookStore.create(data) : this.guestBookStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.guestBookStore.getAll();
            await this.guestBookStore.getChart();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} entry guest`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    refreshChart = async (value) => {
        this.global_ui.openLoader();
        await this.guestBookStore.getChart(value);
        this.global_ui.closeLoader();
    }

    closeImportDialog = ()=>{
        this.setState({
            importDialogOpen : false
        })
    }

    openImportDialog = ()=>{
        this.setState({
            importDialogOpen : true
        })
    }

    downloadData = ()=>{
        this.global_ui.openLoader();
        this.guestBookStore.exportData().then(res=>{
            this.global_ui.closeLoader();
            let tempLink = document.createElement('a');
            tempLink.href = appConfig.apiUrl + '/download_template/'+ res.file;
            tempLink.setAttribute('download', res.file);
            tempLink.click();
        })
    };

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.openImportDialog} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudUpload />
            </Fab>,
            <Fab onClick={this.downloadData} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudDownload />
            </Fab>,
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'created_at',
                label : 'Tanggal Kedatangan',
                type : 'date'
            },
            {
                key : 'name',
                label : 'Nama Tamu',
                type: 'text'
            },
            {
                key : 'phone',
                label : 'No Telepon',
                type : 'number'
            },
            {
                key : 'address',
                label : 'Domisili',
                type: 'multiline'
            },
            {
                key : 'survei_date',
                label : 'Rencana Survei',
                type : 'date',
                required : false
            },
            {
                key : 'info_source',
                label : 'Sumber info',
                type : 'select',
                items : [
                    {
                        value : 'agent',
                        label : 'Agent'
                    },
                    {
                        value : 'social_media',
                        label : 'Sosial Media'
                    },
                    {
                        value : 'walk_in',
                        label : 'Walk in'
                    },
                    {
                        value : 'baliho',
                        label : 'Baliho kantor/lokasi'
                    },
                    {
                        value : 'event',
                        label : 'Event survey serentak / gathering konsumen'
                    },
                    {
                        value : 'marketplace',
                        label : 'Marketplace'
                    },
                    {
                        value : 'lain_lain',
                        label : 'Lain lain'
                    },
                ]
            }

        ];

        let topStatusData = [{
            type : 'count',
            title : "Guest",
            icon : "book",
            subtitle : "This month guest",
            subtitle_icon : "date_range",
            value : this.guestBookStore.data.filter(it=>moment(it.created_at).format('MM') === moment().format('MM')).length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Entry Guest`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                {/* <TopStatus data={topStatusData}/> */}
                <DefaultChart 
                    subtitle={'Grafik Jumlah Tamu'} 
                    changeFilter={(value)=>this.refreshChart(value)}
                    data={this.guestBookStore.chart}/>
                <GenericPage
                    title={'Guest Book'}
                    subtitle={'list of your guest'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[{key:'no',label:'No',type:'text'}].concat(form_items)}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.guestBookStore.data.map((prop, index)=>{
                        return {
                            ...prop,
                            no: index+1
                        }
                    })}
                    tools={tools}
                />
                <ImportDialog
                    open={this.state.importDialogOpen}
                    handleClose={this.closeImportDialog}
                    />
            </div>
        );
    }

}

export default GuestBook;