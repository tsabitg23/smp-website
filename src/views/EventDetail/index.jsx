import React from 'react';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import Fab from '@material-ui/core/Fab';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {inject, observer} from 'mobx-react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {LINKS} from '../../routes/index';
import EditIcon from '@material-ui/icons/Edit';
import Table from '../../components/Table/index';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import FormBuilder from '../../components/FormBuilder';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { constant } from '../../../configs/const';
import EventFormComponent from '../Events/EventFormComponent';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
export default class EventDetail extends React.Component{
	constructor(props){
        super(props);
        this.eventStore = props.appstate.events;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('events');
        this.state = {
            pimpro_manager_permission : {},
            permission : permission,
            openEditDialog : false,
            openRejectDialog : false,
            formErrors : [],
            rejectedKey : null,
            formData : {
                desc : ''
            },
            openEditCommentDialog: false
        };
        
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.eventStore.getDetail(this.props.match.params.id);
        this.setState({
            pimpro_manager_permission : this.props.appstate.getPermission('manager_pimpro'),
        })
        this.global_ui.closeLoader();
    }


	onClickBack = ()=>{
        this.props.history.push(LINKS.EVENTS);
    };

    get dataEvaluasi(){
        let column = [
            {
                key : 'target',
                label : 'Target',
                type : 'text'
            },
            {
                key : 'achievement',
                label : 'Pencapaian Aktual',
                type : 'text'
            },
            {
                key : 'evaluation',
                label : 'Evaluasi',
                type : 'text'
            }
        ];

        let data = this.eventStore.selectedData;
        return column.map(it=>{
            it.value = get(data,it.key,'') || "-";
            return it;
        });
    };

    getValueColor(status){
        if(status === 'approved'){
            return 'success'
        }
        else if(status === 'rejected'){
            return 'danger'
        }
        else{
            return ''
        }
    }

    get dataEvent(){
        let column = [
            {
                key : 'name',
                label : 'Nama Event',
                type : 'text'
            },
            {
                key : 'date',
                label : 'Tanggal',
                type : 'date'
            },
            {
                key : 'place',
                label : 'Lokasi',
                type : 'text'
            },
            {
                key : 'total',
                label : 'Total Biaya',
                type : 'money'
            },
            {
                key : 'status',
                label : 'Approve Pimpro',
                type : 'select',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.eventStore.selectedData,'status',''))
            },
            {
                key : 'desc',
                label : 'Keterangan Pimpro',
                type : 'text'
            },
        ];

        let data = this.eventStore.selectedData;
        return column.map(it=>{
            if(it.key == 'status' && data[it.key] == 'created') {
                it.value = 'waiting'
                return it;
            };
            it.value = get(data,it.key,'') || "-";
            return it;
        });
    };

    closeDialogEdit = async ()=>{
        this.global_ui.openLoader();
        await this.eventStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
        this.setState({openEditDialog : false})
    }

    saveComment = async (data)=>{
        this.global_ui.openLoader();
        let res = this.eventStore.update(data.id, {
            ...this.eventStore.selectedData,
            ...data
        }, false);
        res.then(async res=>{
            this.setState({
                openEditCommentDialog : false,
                openEditDialog : false
            });
            await this.eventStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} project`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    onClickApprove = key=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to approved this request?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.saveComment({[key] : 'approved', id: this.props.match.params.id});
                }
            }
        );
    };

    onClickReject = key => {
        this.setState({
            rejectedKey : key,
            openRejectDialog : true
        });
    };

    reject = () =>{
        this.setState({
            openRejectDialog: false
        });
        this.saveComment({
            [this.state.rejectedKey] : 'rejected',
            desc : this.state.formData.desc,
            id : this.props.match.params.id
        });
    };

    closeDialogReject = ()=>this.setState({openRejectDialog : false});

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){
		const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];
        
        if(this.state.permission.update){
            tools.push(
                <Fab onClick={()=>{this.setState({openEditDialog : true})}} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                    <EditIcon />
                </Fab>
            )
        }

        const commentTools = this.state.permission.update ? [
            <Fab onClick={()=>this.setState({openEditCommentDialog : true})} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <EditIcon />
            </Fab>
        ] : [];

        let actions = [
            <Button color="inherit" onClick={this.reject}>
                save
            </Button>
        ];

        let dataFundsColumn = [
            {
                key : 'number',
                label : "No"
            },
            {
                key : 'name',
                label : "Nama"
            },
            {
                key : 'amount',
                label : "Biaya"
            },
            {
                key : 'desc',
                label : "Keterangan"
            }
        ];

        let dataCrewColumn = [
            {
                key : 'number',
                label : "No"
            },
            {
                key : 'name',
                label : "Nama"
            },
            {
                key : 'job',
                label : "Pekerjaan"
            },
            {
                key : 'desc',
                label : "Keterangan"
            }
        ]

        let dataFunds = get(this.eventStore.selectedData,'funds',[]).map((prop, index)=>{
            return {
                number : index+1,
                ...prop
            }
        });

        let dataCrew = (get(this.eventStore.selectedData,'crew') || []).map((prop, index)=>{
            return {
                number : index+1,
                ...prop
            }
        });
        const allowedChangeStatus = this.props.appstate.userData.role_id === constant.ROLES.MR;
        // console.log(this.props.appstate.userData);
        let form_comment = [
            {
                key : 'target',
                label : 'Target',
                type : 'multiline',
            },
            {
                key : 'achievement',
                label : 'Pencapaian Aktual',
                type : 'multiline',
                // disabled: !allowedChangeComment
            },
            {
                key : 'evaluation',
                label : 'Evaluasi',
                type : 'multiline',
                // disabled: !allowedChangeComment
            }
        ];

        let form_event = [
            {
                key : 'name',
                label : 'Nama Event',
                type : 'text',
            },
            {
                key : 'date',
                label : 'Tanggal',
                type : 'date',
            },
            {
                key : 'place',
                label : 'Lokasi',
                type : 'text',
            },
            // {
            //     key : 'status',
            //     label : 'Approve Pimpro',
            //     type : 'select',
            //     items : [
            //         {
            //             value : 'created',
            //             label : 'Waiting'
            //         },
            //         {
            //             value : 'approved',
            //             label : 'Approved'
            //         },
            //         {
            //             value : 'rejected',
            //             label : 'Rejected'
            //         },
            //     ],
            //     disabled: !allowedChangeStatus
            // },
            // {
            //     key : 'desc',
            //     label : 'Keterangan Pimpro',
            //     type : 'multiline',
            //     required: false,
            //     disabled: !allowedChangeStatus
            // },
        ];

		return (
			<Grid container spacing={24}>
                <Grid item xs={12}>
                    <DialogForm 
                        open={this.state.openEditCommentDialog} 
                        title={'Edit Komentar'}
                        onClose={()=>this.setState({openEditCommentDialog: false})}
                        forms={form_comment}
                        onSave={this.saveComment}
                        value={this.eventStore.selectedData}
                    />
                    <DialogForm
	                    open={this.state.openEditDialog} 
                        title={'Edit Event'}
                        onClose={()=>this.setState({openEditDialog: false})}
                        forms={form_event}
                        onSave={this.saveComment}
                        value={this.eventStore.selectedData}
                    >
                        <EventFormComponent 
                            hideEvaluation={true} 
                            defaultFunds={this.eventStore.selectedData.funds} 
                            defaultCrew={this.eventStore.selectedData.crew}/>
	                </DialogForm>
					<GenericHorizontal
					    title={`Event ${get(this.eventStore.selectedData,'name','-')}`}
					    subtitle={`detail event`}
					    data={this.dataEvent}
					    tools={tools}
					>
						<div>
						    <h5>A. Biaya Teknis</h5>
						    <Table
                                actionColumns={[]}
                                tableHeaderColor="primary"
                                tableHead={dataFundsColumn}
                                tableData={dataFunds}   
                                pagination={false}
                                dataHeaderActive={true}
                                dataHeaderColor={"#a5c7ff"}
                            />

                            <h5>B. Crew</h5>
						    <Table
                                actionColumns={[]}
                                tableHeaderColor="primary"
                                tableHead={dataCrewColumn}
                                tableData={dataCrew}   
                                pagination={false}
                                dataHeaderActive={true}
                                dataHeaderColor={"#a5c7ff"}
                            />
						</div>
					</GenericHorizontal>
                    <GenericHorizontal
					    title={'Target dan Evaluasi'}
					    subtitle={``}
					    data={this.dataEvaluasi}
					    tools={commentTools}
					></GenericHorizontal>

                    {
                        (this.state.pimpro_manager_permission.update && get(this.eventStore.selectedData,'status','') !== 'approved' )  &&

                        <Grid item xs={12} >
                            <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                Pimpinan Project
                            </Typography>
                            <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('status')} style={{marginRight : '20px'}}>
                                Approve
                            </Button>
                            {
                                (get(this.eventStore.selectedData,'status','') === 'created') &&
                                <Button color="secondary" onClick={()=>this.onClickReject('status')}>
                                    Reject
                                </Button>
                            }
                        </Grid>

                    }
                </Grid>
                <Dialog
                    open={this.state.openRejectDialog}
                    title={'Isi Keterangan'}
                    onClose={this.closeDialogReject}
                    actions={actions}
                >
                    <FormBuilder forms={[{
                        key : 'desc',
                        label : 'Keterangan',
                        type : 'multiline',
                        placeholder : 'Alasan ditolak atau revisi'
                    }]} value={{}} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                </Dialog>
            </Grid>
		)
	}
}