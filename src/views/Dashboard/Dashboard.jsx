import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions/Dashboard';

// react plugin for creating charts
import ChartistGraph from 'react-chartist';
// @material-ui/core
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// @material-ui/icons
import ContentCopy from '@material-ui/icons/FileCopy';
import Store from '@material-ui/icons/Store';
import InfoOutline from '@material-ui/icons/InfoOutlined';
import Warning from '@material-ui/icons/Warning';
import DateRange from '@material-ui/icons/DateRange';
import LocalOffer from '@material-ui/icons/LocalOffer';
import Update from '@material-ui/icons/Update';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import AccessTime from '@material-ui/icons/AccessTime';
import Accessibility from '@material-ui/icons/Accessibility';
import BugReport from '@material-ui/icons/BugReport';
import Code from '@material-ui/icons/Code';
import Cloud from '@material-ui/icons/Cloud';
// core components
import GridItem from '../../components/Grid/GridItem.jsx';
import Table from '../../components/Table/Table.jsx';
import Tasks from '../../components/Tasks/Tasks.jsx';
import CustomTabs from '../../components/CustomTabs/CustomTabs.jsx';
import Danger from '../../components/Typography/Danger.jsx';
import Card from '../../components/Card/Card.jsx';
import CardHeader from '../../components/Card/CardHeader.jsx';
import CardIcon from '../../components/Card/CardIcon.jsx';
import CardBody from '../../components/Card/CardBody.jsx';
import CardFooter from '../../components/Card/CardFooter.jsx';

import dashboardStyle from './dashboardStyle.jsx';
import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';
import Icon from '@material-ui/core/Icon';
import {startCase,orderBy} from 'lodash';
import { PieChart, Pie,Tooltip,Cell,ResponsiveContainer } from 'Recharts';

//Connect component to Redux store.
@connect(
  state => {
    return {
      charts: state.charts.data || {},
      general: state.general.data || {},
      loading: state.general.loading || state.charts.loading
    };
  },
  dispatch => {
    return { actions: bindActionCreators(actions, dispatch) };
  }
)
@inject('appstate')
@observer
class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.dashboardStore = props.appstate.dashboard;
    this.globalUI = props.appstate.global_ui;
    this.userData = props.appstate.userData;
    this.state = {
      value: 0,
      project_id : this.userData.selected_project,
    };

  }

  async componentDidMount() {
    // const {
    //   dailySalesChart,
    //   emailsSubscriptionChart,
    //   completedTasksChart
    // } = this.props.charts;
    // const { bugs, website, server } = this.props.general;

    //Check and load data from Server
    // if (!dailySalesChart || !emailsSubscriptionChart || !completedTasksChart)
    //   await this.props.actions.getChartData();

    //Check and load data from Server
    // if (!bugs || !website || !server) await this.props.actions.getGeneral();
    this.globalUI.openLoader();
    await this.dashboardStore.getAll();
    this.globalUI.closeLoader();
  }

  async componentDidUpdate(prev){
      if(prev.appstate.project.selectedProject !== this.state.project_id){
          this.setState({project_id : prev.appstate.project.selectedProject});
          this.globalUI.openLoader();
          await this.dashboardStore.getAll().catch((err)=>{
              this.globalUI.closeLoader();
          });
          this.globalUI.closeLoader();
      }
  }

  handleChange = (_, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  renderCountCard = (prop,key)=>{
    const { classes } = this.props;
    return (
      <GridItem xs={12} sm={6} md={3} key={key}>
        <Card>
          <CardHeader color="success" stats icon>
            <CardIcon color="success">
              <Icon className={classes.iconDash}>{prop.icon}</Icon>
            </CardIcon>
            <p className={classes.cardCategory}>{prop.title}</p>
            <h3 className={classes.cardTitle}>{prop.value}</h3>
          </CardHeader>
          <CardFooter stats>
            <div className={classes.stats}>
              <Icon className={classes.iconSub}>{prop.subtitle_icon}</Icon> {" "+prop.subtitle}
            </div>
          </CardFooter>
        </Card>
      </GridItem>
    )  
  }

  renderPieChart = (prop,key)=>{
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
    const { classes } = this.props;

    let dataPie = toJS(prop.value)
    dataPie.map(it=>{
      it.value = parseInt(it.value);
      it.name = startCase(it.name)
      return it;
    })

    return (
      <GridItem xs={12} sm={6} md={3} key={key}>
         <Card chart>
          <CardHeader color="success">
            <ResponsiveContainer  width={'100%'} height={200}>
            <PieChart>
                    <Pie
                      data={dataPie} 
                      dataKey={'value'}
                      cx={'50%'} 
                      cy={'50%'}
                      outerRadius={80} 
                      fill="#8884d8"
                    >
                    {
                      dataPie.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
                    }
                </Pie>
                <Tooltip/>
              </PieChart>
              </ResponsiveContainer>
          </CardHeader>
          <CardBody>
            <h4 className={classes.cardTitle}>{prop.title}</h4>
          </CardBody>
          <CardFooter chart>
            <div className={classes.stats}>
              <Icon className={classes.iconSub}>{prop.subtitle_icon}</Icon> {" "+prop.subtitle}
            </div>
          </CardFooter>
        </Card>
      </GridItem>
    )
  }

  render() {
    const { classes } = this.props;
    const {
      dailySalesChart,
      emailsSubscriptionChart,
      completedTasksChart
    } = this.props.charts;
    const { bugs, website, server } = this.props.general;
    const loading = this.props.loading;
    const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
                  {name: 'Group C', value: 300}, {name: 'Group D', value: 200}];
    // console.log(this.props.appstate.project.selectedProject,'project_id')
    return (
      <div>
        <div style={{display: 'none'}}>{this.props.appstate.project.selectedProject}</div>
        <Grid container>
          {
            this.dashboardStore.data.filter(it=>it.type === 'count').map((prop,key)=>{
                return this.renderCountCard(prop,key)
            })
          }
        </Grid>
        <Grid container>
          {
            this.dashboardStore.data.filter(it=>it.type === 'pie').map((prop,key)=>{
                return this.renderPieChart(prop,key)
            })
          }
        </Grid>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
