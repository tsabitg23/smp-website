import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import PrintIcon from '@material-ui/icons/Print';
import DescriptionIcon from '@material-ui/icons/Description';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import Edit from '@material-ui/icons/Edit';
import Table from '../../components/Table/';
import TableHorizontal from '../../components/Table/TableHorizontal';
import Dialog from '../../components/Dialog/DialogOnly';
import VerificationForm from '../VerificationForm/';
@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class UnitVerificationDetail extends React.Component {
    constructor(props){
        super(props);
        this.unitVerifStore = props.appstate.unit_verification;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            openVerifResultDialog : false,
            defaultValue : {}
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.unitVerifStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
    }

    get detailData(){
        let column = [
            {
                key : 'customer',
                label : 'Customer',
                type : 'text'
            },
            {
                key : 'unit.kavling_number',
                label : 'Unit Kavling NO',
                type : 'text'
            },
            {
                key : 'unit.status',
                label : 'Unit Status',
                type : 'text'
            },
            {
                key : 'agent.name',
                label : 'Agent',
                type : 'text'
            },
            {
                key : 'booking_date',
                label : 'Booking Date',
                type : 'date'
            },
            {
                key : 'verification_date',
                label : 'Verification Date',
                type : 'date'
            },
            {
                key : 'akad_date',
                label : 'Akad Date',
                type : 'date',
                required : false
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerifStore.selectedData,it.key,'');
            return it;
        });
    }

    get formResult(){
        let column = [
            {
                key : 'name',
                label : 'Name',
                type : 'text'
            },
            {
                key : 'gender',
                label : 'Gender',
                type : 'text'
            },
            {
                key : 'ktp_number',
                label : 'KTP Number',
                type : 'text'
            },
            {
                key : 'city',
                label : 'City',
                type : 'text'
            },
            {
                key : 'occupation',
                label : 'Occupation',
                type : 'text'
            },
            {
                key : 'created_at',
                label : 'Verification Date',
                type : 'date'
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerifStore.selectedData.verification,it.key,'');
            return it;
        });
    }

    get partOneSum(){
        let column = [
            {
                key : 'total_score',
                label : "Total Score"
            },
            {
                key : 'category',
                label : "Category"
            },
            {
                key : 'desc',
                label : "Description"
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerifStore.selectedData.verification,`scores[0][${it.key}]`,'');
            return it;
        });
    }

    get partTwoSum(){
        let column = [
            {
                key : 'total_score',
                label : "Total Score"
            },
            {
                key : 'category',
                label : "Category"
            },
            {
                key : 'desc',
                label : "Description"
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerifStore.selectedData.verification,`scores[1][${it.key}]`,'');
            return it;
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.unitVerifStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.unitVerifStore.getDetail(data.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit unit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };


    onClickBack = ()=>{
        this.props.history.push(LINKS.UNIT_VERIFICATION);
    };

    onDialogClose = ()=>this.setState({isDialogOpen : false});


    render(){
        const {classes} = this.props;

        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>,
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <PrintIcon />
            </Fab>
        ];

        const verificationResultTools = [
            <Fab onClick={()=>this.setState({openVerifResultDialog : true})} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <DescriptionIcon />
            </Fab>
        ]

        let partOneHeader = [
            {
                key : 'category',
                label : "Category"
            },
            {
                key : 'value',
                label : "Score"
            },
            {
                key : 'desc',
                label : "Description"
            }
        ]

        let partOneData = get(this.unitVerifStore.selectedData,'verification.scores[0].data',[]);

        return (
            <div className={classes.root}>

                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        {/*<Dialog
                            open={this.state.openVerifResultDialog}
                            title={'Verification Result'}
                            onClose={()=>this.setState({openVerifResultDialog : false})}
                        >*/}
                            {/*<VerificationForm readonly={true}/>*/}
                        {/*</Dialog>*/}
                        <GenericHorizontal
                            title={`Verification ${get(this.unitVerifStore.selectedData,'id','asd22i-sdjk').split('-')[0]}`}
                            subtitle={`Detail information of customer verification ${get(this.unitVerifStore.selectedData,'id','asd22i-sdjk').split('-')[0]}`}
                            data={this.detailData}
                            tools={tools}
                        />


                        <GenericHorizontal
                            title={`Verification Result`}
                            subtitle={`The result from verification form`}
                            data={this.formResult}
                            tools={verificationResultTools}
                        >
                            <div>
                                <h5>Pertanyaan Bagian 1</h5>
                                <Table
                                    actionColumns={[]}
                                    tableHeaderColor="primary"
                                    tableHead={partOneHeader}
                                    tableData={partOneData}   
                                    pagination={false}
                                />
                                <TableHorizontal
                                    data={this.partOneSum}/>
                                <h5>Pertanyaan Bagian 2</h5>
                                <TableHorizontal
                                    data={this.partTwoSum}/>
                            </div>
                        </GenericHorizontal>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(UnitVerificationDetail);