import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';
import RemoveRedEye from "@material-ui/icons/RemoveRedEye";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MinuteOfMeeting extends React.Component{
    constructor(props){
        super(props);
        this.minuteOMeetingStore = props.appstate.minute_of_meeting;
        this.employeeStore = props.appstate.employee;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('minutes_of_meetings')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.employeeStore.setRequestQuery({show_all : true});
        await this.minuteOMeetingStore.getAll();
        await this.employeeStore.getAll();
        this.global_ui.closeLoader();
    }

    // componentWillUnmount(){
    //     this.employeeStore.setRequestQuery({});
    // }

    componentWillUnmount() {
        this.minuteOMeetingStore.setRequestQuery({
            show_all: true
        });
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            this.employeeStore.setRequestQuery({show_all : true});
            await this.minuteOMeetingStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        if(this.employeeStore.data.length == 0){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Employee data not found, please add employee data first before insert this data',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.minuteOMeetingStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.minuteOMeetingStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        let res = this.state.openMode === 'create' ? this.minuteOMeetingStore.create(data) : this.minuteOMeetingStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.minuteOMeetingStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    applyFilter = (data) => {
        this.minuteOMeetingStore.setRequestQuery({
            ...data,
            start_key : 'meeting_date',
            end_key : 'meeting_date',
            show_all : true
        });
        this.global_ui.openLoader();
        this.minuteOMeetingStore.getAll().then(res=>{
            this.global_ui.closeLoader();
        }).catch((er)=>{
            this.global_ui.closeLoader();
        });
    };

    onViewDetail = async (rowData)=>{
        this.props.history.push('/app/notulen/'+rowData.id);
    };

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'free'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete'},
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'meeting_date',
                label : 'Tanggal Meeting',
                type: 'date'
            },
            {
                key : 'employee_id',
                label : 'PIC',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.name} - ${it.nik}`
                    };
                })
            },
            {
                key : 'place',
                label : 'Tempat',
                type: 'text'
            },
            {
                key : 'time',
                label : 'Pukul',
                type: 'time'
            },
            {
                key : 'description',
                label : 'Pembahasan',
                type: 'multiline'
            },
            {
                key : 'participant',
                label : 'Peserta Rapat',
                type: 'multiline'
            },
            {
                key : 'problems',
                label : 'Kendala',
                type: 'multiline'
            },
            {
                key : 'results',
                label : 'Hasil Keputusan',
                type: 'multiline'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Notulen Meeting",
            icon : "class",
            subtitle : "Jumlah notulen meeting",
            subtitle_icon : "assignment",
            value : this.minuteOMeetingStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} data`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Notulen Meeting'}
                    subtitle={'Pembahasan dan notulen meeting'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {
                            key : 'meeting_date',
                            label : 'Tanggal Meeting',
                            type: 'date'
                        },
                        {
                            key : 'employee.name',
                            label : 'PIC',
                            type: 'select'
                        },
                        {
                            key : 'place',
                            label : 'Tempat',
                            type: 'string'
                        },
                        {
                            key : 'time',
                            label : 'time'
                        },
                        {
                            key : 'description',
                            label : 'Pembahasan',
                            type: 'text'
                        },
                        {
                            key : 'status',
                            label : 'Approve PIMPRO',
                            type : 'branch',
                            branch : [{
                                value : null,
                                text : 'Pending'
                            },{
                                value : undefined,
                                text : 'Pending'
                            },{
                                value : false,
                                text : 'Revisi'
                            },{
                                value : true,
                                text : 'Diterima'
                            }]
                        },
                ]}
                    actionColumn={configActionColumns.filter(it=>(it.key === 'free') ? true : this.state.permission[it.key])}
                    data={this.minuteOMeetingStore.data}
                    tools={tools}
                    filter={[{
                        type : 'range'
                    }]}
                    onFilter={this.applyFilter}
                />
            </div>
        );
    }

}

export default MinuteOfMeeting;