import React from 'react';
import { constant } from '../../../configs/const';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get, omit} from 'lodash';
import TopStatus from '../../components/TopStatus';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import DefaultChart from "../../components/Chart";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class HRDReports extends React.Component{
    constructor(props){
        super(props);
        this.hrdReportStore = props.appstate.hrd_reports;
        this.employeeStore = props.appstate.employee;
        this.unitStore = props.appstate.unit;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('report_mingguan');
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : permission,
            numberOfMonthChart : 4
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.hrdReportStore.getAll();
        await this.hrdReportStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
        await this.employeeStore.getAll();
        // this.unitStore.();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.hrdReportStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.hrdReportStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = async ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.hrdReportStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.hrdReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        const reqBody = data;
        // reqBody.total_customer_pending_interview =
        reqBody.total_customer_pending_interview = +reqBody.total_customer_interviewed - (+reqBody.total_customer_interviewed_passed) - (+reqBody.total_customer_interviewed_failed);
        reqBody.total_customer_pending_akad = +reqBody.total_customer_akad - (+reqBody.total_customer_interviewed_passed)

        let res = this.state.openMode === 'create' ? this.hrdReportStore.create(reqBody) : this.hrdReportStore.update(data.id,reqBody);
        res.then(async res=>{
            this.onDialogClose();
            await this.hrdReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    onViewDetail = async (rowData)=>{
        this.props.history.push('/app/hrd_report/'+rowData.id);
    };

    applyFilter = (data) => {
        this.hrdReportStore.setRequestQuery({
            ...data,
            start_key : 'meeting_date',
            end_key : 'meeting_date',
            show_all : true
        });
        this.global_ui.openLoader();
        this.hrdReportStore.getAll().then(res=>{
            this.global_ui.closeLoader();
        }).catch((er)=>{
            this.global_ui.closeLoader();
        });
    };

    onSetPeriode = (val, updateData=null)=>{
        const dummyFunc = ()=>{}
        this.global_ui.openLoader();
        this.unitStore.getUnitBookedByPeriod(val).then(res=>{
            this.global_ui.closeLoader();
            if(updateData){
                updateData('total_customer_booking', `${res.length}`);
            }
        });
    }

    refreshChart = async (value, closeLoader=true) => {
        this.global_ui.openLoader();
        await this.hrdReportStore.getChart(value);
        this.global_ui.closeLoader();
    }

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'read'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'employee_id',
                label : 'Karyawan Input',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : true
            },
            {
                key : 'period_start',
                label : 'Periode Mulai',
                twin_form : 'period_end',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'period_end',
                label : 'Periode Selesai',
                twin_form : 'period_start',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'total_customer_booking',
                label : 'Total Customer Booking',
                type: 'text',
                disabled: true
            },
            {
                key : 'total_customer_interviewed',
                label : 'Total Customer Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_akad',
                label : 'Total Customer Akad',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_passed',
                label : 'Total Customer Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_failed',
                label : 'Total Customer Tidak Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_pending_interview',
                label : 'Total Customer Belum Wawancara',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_interviewed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_failed'
                }],
                fixed: 0,
                placeholder: ''
            },
            {
                key : 'total_customer_pending_akad',
                label : 'Total Customer Belum Akad',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_akad'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                }],
                fixed: 0,
                placeholder: ''
            },
        ];

        let table_header = [
            {
                key: 'no',
                label : 'No',
                type : 'text'
            },
            {
                key : 'employee.name',
                label : 'Karyawan Input',
                type: 'date'
            },
            {
                key : 'employee.position',
                label : 'Jabatan',
                type: 'text'
            },
            {
                key : 'total_customer_booking',
                label : 'Total Customer Booking',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed',
                label : 'Total Customer Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_akad',
                label : 'Total Customer Akad',
                type : 'number'
            },
            {
                key : 'manager_approval_status',
                label : 'Approve Management',
                type : 'branch',
                branch : [{
                    value : null,
                    text : 'Pending'
                },{
                    value : false,
                    text : 'Revisi'
                },{
                    value : true,
                    text : 'Diterima'
                }]
            },
            {
                key : 'manager_desc',
                label : 'Keterangan',
                type : 'text'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : 'Laporan Mingguan',
            icon : 'people',
            subtitle : 'Jumlah Data',
            subtitle_icon : 'assignment',
            value : this.hrdReportStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Data`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <DefaultChart
                    subtitle={'Grafik Data Unit'}
                    changeFilter={this.refreshChart}
                    showMonth={false}
                    data={this.hrdReportStore.chart}/>
                <GenericPage
                    title={'Laporan Mingguan'}
                    subtitle={'laporan mingguan hrd'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={table_header}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.hrdReportStore.data}
                    tools={tools}
                    filter={[{
                        type : 'range'
                    }]}
                    onFilter={this.applyFilter}
                    maxData={this.hrdReportStore.maxData}
                    customWidth={'90vw'}
                />
            </div>
        );
    }

}

export default HRDReports;
