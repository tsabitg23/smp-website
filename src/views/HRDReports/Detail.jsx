import React from 'react';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import Fab from '@material-ui/core/Fab';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {inject, observer} from 'mobx-react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {LINKS} from '../../routes/index';
import EditIcon from '@material-ui/icons/Edit';
import Table from '../../components/Table/index';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import FormBuilder from '../../components/FormBuilder';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { constant } from '../../../configs/const';
import EventFormComponent from '../Events/EventFormComponent';
import Form from "../CustomerAnalyst/Form";
import TableHorizontal from "../../components/Table/TableHorizontal";
import moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
export default class HRDReportDetail extends React.Component{
    constructor(props){
        super(props);
        this.hrdReportStore = props.appstate.hrd_reports;
        this.employeeStore = props.appstate.employee;
        this.unitStore = props.appstate.unit;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('customer_analyst');
        this.state = {
            pimpro_manager_permission : {},
            permission : permission,
            openEditDialog : false,
            openRejectDialog : false,
            formErrors : [],
            rejectedKey : null,
            formData : {
                desc : ''
            },
            openEditCommentDialog: false
        };

    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.hrdReportStore.getDetail(this.props.match.params.id);
        await this.employeeStore.getAll();
        this.setState({
            pimpro_manager_permission : this.props.appstate.getPermission('manager_pimpro'),
        })
        this.global_ui.closeLoader();
    }


    onClickBack = ()=>{
        this.props.history.push(LINKS.HRD_REPORT);
    };

    getValueColor(status){
        if(status === 'approved' || status){
            return 'success'
        }
        else if(status === 'rejected' || status === false){
            return 'danger'
        }
        else{
            return ''
        }
    }



    closeDialogEdit = async ()=>{
        this.global_ui.openLoader();
        await this.hrdReportStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
        this.setState({openEditDialog : false})
    }

    onSetPeriode = (val, updateData=null)=>{
        const dummyFunc = ()=>{}
        this.global_ui.openLoader();
        this.unitStore.getUnitBookedByPeriod(val).then(res=>{
            this.global_ui.closeLoader();
            if(updateData){
                updateData('total_customer_booking', `${res.length}`);
            }
        });
    }

    saveAnalyst = async (data, isApproval=false)=>{
        this.global_ui.openLoader();
        const reqBody = data;
        if(!isApproval){
            reqBody.total_customer_pending_interview = +reqBody.total_customer_interviewed - (+reqBody.total_customer_interviewed_passed) - (+reqBody.total_customer_interviewed_failed);
            reqBody.total_customer_pending_akad = +reqBody.total_customer_akad - (+reqBody.total_customer_interviewed_passed)
        }
        let res = this.hrdReportStore.update(reqBody.id, reqBody, false);
        res.then(async res=>{
            this.setState({
                openEditCommentDialog : false,
                openEditDialog : false
            });
            await this.hrdReportStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };


    onClickApprove = key=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to approved this request?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.saveAnalyst({[key] : true, id: this.props.match.params.id}, true);
                }
            }
        );
    };

    onClickReject = key => {
        this.setState({
            rejectedKey : key,
            openRejectDialog : true
        });
    };

    reject = () =>{
        this.setState({
            openRejectDialog: false
        });
        this.saveAnalyst({
            [this.state.rejectedKey] : false,
            manager_desc : this.state.formData.manager_desc,
            id : this.props.match.params.id
        }, true);
    };

    closeDialogReject = ()=>this.setState({openRejectDialog : false});

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    debtorColumn = [
        {
            key : 'debtor_dob',
            label : 'Tanggal Lahir Debitor',
            type: 'date'
        },
        {
            key : 'spouse_name',
            label : 'Nama Istri',
            type: 'text'
        },
        {
            key : 'occupation',
            label : 'Perkerjaan',
            type: 'text'
        },
        {
            key : 'spouse_occupation',
            label : 'Pekerjaan Istri',
            type: 'text'
        },
        {
            key : 'work_duration',
            label : 'Lama bekerja',
            type: 'text'
        },
        {
            key : 'spouse_work_duration',
            label : 'Lama istri bekerja',
            type: 'text'
        },
        {
            key : 'children',
            label : 'Jumlah anak',
            type: 'text'
        },
        {
            key : 'payroll_desc',
            label : 'Deskripsi Gaji',
            type: 'text'
        },
        {
            key : 'debtor_asset',
            label : 'Aset',
            type: 'text'
        },
    ];

    render(){
        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];

        if(this.state.permission.update){
            tools.push(
                <Fab onClick={()=>{this.setState({openEditDialog : true})}} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                    <EditIcon />
                </Fab>
            )
        }

        let actions = [
            <Button color="inherit" onClick={this.reject}>
                save
            </Button>
        ];

        let dataDebitor = this.debtorColumn.map(it=>{
            const val = get(this.hrdReportStore.selectedData,it.key,'');
            it.value = val || "-";
            return it;
        })

        let mainDataColumn  = [
            {
                key : 'employee_id',
                label : 'Karyawan Input',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : true
            },
            {
                key : 'period_start',
                label : 'Periode Mulai',
                twin_form : 'period_end',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'period_end',
                label : 'Periode Selesai',
                twin_form : 'period_start',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'total_customer_booking',
                label : 'Total Customer Booking',
                type: 'number',
                disabled: true
            },
            {
                key : 'total_customer_interviewed',
                label : 'Total Customer Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_akad',
                label : 'Total Customer Akad',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_passed',
                label : 'Total Customer Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_failed',
                label : 'Total Customer Tidak Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_pending_interview',
                label : 'Total Customer Belum Wawancara',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_interviewed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_failed'
                }],
                fixed: 0,
                placeholder: ''
            },
            {
                key : 'total_customer_pending_akad',
                label : 'Total Customer Belum Akad',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_akad'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                }],
                fixed: 0,
                placeholder: ''
            },
            {
                key : 'manager_approval_status',
                label : 'Approve Manager',
                type : 'select',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.hrdReportStore.selectedData,'manager_approval_status','')),
            },
            {
                key : 'manager_desc',
                label : 'Deskripsi',
                type : 'text',
            },

        ];

        let mainData = mainDataColumn.map(it=>{

            if(it.key.includes('status')){
                let val = it.value = get(this.hrdReportStore.selectedData,it.key,'');
                if(val){
                    it.value = 'Approved';
                } else if(val === false){
                    it.value = 'Revisi';
                } else{
                    it.value = 'Waiting';
                }
            }
            else{
                it.value = get(this.hrdReportStore.selectedData,it.key,'') || "-";
            }
            if(it.value ==='-' && it.type==='number'){
                it.value = 0;
            }
            return it;
        })

        let form_items = [
            {
                key : 'employee_id',
                label : 'Karyawan Input',
                type: 'autocomplete',
                items: this.employeeStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : true
            },
            {
                key : 'period_start',
                label : 'Periode Mulai',
                twin_form : 'period_end',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'period_end',
                label : 'Periode Selesai',
                twin_form : 'period_start',
                type: 'date',
                onChange : this.onSetPeriode
            },
            {
                key : 'total_customer_booking',
                label : 'Total Customer Booking',
                type: 'text',
                disabled: true
            },
            {
                key : 'total_customer_interviewed',
                label : 'Total Customer Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_akad',
                label : 'Total Customer Akad',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_passed',
                label : 'Total Customer Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_interviewed_failed',
                label : 'Total Customer Tidak Lolos Wawancara',
                type : 'number'
            },
            {
                key : 'total_customer_pending_interview',
                label : 'Total Customer Belum Wawancara',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_interviewed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_failed'
                }],
                fixed: 0,
                placeholder: ''
            },
            {
                key : 'total_customer_pending_akad',
                label : 'Total Customer Belum Akad',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_customer_akad'
                },{
                    operator : 'sub',
                    key : 'total_customer_interviewed_passed'
                }],
                fixed: 0,
                placeholder: ''
            },
        ];


        return (
            <Grid container spacing={24}>
                <Grid item xs={12}>
                    <DialogForm
                        open={this.state.openEditDialog}
                        title={'Edit Data'}
                        onClose={()=>this.closeDialogEdit()}
                        forms={form_items}
                        onSave={this.saveAnalyst}
                        value={this.hrdReportStore.selectedData}
                    />
                    <GenericHorizontal
                        title={`Laporan Mingguan${get(this.hrdReportStore.selectedData,'id','-').split('-')[0]}`}
                        subtitle={`Laporan Mingguan HRD`}
                        data={mainData}
                        tools={tools}
                    />

                    {
                        (this.state.pimpro_manager_permission.update && get(this.hrdReportStore.selectedData,'manager_approval_status','') !== true )  &&

                        <Grid item xs={12} >
                            <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                Pimpinan Project
                            </Typography>
                            <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('manager_approval_status')} style={{marginRight : '20px'}}>
                                Approve
                            </Button>
                            {
                                (get(this.hrdReportStore.selectedData,'manager_approval_status','') === null) &&
                                <Button color="secondary" onClick={()=>this.onClickReject('manager_approval_status')}>
                                    Reject
                                </Button>
                            }
                        </Grid>

                    }
                </Grid>
                <Dialog
                    open={this.state.openRejectDialog}
                    title={'Isi Keterangan'}
                    onClose={this.closeDialogReject}
                    actions={actions}
                >
                    <FormBuilder forms={[{
                        key : 'manager_desc',
                        label : 'Keterangan',
                        type : 'multiline',
                        placeholder : 'Alasan ditolak atau revisi'
                    }]} value={{}} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                </Dialog>
            </Grid>
        )
    }
}
