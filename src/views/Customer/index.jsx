import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get,startCase} from 'lodash';
import TopStatus from '../../components/TopStatus';
import Dialog from '../../components/Dialog/DialogOnly';
import CustomerForm from './customerForm';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';

let titlePage = "customer";
@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Customers extends React.Component{
    constructor(props){
        super(props);
        this.customerStore = props.appstate.customer;
        this.unitAkadStore = props.appstate.unit_akad_data;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            unit_akad_id : '',
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('customers')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.customerStore.getCustomerAkad();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.customerStore.getCustomerAkad().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.join_date = moment(data.join_date).format('YYYY-MM-DD');
        console.log(rowData.akad[0].id,'~~~~~~~~')
        const editData = {
            name : rowData.akad[0].customer
        };
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : editData, unit_akad_id : rowData.akad[0].id});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            `Are you sure you want to delete this ${titlePage}?`,
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.customerStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.customerStore.getCustomerAkad();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success delete ${titlePage}`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        let res = this.state.openMode === 'create' ? this.customerStore.create(data) : this.customerStore.update(data.id,data);
        res.then(async res=>{
            const customerId = res.id;
            await this.unitAkadStore.update(this.state.unit_akad_id,{
                customer_id : customerId
            }, false).catch(err=>{
                throw err;
            });
            this.onDialogClose();
            await this.customerStore.getCustomerAkad();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} ${titlePage}`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    onViewDetail = async (rowData)=>{
        const customer_id = rowData.akad[0].customer_id;
        this.props.history.push('/app/customers/'+customer_id);
    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'Lihat Data Customer', Color: 'gray', Callback: this.onViewDetail ,key : 'read', show_if_true : ['is_data_valid']},
            { Icon: Edit, Tooltip: 'Tambah Data Customer', Color: 'primary', Callback: this.onEditClick ,key : 'update', show_if_true : ['is_data_not_valid']},
            // { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            // <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
            //     <Add />
            // </Fab>
        ];

        let form_items = [
            {
                key: 'no',
                label: 'No'
            },
            {
                key : 'kavling_number',
                label : 'Unit',
                type: 'text'
            },
            {
                key : 'akad[0].customer',
                label : 'Nama Konsumen',
                type: 'text'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Customers",
            icon : "face",
            subtitle : "Total customers",
            subtitle_icon : "assignment",
            value : this.customerStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} ${titlePage}`}
                    onClose={this.onDialogClose}
                >
                    <CustomerForm onClose={()=>this.onDialogClose()} openMode={this.state.openMode} onSave={this.save} defaultValue={this.state.defaultValue}/>
                </Dialog>
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={startCase(titlePage)}
                    subtitle={`daftar ${titlePage} akad`}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.customerStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default Customers;
