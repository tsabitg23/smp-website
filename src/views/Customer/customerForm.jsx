import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import {inject, observer} from 'mobx-react';
import styles from './styles.jsx';
import {get} from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import MessageBox from '../../components/MessageBox';
import messageBoxActions from '../../actions/MessageBox';
import DataPribadi from './Step/DataPribadi';
import DataPekerjaan from './Step/DataPekerjaan';
import DataKontak from './Step/DataKontak';
import DataFinansial from './Step/DataFinansial';
import DataUnit from './Step/DataUnit';
import {omit,pick} from 'lodash';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class CustomerForm extends React.Component{
	constructor(props){
		super(props);
		let stateData ={
			activeStep : 0,
			data_pribadi_form : {},
			data_pekerjaan_form : {},
			data_kontak_form : {},
			data_finansial_form : {},
			data_unit_form : {},
			isLoading : false,
			id : get(props.defaultValue,'id','null')
		}
		this.customerStore = props.appstate.customer;
		this.global_ui = props.appstate.global_ui;
			if(this.props.openMode === 'edit'){
			let dataPribadi = omit(this.props.defaultValue,['company_data','contactable_data','financial_data']);
			if(typeof dataPribadi.phones_data === 'string'){
				dataPribadi.phones_data = JSON.parse(dataPribadi.phones_data);
			}
			let dataKantor,dataKontak,dataFinansial;
			if(typeof this.props.defaultValue.company_data === 'string'){
				dataKantor = JSON.parse(this.props.defaultValue.company_data);
			}
			if(typeof this.props.defaultValue.contactable_data === 'string'){
				dataKontak = JSON.parse(this.props.defaultValue.contactable_data);
			}
			if(typeof this.props.defaultValue.financial_data === 'string'){
				dataFinansial = JSON.parse(this.props.defaultValue.financial_data);
			}


			Object.keys(dataPribadi.phones_data).map(key=>{
				dataPribadi[`phones_data.${key}`] = dataPribadi.phones_data[key];
				delete dataPribadi[key]
			})

			Object.keys(dataKantor).map(key=>{
				dataKantor[`company_data.${key}`] = dataKantor[key];
				delete dataKantor[key]
			})

			Object.keys(dataKontak).map(key=>{
				dataKontak[`contactable_data.${key}`] = dataKontak[key];
				delete dataKontak[key]
			})

			Object.keys(dataFinansial).map(key=>{
				dataFinansial[`financial_data.${key}`] = dataFinansial[key];
				delete dataFinansial[key]
			})

			stateData.data_pribadi_form = dataPribadi;
			stateData.data_pekerjaan_form = dataKantor;
			stateData.data_kontak_form = dataKontak;
			stateData.data_finansial_form = dataFinansial;
		}

		if(this.props.openMode === 'create'){
			stateData.data_pribadi_form = this.props.defaultValue
		}
		this.state = stateData;
	};

	steps = this.props.openMode === 'create' ? [
		'Data Pribadi',
		'Data Pekerjaan',
		'Kontak yang bisa dihubungi',
		'Data Finansial',
		// 'Data Unit'
	] : [
		'Data Pribadi',
		'Data Pekerjaan',
		'Kontak yang bisa dihubungi',
		'Data Finansial',
	];

	ref = [
			"data_pribadi_form",
			"data_pekerjaan_form",
			"data_kontak_form",
			"data_finansial_form",
			"data_unit_form"
	]

	handleNext = async ()=>{
		const {activeStep} = this.state;

		if(this.props.readonly){
			this.updateStep(activeStep + 1);
			return;
		}
		this[this.ref[activeStep]].validate().then(async res=>{
			this.saveFormData(this.ref[activeStep],res)
			if(activeStep < (this.steps.length -1) && this.props.openMode === 'create'){
				this.updateStep(activeStep + 1);
			}
			else{
				this.save(activeStep)
			}
        });

	};


	handleBack = ()=>{

		this.updateStep(this.state.activeStep -1);

		// this.setState({activeStep : this.state.activeStep - 1});/
	};

	changeFormatData = (data)=>{
		let formatedObject = data;
		Object.keys(formatedObject).map(key=>{
			if(key.startsWith("customer_units") || key.startsWith("contactable_data")
				|| key.startsWith("phones_data") || key.startsWith("company_data")
				|| key.startsWith("financial_data")){

				formatedObject[key.split(".")[0]] = {...formatedObject[key.split(".")[0]],[key.split(".")[1]] : formatedObject[key]};
				delete formatedObject[key];
			}
		})
		if(formatedObject.contactable_data){
			formatedObject.contactable_data = JSON.stringify(formatedObject.contactable_data);
		}
		if(formatedObject.phones_data){
			formatedObject.phones_data = JSON.stringify(formatedObject.phones_data);
		}
		if(formatedObject.company_data){
			formatedObject.company_data = JSON.stringify(formatedObject.company_data);
		}
		if(formatedObject.financial_data){
			formatedObject.financial_data = JSON.stringify(formatedObject.financial_data);
		}
		return formatedObject;
	}

	save = async (activeStep)=>{
		const {data_pribadi_form,data_pekerjaan_form,data_kontak_form,data_finansial_form,data_unit_form} = this.state
		if(this.props.openMode === 'create'){
			let data = this.changeFormatData({...data_pribadi_form,...data_pekerjaan_form,...data_kontak_form,...data_finansial_form,...data_unit_form});
			this.props.onSave(data)
		}
		else{
			this.global_ui.openLoader();
			let data = this.changeFormatData({...this.state[this.ref[activeStep]]});
			this.customerStore.update(this.state.id,data,false).then(async res=>{
	            this.global_ui.closeLoader();
	            if(activeStep === (this.steps.length -1)){
	            	this.props.onClose();
	            	this.props.actions.showMessage(
	            	    MessageBoxType.SUCCESS,
	            	    `Success edit customer`,
	            	    ()=>console.log('OK Clicked')
	            	);
	            }
	            else{
		            this.updateStep(activeStep + 1);
	            }
	        }).catch(err=>{
	            this.global_ui.closeLoader();
	            this.props.actions.showMessage(
	                MessageBoxType.DANGER,
	                err.message,
	                ()=>console.log('OK Clicked')
	            );
	        });
			// this.props.onClose();
		}
	};

	updateStep = (val)=>{
		this.setState({activeStep : val,isLoading : true});
		setTimeout(()=>{
			this.setState({isLoading : false})
		},5);
		if(!this.props.readonly){
			this.props.updateStep(val);
		}
	};

	async componentDidMount(){
		this.global_ui.openLoader();
		if(get(this.props,'match.params.id',false)){
	        await this.props.appstate.unit_verification.getDetail(this.props.match.params.id).catch(err=>{
				this.props.actions.showMessage(
	                MessageBoxType.DANGER,
	                err,
	                ()=>this.props.history.push('/login')
	            );
	        });
		}
        this.global_ui.closeLoader();
	}

	saveFormData = (key,val)=>{
		this.setState({[key] : val});
	}

	getStepContent = (stepIndex)=>{
		switch (stepIndex) {
			case 0:
				return this.state.isLoading ? <div></div> : <DataPribadi
					defaultValue={this.state.data_pribadi_form}
					getInstance={ref=>this.data_pribadi_form = ref}/>;
			case 1:
				// return <PartOne onUpdate={this.onUpdatePartOne} data={this.customerStore.selectedData.verification.test_result.filter(it=>it.type ==='five_option')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> : <DataPekerjaan
					defaultValue={this.state.data_pekerjaan_form}
					getInstance={ref=>this.data_pekerjaan_form = ref}/>;
			case 2:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> : <DataKontak
					defaultValue={this.state.data_kontak_form}
					getInstance={ref=>this.data_kontak_form = ref}/>;
			case 3:
				return this.state.isLoading ? <div></div> : <DataFinansial
					defaultValue={this.state.data_finansial_form}
					getInstance={ref=>this.data_finansial_form = ref}/>;
			case 4:
				return this.state.isLoading ? <div></div> : <DataUnit
					defaultValue={this.state.data_unit_form}
					getInstance={ref=>this.data_unit_form = ref}/>;
			default:
				return <div></div>;
		}
	};

	render(){
		const {activeStep} = this.state;
		const {classes} = this.props;
		return(
			<div>
				<Stepper activeStep={activeStep} alternativeLabel>
				{this.steps.map((label,index) => {
					return (
						<Step key={index}>
							<StepLabel>{label}</StepLabel>
						</Step>
					);
				})}
				</Stepper>
				<div>
					{
						this.getStepContent(activeStep)
					}
				</div>
				<div className={classes.stepButtons}>
					<Button variant="contained" color="primary" onClick={this.handleNext}>
						{activeStep === this.steps.length - 1 ? 'Save' : 'Next'}
					</Button>
					<Button
						onClick={this.handleBack}
						className={classes.backButton}
						disabled={activeStep === 0}
					>
						Back
					</Button>
				</div>
			</div>
		)
	}
}

CustomerForm.defaultProps = {
	updateStep : (val)=>{}
}

export default withStyles(styles)(CustomerForm);
