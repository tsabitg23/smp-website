import React from 'react';
import FormBuilder from '../../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
const styles={
	text : {color: '#000',marginBottom : '20px',marginTop : '20px'}
}
class DataPribadi extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : Object.assign({},props.defaultValue),
			formErrors : []
		}
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	jenisKelamin = [
	    {
	        value : 'male',
	        label : 'Laki-laki'
	    },
	    {
	        value : 'female',
	        label : 'Perempuan'
	    }
	];

	form_items = [
	    {
	        key : 'name',
	        label : 'Nama Lengkap (Sesuai KTP)',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'home_address',
	        label : 'Alamat rumah',
	        type: 'multiline',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'home_status',
	        label : 'Status rumah',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'id_card_type',
	        label : 'Tipe Identitas',
	        type: 'select',
	        items : [
	        	{
		        	value : 'KTP',
		        	label : "KTP"
	        	},
	        	{
		        	value : 'SIM',
		        	label : "SIM"
	        	},
	        	{
		        	value : 'PASPORT',
		        	label : "Passport"
	        	},
	        ],
	        disabled : this.props.readonly
	    },
	    {
	        key : 'id_card_number',
	        label : 'Nomor Identitas',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'birthplace',
	        label : 'Tempat Lahir',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'dob',
	        label : 'Tanggal Lahir',
	        type: 'date',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'gender',
	        label : 'Jenis Kelamin',
	        type: 'select',
	        items : this.jenisKelamin.map(it=>it),
	        disabled : this.props.readonly
	    },
	    {
	        key : 'marital_status',
	        label : 'Status Pernikahan',
	        type: 'select',
	        items : [
	        	{
		        	value : 'menikah',
		        	label : "Menikah"
	        	},
	        	{
		        	value : 'belum_menikah',
		        	label : "Belum Menikah"
	        	},
	        	{
		        	value : 'duda',
		        	label : "Duda"
	        	},
	        	{
		        	value : 'janda',
		        	label : "Janda"
	        	}
	        ],
	        disabled : this.props.readonly
	    },
	    {
	        key : 'religion',
	        label : 'Agama',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'phones_data.home',
	        label : 'Nomor Telepon Rumah',
	        type: 'number',
	        disabled : this.props.readonly,
	        required : false
	    },
	    {
	        key : 'phones_data.handphone',
	        label : 'Nomor Handphone',
	        type: 'number',
	        disabled : this.props.readonly,
	        required : false
	    },
	    {
	        key : 'phones_data.whatsapp',
	        label : 'Nomor WhatsApp',
	        type: 'number',
	        disabled : this.props.readonly,
	        required : false
	    },
	    {
	        key : 'email',
	        label : 'Email',
	        type: 'email',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'last_degree',
	        label : 'Pendidikan Terakhir',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	];

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){

		let defaultData = this.props.defaultValue;
		return (
			<div>
				<FormBuilder 
					forms={this.form_items} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
DataPribadi.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataPribadi;