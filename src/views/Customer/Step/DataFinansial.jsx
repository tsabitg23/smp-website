import React from 'react';
import FormBuilder from '../../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
const styles={
	text : {color: '#000',marginBottom : '20px',marginTop : '20px'}
}
class DataFinansial extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : Object.assign({},props.defaultValue),
			formErrors : []
		}
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	form_items = [
	    {
	        key : 'financial_data.monthly_income',
	        label : 'Pendapatan / Bulan',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.spouse_monthly_income',
	        label : 'Pendapatan Pasangan / Bulan',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.monthly_expense',
	        label : 'Pengeluaran / Bulan',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.other_installment_pay',
	        label : 'Angsuran lain. ke',
	        type: 'number',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.other_installment_total',
	        label : 'Angsuran lain. jumlah',
	        type: 'number',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.bank_account_number',
	        label : 'Nomor Rekening',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.bank_on_behalf',
	        label : 'Reking Atas Nama',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.bank_name',
	        label : 'Nama Bank',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'financial_data.dependents',
	        label : 'Jumlah tanggungan (orang)',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	];

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){

		let defaultData = this.props.defaultValue;
		return (
			<div>
				<FormBuilder 
					forms={this.form_items} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
DataFinansial.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataFinansial;