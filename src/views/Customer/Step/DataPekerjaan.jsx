import React from 'react';
import FormBuilder from '../../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';

class DataPekerjaan extends React.Component{
	constructor(props){
		super(props);
		let data = Object.assign({},props.defaultValue)
		if(get(props.defaultValue,'company_data',false)){
			Object.keys(props.defaultValue.company_data).map(key=>{
				data[`company_data.${key}`] = data.company_data[key];
			})
		}

		this.state = {
			formData : data,
			formErrors : []
		}
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                	console.log(this.state.formData)
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	jenisKelamin = [
	    {
	        value : 'male',
	        label : 'Laki-laki'
	    },
	    {
	        value : 'female',
	        label : 'Perempuan'
	    }
	];

	form_items = [
	    {
	        key : 'company_data.occupation',
	        label : 'Pekerjaan',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'company_data.company_name',
	        label : 'Nama Perusahaan',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'company_data.company_field',
	        label : 'Bidang Usaha',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'company_data.company_address',
	        label : 'Alamat Kantor',
	        type: 'multiline',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'company_data.company_phone',
	        label : 'Nomor telepon kantor',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'company_data.work_duration',
	        label : 'Lama Bekerja',
	        placeholder : "Contoh. 1 Tahun 2 Bulan",
	        type: 'text',
	        disabled : this.props.readonly
	    }
	];

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){

		let defaultData = this.props.defaultValue;
		return (
			<div>
				<FormBuilder 
					forms={this.form_items} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
DataPekerjaan.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataPekerjaan;