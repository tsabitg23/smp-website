import React from 'react';
import FormBuilder from '../../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
const styles={
	text : {color: '#000',marginBottom : '20px',marginTop : '20px'}
}
class DataKontak extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : Object.assign({},props.defaultValue),
			formErrors : []
		}
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	form_items = [
	    {
	        key : 'contactable_data.name',
	        label : 'Nama',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.relationship',
	        label : 'Hubungan',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.phone_home',
	        label : 'Nomor telepon rumah',
	        type: 'text',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.handphone',
	        label : 'Nomor handphone',
	        type: 'text',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.whatsapp',
	        label : 'Nomor whatsapp',
	        type: 'text',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.email',
	        label : 'Email',
	        type: 'email',
	        required : false,
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.mail_address',
	        label : 'Alamat surat menyurat',
	        type: 'multiline',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.address_status',
	        label : 'Status Rumah',
	        type: 'select',
	        items : [
	        	{
		        	value : 'rumah',
		        	label : "Rumah"
	        	},
	        	{
		        	value : 'kantor',
		        	label : "Kantor"
	        	},
	        	{
		        	value : 'lainnya',
		        	label : "Lainnya"
	        	}
	        ],
	        disabled : this.props.readonly
	    },
	    {
	        key : 'contactable_data.address',
	        label : 'Alamat (dengan kode pos)',
	        type: 'multiline',
	        disabled : this.props.readonly
	    }
	];

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){

		let defaultData = this.props.defaultValue;
		return (
			<div>
				<FormBuilder 
					forms={this.form_items} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
DataKontak.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataKontak;