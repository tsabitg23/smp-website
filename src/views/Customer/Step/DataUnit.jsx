import React from 'react';
import FormBuilder from '../../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import {inject, observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';

@inject('appstate')
@observer
class DataUnit extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : {},
			formData2 : {},
			formErrors : [],
			unitData : {}
		}
		this.globalUI = props.appstate.global_ui;
		this.unitStore = props.appstate.unit;
		this.paymentTypeStore = props.appstate.payment_type;
	}

	async componentDidMount(){
		await this.globalUI.openLoader();
        await this.unitStore.getAll().catch(err=>{
			this.globalUI.closeLoader();
        })
        await this.paymentTypeStore.getAll().catch(err=>{
			this.globalUI.closeLoader();
        })
        this.props.getInstance(this);
		await this.globalUI.closeLoader();
    }

    form_items = [
    	{
    	    key : 'customer_units.unit_id',
    	    label : 'Unit',
    	    type: 'autocomplete'
    	},
    	{
    	    key : 'customer_units.payment_type_id',
    	    label : 'Cara Pembayaran',
    	    type: 'select',
    	},
    	{
    	    key : 'customer_units.downpayment',
    	    label : 'DP Awal',
    	    type: 'number',
    	},
    	{
    	    key : 'customer_units.installment_per_month',
    	    label : 'Angsuran Perbulan',
    	    type: 'number',
    	}
    ];

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate({...this.state.formData,...this.state.formData2}, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve({...this.state.formData,...this.state.formData2});
                }
            });
        });
    };

	onFormUpdate = (formData)=>{
		let unitData = this.unitStore.data.find(it=>it.id === formData['customer_units.unit_id']);
        this.setState({formData});
		if(unitData){
			this.setState({unitData : unitData})
		}
    };
    onFormUpdate2 = (formData2)=>{
        this.setState({formData2});
    };


	render(){
	    let form_items = [
	        {
	            key : 'customer_units.unit_id',
	            label : 'Unit',
	            type: 'autocomplete',
	            items: this.unitStore.data.map(it=>{
	                return {
	                    value : it.id,
	                    label : it.kavling_number
	                };
	            }),
	        }
	    ];
	    let form_items2 = [
	        {
	            key : 'customer_units.payment_type_id',
	            label : 'Cara Pembayaran',
	            type: 'select',
	            items: this.paymentTypeStore.data.map(it=>{
	                return {
	                    value : it.id,
	                    label : it.name
	                };
	            }),
	        },
	        {
	            key : 'customer_units.downpayment',
	            label : 'DP Awal',
	            type: 'number',
	        },
	        {
	            key : 'customer_units.installment_per_month',
	            label : 'Angsuran Perbulan',
	            type: 'number',
	        }
	    ];

		let defaultData = this.props.defaultValue;
		return (
			<div>
				<FormBuilder 
					forms={form_items} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
				<TextField
					id={'lb'}
					label={'Luas Bangunan'}
					value={get(this.state.unitData,'building_area','')}
					margin="normal"
					disabled={true}
					type={'text'}
					fullWidth
					// error={get(this.state,`${form.key}-error`,false)}
					// helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
				<TextField
					id={'lt'}
					label={'Luas Tanah'}
					value={get(this.state.unitData,'land_area','')}
					margin="normal"
					disabled={true}
					type={'text'}
					fullWidth
					// error={get(this.state,`${form.key}-error`,false)}
					// helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
				<FormBuilder 
					forms={form_items2} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate2}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
DataUnit.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataUnit;