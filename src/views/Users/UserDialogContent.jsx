import React from 'react';
import Table from '../../components/Table/index';
import {inject, observer,Observer} from 'mobx-react';
import {startCase,get,upperCase} from 'lodash';
import Checkbox from '@material-ui/core/Checkbox';

@inject('appstate')
@observer
class UserDialogContent extends React.Component{
    constructor(props) {
        super(props);
        this.permission = props.appstate.permission;
        this.userStore = props.appstate.user;
    }

    async componentDidMount(){
        this.props.appstate.global_ui.openLoader();
        await this.permission.getAll();
        this.props.appstate.global_ui.closeLoader();
    }

    isChecked = (per)=>{
        if(this.props.mode !== 'edit'){
            return {
                create : false,
                update : false,
                read : false,
                delete : false
            };
        }
        if(this.userStore.selectedData.permissions){
            let data = this.userStore.selectedData.permissions.find(it=>it.permission_id === per.id);
            return {
                create : get(data,'create',false) || false,
                read : get(data,'read',false) || false,
                update : get(data,'update',false) || false,
                delete : get(data,'delete',false) || false,
            };
        }
        else{
            return {
                create : false,
                update : false,
                read : false,
                delete : false
            };
        }
    };

    get dataPermission(){
        return this.permission.data.map(it=>{
            let permissions = this.isChecked(it);
            let name = it.name.split("_");
            let role = name.splice(0,1);

            if(role == 'mr'){
                role = 'Pimpinan Project';
            }

            return {
                id : it.id,
                name : `${upperCase(role)} - ${startCase(name.join("_"))}`,
                key : it.key,
                create : <Checkbox color="primary" defaultChecked={permissions.create} onChange={(event)=>this.props.onCheckPermission(it.id,'create',event.target.checked)}/>,
                read : <Checkbox color="primary" defaultChecked={permissions.read} onChange={(event)=>this.props.onCheckPermission(it.id,'read',event.target.checked)}/>,
                update : <Checkbox color="primary" defaultChecked={permissions.update} onChange={(event)=>this.props.onCheckPermission(it.id,'update',event.target.checked)}/>,
                delete : <Checkbox color="primary" defaultChecked={permissions.delete} onChange={(event)=>this.props.onCheckPermission(it.id,'delete',event.target.checked)}/>,
            };
        });
    }

    render(){
        const configActionColumns = [];
        let headerColumn=[
                {label : 'Permission',key : 'name'},
                {label : 'Read',key : 'read'},
                {label : 'Create',key : 'create'},
                {label : 'Update',key : 'update'},
                {label : 'Delete',key : 'delete'},
            ];

        return(
                <Table
                    actionColumns={configActionColumns}
                    tableHeaderColor="primary"
                    tableHead={headerColumn}
                    tableData={this.dataPermission}
                    pagination={false}
                    stripped={true}
                />
        );
    }
}
export default UserDialogContent;