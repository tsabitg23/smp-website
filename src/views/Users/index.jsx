import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import LockOpen from '@material-ui/icons/LockOpen';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import UserDialog from '../../components/Dialog';
import UserDialogContent from './UserDialogContent';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import {constant} from '../../../configs/const';
import ChangePasswordComponent from './ChangePassword';
import TopStatus from '../../components/TopStatus';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Users extends React.Component{
    constructor(props){
        super(props);
        this.userStore = props.appstate.user;
        this.roleStore = props.appstate.role;
        this.userData = props.appstate.userData;
        this.projectStore = props.appstate.project;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            roles : [],
            permissions : [],
            defaultValue : {},
            selectedUserId : null,
            isChangePasswordOpen : false,
            user_permission : props.appstate.getPermission('users')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.userStore.getAll();
        await this.roleStore.getAll();
        await this.projectStore.getAll();
        this.global_ui.closeLoader();
    }

    // onEditClick = async (rowData) => {
        // console.log(rowData);

    // };

    onEditClick = async (rowData)=>{
        this.global_ui.openLoader();
        await this.userStore.getDetail(rowData.id);
        this.global_ui.closeLoader();
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : this.userStore.selectedData});
    };

    onChangePassword = async (rowData)=>{
        this.setState({isChangePasswordOpen : true,selectedUserId : rowData.id});
    }

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please add it first',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        console.log(this.userData);
        if(rowData.id === this.userData.user_id){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'You cant delete your own account',
                ()=>console.log('OK Clicked')
            );
            return;
        }
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this user?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.userStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.userStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete user',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        let permissions_data = this.state.permissions;
        
        if(this.state.openMode === 'edit'){
            // permissions_data.map(it=>{
            //     let userPer = this.userStore.selectedData.permissions.find(per=>per.permission_id === it.permission_id);
            //     if(!userPer){
            //         return it
            //     }
            //     it.create = get(it,'create',!!userPer.create);
            //     it.read = get(it,'read',!!userPer.read);
            //     it.update = get(it,'update',!!userPer.update);
            //     it.delete = get(it,'delete',!!userPer.delete);
            //     return it;
            // })
            this.userStore.selectedData.permissions.map(it=>{
                let index_data = permissions_data.findIndex(per=>per.permission_id === it.permission_id);
                if(index_data > -1){
                    permissions_data[index_data].create = get(permissions_data[index_data],'create',!!it.create);
                    permissions_data[index_data].read = get(permissions_data[index_data],'read',!!it.read);
                    permissions_data[index_data].update = get(permissions_data[index_data],'update',!!it.update);
                    permissions_data[index_data].delete = get(permissions_data[index_data],'delete',!!it.delete);
                }
                else {
                    permissions_data.push({
                        permission_id : it.permission_id,
                        create : it.create,
                        read : it.read,
                        update : it.update,
                        delete : it.delete,
                    });
                }
            });
        }

        let res = this.state.openMode === 'create' ? 
            this.userStore.create(
                Object.assign({permissions: permissions_data},data)
            ) : 
            this.userStore.update(
                data.id,
                Object.assign({permissions: permissions_data},data)
            );
            
        res.then(async res=>{
            this.onDialogClose();
            await this.userStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} user`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    onCheckPermission = (id,key,val)=>{
        let data = this.state.permissions;
        let dataIndex = data.findIndex(it=>it.permission_id === id);
        if(dataIndex > -1){
            data[dataIndex][key] = val;
        }
        else{
            data.push({
                permission_id : id,
                [key] : val
            });
        }
        // console.log(data);
        // this.setState({permissions : data});
    };

    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick,key : 'update' },
            { Icon: LockOpen, Tooltip: 'Change Password', Color: 'primary', Callback: this.onChangePassword,key : 'update' },
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.user_permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>,
        ];

        let form_items = [
            {
                key : 'email',
                label : 'Email',
                type: 'email'
            },
            {
                key : 'password',
                label : 'Password',
                type: 'password'
            },
            {
                key : 'role_id',
                label : 'Role',
                type: 'select', 
                items: this.roleStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                })
            },
            {
                key : 'projects',
                label : 'Projects',
                type : 'autocomplete_multi',
                placeholder : 'Select multiple project',
                items : this.projectStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    }
                })
            }
        ];

        if(this.state.openMode === 'edit'){
            form_items = form_items.filter(it=>it.key !== 'password');
        }

        if(this.state.defaultValue.role_id === constant.ROLES.ADMIN){
            form_items = form_items.filter(it=>it.key !== 'role_id');
        }

        let topStatusData = [{
            type : 'count',
            title : "Users",
            icon : "people",
            subtitle : "Total users",
            subtitle_icon : "assignment",
            value : this.userStore.data.length
        }]

        return  (
            <div>
                <UserDialog 
                    open={this.state.isDialogOpen} 
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} User`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                >
                    {
                        this.state.defaultValue.role_id !== constant.ROLES.ADMIN &&
                        <UserDialogContent mode={this.state.openMode} onCheckPermission={this.onCheckPermission} value={this.state.defaultValue}/>
                    }
                </UserDialog>
                <ChangePasswordComponent
                    open={this.state.isChangePasswordOpen}
                    user_id={this.state.selectedUserId}
                    onClose={()=>this.setState({isChangePasswordOpen : false})}
                    title={"Change User Password"}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'User list'}
                    subtitle={'List of user who have access to the system'}
                    headerColumn={[
                        {label : 'Email',key : 'email'},
                        {label : 'Role',key : 'role.name'},
                        {label : 'Created date',key : 'created_at'}]
                    }
                    actionColumn={configActionColumns.filter(it=>this.state.user_permission[it.key])}
                    data={this.userStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default Users;
