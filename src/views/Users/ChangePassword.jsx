import React from 'react';
import DialogOnly from '../../components/Dialog/DialogOnly';
import Button from '@material-ui/core/Button';
import FormBuilder from '../../components/FormBuilder';
import schema from 'async-validator';
import {inject, observer} from 'mobx-react';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
export default class ChangePassword extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			formData : {},
			user_id : null,
			formErrors : [],
			open : this.props.open,
			title : this.props.title
		}
		this.authStore = props.appstate.auth;
        this.global_ui = props.appstate.global_ui;
	}

	componentDidUpdate(prevProps) {
    	let change = {};
	    if (this.props.open !== prevProps.open) {
	      change.open = this.props.open;
	    }

	    if (this.props.title !== prevProps.title) {
	      change.title = this.props.title;
	    }

	    if (this.props.user_id !== prevProps.user_id) {
	      change.user_id = this.props.user_id;
	    }

	    if(Object.keys(change).length > 0){
	        this.setState(change);
	    }
	}

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    handleSave = ()=>{
    	let rules = {
            new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ],
            confirm_new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ]
        };

        const validator = new schema(rules);
        validator.validate(this.state.formData, (errs, f) => {
            this.setState({formErrors : errs ? errs : []});
            if (errs) {
                console.log(errs);
            } else {
                if(this.state.formData.new_password !== this.state.formData.confirm_new_password){
                    this.setState({formErrors : [{message: 'New password and confirm did not match', field: 'confirm_new_password'}]});
                }
                else{
                    this.global_ui.openLoader();
                    this.authStore.changePasswordAdmin(Object.assign({user_id : this.state.user_id},this.state.formData)).then(async res=>{
                        this.setState({resetForm : true});
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.SUCCESS,
                            'Success change password',
                            ()=>this.props.onClose()
                        );
                    }).catch(err=>{
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.DANGER,
                            err.message,
                            ()=>console.log('OK Clicked')
                        );
                    });
                }
            }
        });
    }

	render(){
		let actions = [
			<Button color="inherit" onClick={this.handleSave}>
                save
          	</Button>
		];

		const form_items = [
		    {
		        key : 'new_password',
		        label : 'New Password',
		        type: 'password'
		    },
		    {
		        key : 'confirm_new_password',
		        label : 'Confirm New Password',
		        type : 'password'
		    }
		];

		return (
			<DialogOnly
			    open={this.state.open}
			    title={this.state.title}
			    onClose={this.props.onClose}
			    actions={actions}
			>
				<FormBuilder 
					forms={form_items} 
					onFormUpdate={this.onFormUpdate} 
					formErrors={this.state.formErrors}/>
			</DialogOnly>
		)
	}
}