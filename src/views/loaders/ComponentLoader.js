import * as React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
var loading = function () { return React.createElement(LinearProgress, { style: { color: '#4caf50' } }); };
export default loading;
