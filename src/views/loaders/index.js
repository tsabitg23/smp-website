import Loadable from 'react-loadable';
import loading from './ComponentLoader';
var Loader = function (loader) {
    return Loadable({
        loader: loader,
        loading: loading,
        timeout: 10000,
        /* devblock:start */
        delay: 1000
        /* devblock:end */
    });
};
export default Loader;
