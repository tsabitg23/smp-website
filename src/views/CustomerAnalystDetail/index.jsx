import React from 'react';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import Fab from '@material-ui/core/Fab';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {inject, observer} from 'mobx-react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {LINKS} from '../../routes/index';
import EditIcon from '@material-ui/icons/Edit';
import Table from '../../components/Table/index';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import FormBuilder from '../../components/FormBuilder';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { constant } from '../../../configs/const';
import EventFormComponent from '../Events/EventFormComponent';
import Form from "../CustomerAnalyst/Form";
import TableHorizontal from "../../components/Table/TableHorizontal";
import moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
export default class CustomerAnalystDetail extends React.Component{
    constructor(props){
        super(props);
        this.customerAnalystStore = props.appstate.customer_analyst;
        this.unitStore = props.appstate.unit;
        this.global_ui = props.appstate.global_ui;
        const permission = props.appstate.getPermission('customer_analyst');
        this.state = {
            hrd_manager_permission : {},
            permission : permission,
            openEditDialog : false,
            openRejectDialog : false,
            formErrors : [],
            rejectedKey : null,
            formData : {
                desc : ''
            },
            openEditCommentDialog: false
        };

    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.customerAnalystStore.getDetail(this.props.match.params.id);
        await this.unitStore.getUnitByCustomer(this.customerAnalystStore.selectedData.customer_id);
        this.setState({
            hrd_manager_permission : this.props.appstate.getPermission('manager_hrd'),
        })
        this.global_ui.closeLoader();
    }


    onClickBack = ()=>{
        this.props.history.push(LINKS.CUSTOMER_ANALYST);
    };

    getValueColor(status){
        if(status === 'approved' || status){
            return 'success'
        }
        else if(status === 'rejected' || status === false){
            return 'danger'
        }
        else{
            return ''
        }
    }



    closeDialogEdit = async ()=>{
        this.global_ui.openLoader();
        await this.customerAnalystStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
        this.setState({openEditDialog : false})
    }

    saveAnalyst = async (data)=>{
        this.global_ui.openLoader();
        const reqBody = data;
        let res = this.customerAnalystStore.update(reqBody.id, reqBody, false);
        res.then(async res=>{
            this.setState({
                openEditCommentDialog : false,
                openEditDialog : false
            });
            await this.customerAnalystStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    onClickApprove = key=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to approved this request?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.saveAnalyst({[key] : true, id: this.props.match.params.id});
                }
            }
        );
    };

    onClickReject = key => {
        this.setState({
            rejectedKey : key,
            openRejectDialog : true
        });
    };

    reject = () =>{
        this.setState({
            openRejectDialog: false
        });
        this.saveAnalyst({
            [this.state.rejectedKey] : false,
            manager_desc : this.state.formData.manager_desc,
            id : this.props.match.params.id
        });
    };

    closeDialogReject = ()=>this.setState({openRejectDialog : false});

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    debtorColumn = [
        {
            key : 'debtor_dob',
            label : 'Tanggal Lahir Debitor',
            type: 'date'
        },
        {
            key : 'spouse_name',
            label : 'Nama Istri',
            type: 'text'
        },
        {
            key : 'occupation',
            label : 'Perkerjaan',
            type: 'text'
        },
        {
            key : 'spouse_occupation',
            label : 'Pekerjaan Istri',
            type: 'text'
        },
        {
            key : 'work_duration',
            label : 'Lama bekerja',
            type: 'text'
        },
        {
            key : 'spouse_work_duration',
            label : 'Lama istri bekerja',
            type: 'text'
        },
        {
            key : 'children',
            label : 'Jumlah anak',
            type: 'text'
        },
        {
            key : 'payroll_desc',
            label : 'Deskripsi Gaji',
            type: 'text'
        },
        {
            key : 'debtor_asset',
            label : 'Aset',
            type: 'text'
        },
    ];

    render(){
        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];

        if(this.state.permission.update){
            tools.push(
                <Fab onClick={()=>{this.setState({openEditDialog : true})}} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                    <EditIcon />
                </Fab>
            )
        }

        let actions = [
            <Button color="inherit" onClick={this.reject}>
                save
            </Button>
        ];

        let dataDebitor = this.debtorColumn.map(it=>{
                const val = get(this.customerAnalystStore.selectedData,it.key,'');
                it.value = val || "-";
                return it;
        })

        let mainDataColumn  = [
            {
                key : 'akad_date',
                label : 'Tanggal Akad',
                type: 'date'
            },
            {
                key : 'customer.name',
                label : 'Nama Debitur',
                type: 'text',
            },
            {
                key : 'unit.kavling_number',
                label : 'Unit',
                type: 'text',
            },
            {
                key : 'recommendation',
                label : 'Rekomendasi',
                type: 'text',
            },
            {
                key : 'manager_approve_status',
                label : 'Approve Manager',
                type : 'select',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.customerAnalystStore.selectedData,'manager_approve_status','')),
            },
            {
                key : 'manager_desc',
                label : 'Keterangan Manager',
                type : 'text'
            },
            {
                key : 'unit_price',
                label : 'Harga Unit',
                type: 'number'
            },
            {
                key : 'payment_scheme',
                label : 'Skema Bayar',
                type: 'text',
            },
            {
                key : 'tenor',
                label : 'Lama Bulan',
                type: 'number'
            },
            {
                key : 'downpayment',
                label : 'Total DP',
                type: 'number'
            },
            {
                key : 'first_downpayment',
                label : 'DP Pertama',
                type: 'number'
            },
            {
                key : 'next_downpayment',
                label : 'DP Selanjutnya',
                type: 'number'
            },
            {
                key : 'next_dp_payment',
                label : 'Jumlah Angsuran DP',
                type: 'number'
            },
            {
                key : 'dp_count',
                label : 'Jumlah DP Sesuai Angsuran',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'next_downpayment'
                },{
                    operator : 'div',
                    key : 'next_dp_payment'
                }],
                placeholder: ''
            },
            {
                key : 'total_price',
                label : 'Harga Total Jual',
                type: 'number'
            },
            {
                key : 'total_debt',
                label : 'Nominal Hutang',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_price'
                },{
                    operator : 'sub',
                    key : 'downpayment'
                }],
                placeholder: ''
            },
            {
                key : 'installment',
                label : 'Jumlah Angsuran',
                type: 'number'
            },
            {
                key : 'total_installment',
                label : 'Total Nominal Angsuran',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_price'
                },{
                    operator : 'sub',
                    key : 'downpayment'
                },{
                    operator : 'div',
                    key : 'installment'
                }],
                placeholder: ''
            },
            {
                key : 'mortgage_history',
                label : 'Riwayat Pinjaman',
                type: 'select',
                items : [
                    {
                        value : 'Pernah Meminjam',
                        label : 'Pernah Meminjam'
                    },
                    {
                        value : 'Tidak Pernah',
                        label : 'Tidak Pernah'
                    }
                ],
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type: 'multiline'
            },
        ];

        let mainData = mainDataColumn.map(it=>{

                    if(it.key.includes('status')){
                        let val = it.value = get(this.customerAnalystStore.selectedData,it.key,'');
                        if(val){
                            it.value = 'Approved';
                        } else if(val === false){
                            it.value = 'Revisi';
                        } else{
                            it.value = 'Waiting';
                        }
                    }
                    else{
                        it.value = get(this.customerAnalystStore.selectedData,it.key,'') || "-";
                    }
                    return it;
        })

        // let dataFundsColumn = [
        //     {
        //         key : 'number',
        //         label : "No"
        //     },
        //     {
        //         key : 'name',
        //         label : "Nama"
        //     },
        //     {
        //         key : 'amount',
        //         label : "Biaya"
        //     },
        //     {
        //         key : 'desc',
        //         label : "Keterangan"
        //     }
        // ];
        //
        let account_column = [
            {
                key : 'number',
                placeholder: '',
                label : 'No',
                type: 'text'
            },
            {
                key : 'bank_name',
                placeholder: '',
                label : 'Nama Bank',
                type: 'text'
            },
            {
                key : 'month',
                placeholder: 'exp: agustus 2018',
                label : 'Bulan - Tahun',
                type: 'text'
            },
            {
                key : 'start_balance',
                placeholder: '',
                label : 'Saldo Awal',
                type: 'number'
            },
            {
                key : 'credit',
                placeholder: '',
                label : 'Mutasi Kredit',
                type: 'number'
            },
            {
                key : 'debit',
                placeholder: '',
                label : 'Mutasi Debit',
                type: 'number'
            },
            {
                key : 'end_balance',
                placeholder: '',
                label : 'Saldo Akhir',
                type: 'number'
            },
        ];
        //
        // let dataFunds = get(this.customerAnalystStore.selectedData,'funds',[]).map((prop, index)=>{
        //     return {
        //         number : index+1,
        //         ...prop
        //     }
        // });
        //
        let dataAccount = (get(this.customerAnalystStore.selectedData,'accounts') || []).map((prop, index)=>{
            return {
                number : index+1,
                ...prop
            }
        });

        const summaryAccount = [{
            key:'total',
            name : 'Total',
            operation : 'add',
            column_index : [3,4]
        },{
            key:'rata_rata',
            name : 'Rata-rata',
            operation : 'mean',
            column_index : [3,4,5]
        }];


        return (
            <Grid container spacing={24}>
                <Grid item xs={12}>
                    <DialogForm
                        open={this.state.openEditDialog}
                        title={'Edit Analisa'}
                        onClose={()=>this.closeDialogEdit()}
                        forms={[]}
                        hidesafebutton={true}
                        onSave={this.saveAnalyst}
                        value={this.customerAnalystStore.selectedData}
                    >
                        <Form defaultValue={this.customerAnalystStore.selectedData} openMode={'edit'} onSave={this.saveAnalyst} onClose={()=>this.closeDialogEdit()}/>
                    </DialogForm>
                    <GenericHorizontal
                        title={`Analisa Customer ${get(this.customerAnalystStore.selectedData,'id','-').split('-')[0]}`}
                        subtitle={`Detail analisa customer`}
                        data={mainData}
                        tools={tools}
                    >
                        <div>
                            <h5>Data Debitor</h5>
                            <TableHorizontal
                                data={dataDebitor}/>
                        </div>
                        <div>
                             <h5>Mutasi Rekening</h5>
                            <Table
                                    actionColumns={[]}
                                    tableHeaderColor="primary"
                                    tableHead={account_column}
                                    tableData={dataAccount}
                                    pagination={false}
                                    summary={summaryAccount}
                                    dataHeaderActive={true}
                                    dataHeaderColor={"#a5c7ff"}
                             />
                        </div>
                        {/*<div>*/}
                        {/*    <h5>A. Biaya Teknis</h5>*/}
                        {/*    <Table*/}
                        {/*        actionColumns={[]}*/}
                        {/*        tableHeaderColor="primary"*/}
                        {/*        tableHead={dataFundsColumn}*/}
                        {/*        tableData={dataFunds}*/}
                        {/*        pagination={false}*/}
                        {/*        dataHeaderActive={true}*/}
                        {/*        dataHeaderColor={"#a5c7ff"}*/}
                        {/*    />*/}

                        {/*    <h5>B. Crew</h5>*/}
                        {/*    <Table*/}
                        {/*        actionColumns={[]}*/}
                        {/*        tableHeaderColor="primary"*/}
                        {/*        tableHead={dataCrewColumn}*/}
                        {/*        tableData={dataCrew}*/}
                        {/*        pagination={false}*/}
                        {/*        dataHeaderActive={true}*/}
                        {/*        dataHeaderColor={"#a5c7ff"}*/}
                        {/*    />*/}
                        {/*</div>*/}
                    </GenericHorizontal>

                    {
                        (this.state.hrd_manager_permission.update && get(this.customerAnalystStore.selectedData,'manager_approve_status','') !== true )  &&

                        <Grid item xs={12} >
                            <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                Pimpinan Project
                            </Typography>
                            <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('manager_approve_status')} style={{marginRight : '20px'}}>
                                Approve
                            </Button>
                            {
                                (get(this.customerAnalystStore.selectedData,'manager_approve_status','') === null) &&
                                <Button color="secondary" onClick={()=>this.onClickReject('manager_approve_status')}>
                                    Reject
                                </Button>
                            }
                        </Grid>

                    }
                </Grid>
                <Dialog
                    open={this.state.openRejectDialog}
                    title={'Isi Keterangan'}
                    onClose={this.closeDialogReject}
                    actions={actions}
                >
                    <FormBuilder forms={[{
                        key : 'manager_desc',
                        label : 'Keterangan',
                        type : 'multiline',
                        placeholder : 'Alasan ditolak atau revisi'
                    }]} value={{}} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                </Dialog>
            </Grid>
        )
    }
}
