import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import accountStyle from './styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography/Typography';
import Button from '@material-ui/core/Button';
import FormBuilder from '../../components/FormBuilder';
import {get} from 'lodash';
import schema from 'async-validator';
import {inject, observer} from 'mobx-react';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';

const form_items = [
    {
        key : 'current_password',
        label : 'Current Password',
        type: 'password'
    },
    {
        key : 'new_password',
        label : 'New Password',
        type: 'password'
    },
    {
        key : 'confirm_new_password',
        label : 'Confirm New Password',
        type : 'password'
    }
];

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class ChangePassword extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            formErrors : [],
            formData : {},
            resetForm : false
        };
        this.authStore = props.appstate.auth;
        this.global_ui = props.appstate.global_ui;
    }

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    handleSave = ()=>{
        let rules = {
            current_password : [{
                required : true,
                type : 'string'
            }],
            new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ],
            confirm_new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ]
        };

        const validator = new schema(rules);
        validator.validate(this.state.formData, (errs, f) => {
            this.setState({formErrors : errs ? errs : []});
            if (errs) {
                console.log(errs);
            } else {
                if(this.state.formData.new_password !== this.state.formData.confirm_new_password){
                    this.setState({formErrors : [{message: 'New password and confirm did not match', field: 'confirm_new_password'}]});
                }
                else{
                    this.global_ui.openLoader();
                    this.authStore.changePassword(this.state.formData).then(async res=>{
                        this.setState({resetForm : true});
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.SUCCESS,
                            'Success change password',
                            ()=>console.log('OK Clicked')
                        );
                    }).catch(err=>{
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.DANGER,
                            err.message,
                            ()=>console.log('OK Clicked')
                        );
                    });
                }
            }
        });
    };

    render(){
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12} md={4}>
                        <Typography variant="h5" gutterBottom>
                            Password
                        </Typography>
                        <Typography variant={'caption'} gutterBottom>Change your password with a new one</Typography>
                    </Grid>
                    <Grid item xs={12} md={8}>
                        <Typography variant={'caption'} gutterBottom>Fill this form to change password</Typography>
                        <FormBuilder forms={form_items} reset={this.state.resetForm} afterReset={()=>this.setState({reset: false})} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                        <Button variant="contained" color="primary" className={classes.changePasswordSave} onClick={this.handleSave}>Save</Button>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(accountStyle)(ChangePassword);