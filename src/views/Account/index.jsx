import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import accountStyle from './styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ChangePassword from './change_password';

class AccountPage extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <ChangePassword/>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(accountStyle)(AccountPage);