import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../MRProjectJob/styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Edit from '@material-ui/icons/Edit';
import Dialog from '../../components/Dialog/DialogOnly';
import DialogForm from '../../components/Dialog';
import Typography from '@material-ui/core/Typography';
import FormBuilder from '../../components/FormBuilder';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class LaporanKomisiDetail extends React.Component {
    constructor(props){
        super(props);
        this.commissionReportStore = props.appstate.commission_report;
        this.commissionReqStore = props.appstate.commission_request;
        this.global_ui = props.appstate.global_ui;
        this.unitStore = props.appstate.unit;
        this.agentStore = props.appstate.agent;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            formErrors : [],
            defaultValue : {},
            dialogEditOpen : false,
            marketing_manager_permission : {},
            keuangan_manager_permission : {},
            pimpro_manager_permission : {},
            permission : {},
            rejectedKey : null,
            formData : {
              desc : ''
            },
            openRejectDialog : false
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.commissionReportStore.getDetail(this.props.match.params.id);
        this.setState({
            defaultValue : this.commissionReportStore.selectedData,
            marketing_manager_permission : this.props.appstate.getPermission('manager_marketing'),
            keuangan_manager_permission : this.props.appstate.getPermission('manager_keuangan'),
            pimpro_manager_permission : this.props.appstate.getPermission('manager_pimpro'),
            permission : this.props.appstate.getPermission(['komisi','manager_marketing']),
        });
        this.global_ui.closeLoader();
    }

    getValueColor(status){
        if(status === 'approved' || status){
            return 'success';
        }
        else if(status === 'rejected' || status === false){
            return 'danger';
        }
        else{
            return '';
        }
    }

    get detailData(){

        let column = [
            {
                key : 'commission_request.agent.name',
                label : 'Agent',
                type: 'text'
            },
            {
                key : 'commission_request.agent.agency.name',
                label : 'Agency',
                type: 'text'
            },
            {
                key : 'commission_request.unit.kavling_number',
                label : 'Unit',
                type: 'text'
            },
            {
                key : 'commission_request.amount',
                label : 'Total Nilai Komisi',
                type: 'text'
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type: 'text'
            },
            {
                key : 'marketing_approval_status',
                label : 'Approve Manager Marketing',
                type : 'text',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.commissionReportStore.selectedData,'marketing_approval_status',''))
            },
            {
                key : 'marketing_desc',
                label : 'Keterangan Marketing',
                type : 'text',
            },
            {
                key : 'finance_approval_status',
                label : 'Approve Manager Keuangan',
                type : 'text',
                labelBold : true,
                valueBold : true,
                valueColor: this.getValueColor(get(this.commissionReportStore.selectedData,'finance_approval_status',''))
            },
            {
                key : 'finance_desc',
                label : 'Keterangan Keuangan',
                type : 'text',
            },
        ];

        const marketingApproval = get(this.commissionReportStore.selectedData,'marketing_approval_status','');
        const financeApproval = get(this.commissionReportStore.selectedData,'finance_approval_status','');
        if(marketingApproval !== false){
            column = column.filter(prop=>prop.key !== 'marketing_desc');
        }
        if(financeApproval !== false){
            column = column.filter(prop=>prop.key !== 'finance_desc');
        }
        return column.map(it=>{
            if(it.key.includes('status')){
                let val = it.value = get(this.commissionReportStore.selectedData,it.key,'');
                if(val){
                    it.value = 'Approved';
                } else if(val === false){
                    it.value = 'Revisi';
                } else if(val === null){
                    it.value = 'Waiting';
                }
            }
            else{
                it.value = get(this.commissionReportStore.selectedData,it.key,'');
            }
            return it;
        });
    }

    saveEdit =  (data)=>{
        this.global_ui.openLoader();
        let res = this.commissionReportStore.update(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.commissionReportStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.setState({
                dialogEditOpen : false
            })
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.setState({
                dialogEditOpen : false
            })
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.commissionReportStore.update(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.commissionReportStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success edit',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }


    onClickBack = ()=>{
        if(this.state.permission.create){
            this.props.history.push(LINKS.COMMISSION_REPORT);
        }
        else{
            this.props.history.push(LINKS.COMMISSION_REPORT);
        }
    };

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    onClickApprove = key=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to approved this request?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.save({[key] : true});
                }
            }
        );
    };

    onClickReject = key => {
        this.setState({
            rejectedKey : key,
            openRejectDialog : true
        });
    };

    reject = () =>{
        this.setState({
            openRejectDialog: false
        });
        this.save({
            [this.state.rejectedKey+'_approval_status'] : false,
            [this.state.rejectedKey+'_desc'] : this.state.formData.desc
        });
    };

    closeDialogReject = ()=>this.setState({openRejectDialog : false});

    onDialogClose = ()=>this.setState({isDialogOpen : false});
    
    editClick = async (val) => {
        this.global_ui.openLoader();
        await this.commissionReqStore.getAll();
        this.setState({
            dialogEditOpen : true
        })
        this.global_ui.closeLoader();
    }

    render(){
        const {classes} = this.props;

        let form_edit = [
            {
                key : 'commission_request_id',
                label : 'No Pengajuan',
                type: 'autocomplete',
                items: this.commissionReqStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.code,
                        agent_name : (it.agent) ? it.agent.name : '-',
                        agency_name : (it.agent.agency) ? it.agent.agency.name : '-',
                        unit_number : (it.unit) ? it.unit.kavling_number : '-',
                        amount : (it.amount) ? it.amount : '-',
                        code : (it.code) ? it.code : '-'
                    };
                }),
                // empty_key : ['unit_id','unit_id-autocomplete'],
                // onChange : this.onChangeAgentData
            },
            {
                key : 'code',
                label : 'Nama Pengajuan',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'kode pengajuan',
                data_key : 'code'
            },
            {
                key : 'agent_id',
                label : 'Agent',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Nama Agent',
                data_key : 'agent_name'
            },
            {
                key : 'agency_id',
                label : 'Agency',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Nama Agency',
                data_key : 'agency_name'
            },
            {
                key : 'amount',
                label : 'Total Nilai',
                type: 'autofill',
                source_index : 0,
                source_key : 'commission_request_id',
                placeholder: 'Total nilai komisi',
                data_key : 'amount'
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type : 'multiline'
            },
        ];
        

        let tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];
        if(this.state.permission.update){
            tools = tools.concat([
                <Fab onClick={this.editClick} style={{display : 'inherit'}}  color="default" aria-label="Edit">
                    <Edit />
                </Fab>
            ])
        }

        let actions = [
            <Button color="inherit" onClick={this.reject}>
                save
            </Button>
        ];

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <GenericHorizontal
                            title={`Laporan Pendistribusian komisi ${get(this.commissionReportStore.selectedData,'code','cbc6c7f0').split('-')[0]}`}
                            subtitle={'Pendistribusian komisi untuk agent'}
                            data={this.detailData}
                            tools={tools}
                        >
                            <Grid container spacing={24} style={{marginTop:'20px'}}>
                            {
                                (this.state.marketing_manager_permission.update && get(this.commissionReportStore.selectedData,'marketing_approval_status','') !== true)  &&

                                    <Grid item xs={12} >
                                        <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                            Manager Marketing
                                        </Typography>
                                        <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('marketing_approval_status')} className={classes.buttonApprove}>
                                            Approve
                                        </Button>
                                        {
                                            (get(this.commissionReportStore.selectedData,'marketing_approval_status','') === null) &&
                                            <Button color="secondary" onClick={()=>this.onClickReject('marketing')} className={classes.button}>
                                                Reject
                                            </Button>
                                        }
                                    </Grid>

                            }

                                {
                                    (this.state.keuangan_manager_permission.update && get(this.commissionReportStore.selectedData,'marketing_approval_status','') === true && get(this.commissionReportStore.selectedData,'finance_approval_status','') !== true )  &&

                                    <Grid item xs={12} >
                                        <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                            Manager Keuangan
                                        </Typography>
                                        <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('finance_approval_status')} className={classes.buttonApprove}>
                                            Approve
                                        </Button>
                                        {
                                            (get(this.commissionReportStore.selectedData,'finance_approval_status','') === null) &&
                                            <Button color="secondary" onClick={()=>this.onClickReject('finance')} className={classes.button}>
                                                Reject
                                            </Button>
                                        }
                                    </Grid>

                                }

                                {
                                    // (this.state.pimpro_manager_permission.update && get(this.commissionReportStore.selectedData,'manager_approval_status','') !== true )  &&

                                    // <Grid item xs={12} >
                                    //     <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                    //         Pimpinan Project
                                    //     </Typography>
                                    //     <Button variant="contained" color="primary" onClick={()=>this.onClickApprove('manager_approval_status')} className={classes.buttonApprove}>
                                    //         Approve
                                    //     </Button>
                                    //     {
                                    //         (get(this.commissionReportStore.selectedData,'manager_approval_status','') === null) &&
                                    //         <Button color="secondary" onClick={()=>this.onClickReject('manager')} className={classes.button}>
                                    //             Reject
                                    //         </Button>
                                    //     }
                                    // </Grid>

                                }
                            </Grid>
                        </GenericHorizontal>
                    </Grid>

                </Grid>
                <Dialog
                    open={this.state.openRejectDialog}
                    title={'Isi Revisi'}
                    onClose={this.closeDialogReject}
                    actions={actions}
                >
                    <FormBuilder forms={[{
                        key : 'desc',
                        label : 'Keterangan',
                        type : 'multiline',
                        placeholder : 'Alasan ditolak atau revisi'
                    }]} value={{}} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                </Dialog>
                <DialogForm
                    open={this.state.dialogEditOpen}
                    title={'Edit Distribusi Komisi'}
                    onClose={()=>this.setState({dialogEditOpen : false})}
                    forms={form_edit}
                    onSave={this.saveEdit}
                    value={this.state.defaultValue}
                />
            </div>
        );
    }
}

export default withStyles(styles)(LaporanKomisiDetail);