const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper : {
        padding : 20
    },
    changePasswordSave : {
        marginTop : '10px'
    },
    buttonApprove : {
    	marginRight : '20px'
    }
});

export default styles;
