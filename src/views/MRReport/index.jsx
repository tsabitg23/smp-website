import React from 'react';
import GenericPage from '../../components/GenericPage';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MRProjectReport extends React.Component{
    constructor(props){
        super(props);
        this.projectReportStore = props.appstate.project_report;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('recommend_project_report')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.projectReportStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.projectReportStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.projectReportStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.projectReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        let res = this.state.openMode === 'create' ? this.projectReportStore.create(data) : this.projectReportStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.projectReportStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    onViewDetail = async (rowData)=>{
      this.props.history.push('/app/recommendation/'+rowData.id);
    };

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'Lihat Detail', Color: 'primary', Callback: this.onViewDetail ,key : 'update'},
        ];

        const tools = [];

        let form_items = [
            {
                key : 'name',
                label : 'Temuan/Kendala',
                type: 'text',
                disabled : true
            },
            {
                key : 'type',
                label : 'Kategori',
                type : 'select',
                items : [
                    {
                        value : 'minor',
                        label : 'Minor'
                    },
                    {
                        value : 'major',
                        label : 'Major'
                    }
                ],
                disabled : true
            },
            {
                key : 'recommendation',
                label : 'Rekomendasi',
                type: 'multiline',
                disabled : true
            },
            {
                key : 'alternate_recommendation',
                label : 'Alternatif Rekomendasi',
                type: 'multiline',
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Report",
            icon : "feedback",
            subtitle : "Jumlah data report project",
            subtitle_icon : "assignment",
            value : this.projectReportStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} data`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Rekomendasi'}
                    subtitle={'Beri rekomendasi pada laporan dari pengawas lapangan'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items.concat({
                        key : 'task.status',
                        label : 'Status',
                        type: 'status'
                    })}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.projectReportStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default MRProjectReport;