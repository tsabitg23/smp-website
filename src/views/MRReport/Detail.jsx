import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../MRProjectJob/styles';
import Grid from '@material-ui/core/Grid';
import GenericHorizontal from '../../components/GenericPage/Horizontal';
import {get} from 'lodash';
import {inject, observer} from 'mobx-react';
import { connect } from 'react-redux';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import Typography from '@material-ui/core/Typography';
import {LINKS} from '../../routes/index';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import Edit from '@material-ui/icons/Edit';
import Dialog from '../../components/Dialog';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class MRReport extends React.Component {
    constructor(props){
        super(props);
        this.projectReportStore = props.appstate.project_report;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            openMode : 'edit',
            isDialogOpen :false,
            defaultValue : {},
            permission : props.appstate.getPermission('recommend_project_report'),
            pimpro_manager_permission : {}
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.setState({
            pimpro_manager_permission : this.props.appstate.getPermission('manager_pimpro'),
        });
        await this.projectReportStore.getDetail(this.props.match.params.id);
        this.global_ui.closeLoader();
    }

    getValueColor(status){
        if(status === 'answered'){
            return 'success'
        }
        else if(status === 'rejected'){
            return 'danger'
        }
        else{
            return ''
        }
    }

    get detailData(){
        
        let column = [
            {
                key : 'name',
                label : 'Temuan/Kendala',
                type: 'text',
                disabled : true
            },
            {
                key : 'type',
                label : 'Kategori',
                type : 'select',
                items : [
                    {
                        value : 'minor',
                        label : 'Minor'
                    },
                    {
                        value : 'major',
                        label : 'Major'
                    }
                ],
                disabled : true
            },
            {
                key : 'recommendation',
                label : 'Rekomendasi',
                type: 'multiline',
                disabled : true
            },
            {
                key : 'alternate_recommendation',
                label : 'Alternatif Rekomendasi',
                type: 'multiline',
            },
            {
                key : 'task.status',
                label : 'Status',
                type : 'select',
                labelBold : true,
                valueBold : true,
                valueColor : this.getValueColor(get(this.projectReportStore.selectedData,'task.status',''))
            }
        ];
        return column.map(it=>{
            if(it.key.includes('status')){
                let val = it.value = get(this.projectReportStore.selectedData,it.key,'');
                it.value = val === 'created' ? 'Waiting' : val;
            }
            else{
                it.value = get(this.projectReportStore.selectedData,it.key,'') || '';
            }
            return it;
        });
    }

    save = (data)=>{
        this.global_ui.openLoader();
        let res = this.projectReportStore.update(this.props.match.params.id,data,false);
        res.then(async res=>{
            this.onDialogClose();
            await this.projectReportStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success approve',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }


    onClickBack = ()=>{
        if(this.props.history.length > 2){
            this.props.history.goBack();
        } else {
            if(this.state.permission.create){
                this.props.history.push(LINKS.MR_PROJECT_REPORT);
            }
            else{
                this.props.history.push(LINKS.PROJECT_REPORT);
            }
        }
    };

    onClickSave = status=>{
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            `Are you sure you want to ${status === 'approved' ? 'approve' : 'reject'} this data?`,
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.save({['task.status'] : status});
                }
            }
        );
    };

    onClickEdit = ()=>{
        this.setState({isDialogOpen : true});
    };

    onDialogClose = ()=>this.setState({isDialogOpen : false});

    reject = () => {
        this.global_ui.openLoader();
        let res = this.projectReportStore.reject(this.props.match.params.id,{},false);
        res.then(async res=>{
            this.onDialogClose();
            await this.projectReportStore.getDetail(this.props.match.params.id);
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success reject',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    }

    render(){
        const {classes} = this.props;

        const tools = [
            <Fab onClick={this.onClickBack} style={{display : 'inherit'}}  color="default" aria-label="ArrowBack">
                <ArrowBack />
            </Fab>
        ];

        let form_items = [
            {
                key : 'alternate_recommendation',
                label : 'Alternatif Rekomendasi',
                type: 'multiline',
            }
        ];

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`Approve Report`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.projectReportStore.selectedData}
                />
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                            <GenericHorizontal
                                title={`Report ${get(this.projectReportStore.selectedData,'id','cbc6c7f0-564c-493a-ae16-2e0cbedf70a6').split('-')[0]}`}
                                subtitle={`Laporan serta rekomendasi penyelesaian`}
                                data={this.detailData}
                                tools={tools}
                            >
                                <Grid container spacing={24} style={{marginTop:'20px',display : this.state.permission.create ? 'inherit' : 'none'}}>
                                    <Grid item xs={12} >
                                    {
                                        ((this.state.pimpro_manager_permission.update || this.state.permission.update) && (get(this.projectReportStore.selectedData,'task.status','-') == 'created'))  &&

                                        <Grid item xs={12} >
                                            <Typography variant="subtitle1" id="tableTitle" style={{marginBottom:20}}>
                                                Pimpinan Project
                                            </Typography>
                                            <Button variant="contained" color="primary" onClick={this.onClickEdit} className={classes.buttonApprove}>
                                                Approve
                                            </Button>
                                            {
                                                (get(this.projectReportStore.selectedData,'task.status','') === 'created') &&
                                                <Button color="secondary" onClick={this.reject} className={classes.button}>
                                                    Reject
                                                </Button>
                                            }
                                        </Grid>

                                    }
                                        {/* <Button variant="contained" color="primary" onClick={this.onClickEdit} className={classes.buttonApprove}>
                                            {get(this.projectReportStore.selectedData,'task.status','-') === 'answered' ? 'edit' : 'Tambah'} Alternatif rekomendasi
                                        </Button> */}
                                    </Grid>
                                </Grid>
                            </GenericHorizontal>
                    </Grid>
                    
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(MRReport);