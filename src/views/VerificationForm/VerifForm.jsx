import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import {inject, observer} from 'mobx-react';
import verificationStyle from '../../layouts/verificationStyle.jsx';
import FinishPage from './FinishPage';
import {get} from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import MessageBox from '../../components/MessageBox';
import messageBoxActions from '../../actions/MessageBox';
import UserData from './UserData';
import BagianSatu from './BagianSatu';
import PartOne from './PartOne';
import PartTwo from './PartTwo';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class VerifForm extends React.Component{
	constructor(props){
		super(props);
		this.state={
			activeStep : 0,
			partIDData : {},
			partOneData : {},
			partTwoData : {},
		}
		this.unitVerifyStore = props.appstate.unit_verification;
		this.global_ui = props.appstate.global_ui;
	};

	steps = (this.props.readonly) ? [
		'Identitas Diri',
		'Bagian 1',
		'Bagian 2',
	] :[
		'Identitas Diri',
		'Bagian 1',
		'Bagian 2',
		'Finish'
	];

	handleNext = async ()=>{
		const {activeStep} = this.state;
		if(this.props.readonly){
			this.updateStep(activeStep + 1);
			return;
		}
		
		if(activeStep === 0){
			this.global_ui.openLoader();
			this.id_form.validate().then(async res=>{
				if(this.props.match.params.id === 'new'){
					this.setState({
						partIDData : res
					});
					this.updateStep(activeStep + 1);
					this.global_ui.closeLoader();
				} else {
					await this.unitVerifyStore.update(this.props.match.params.id,res,false).then(res=>{
						this.updateStep(activeStep + 1);
						this.global_ui.closeLoader();
					}).catch(err=>{
						this.global_ui.closeLoader();
						return this.props.actions.showMessage(
							MessageBoxType.DANGER,
							err.message || err,
							()=>console.log('OK Clicked')
						);
					});
				}
            }).catch(err=>{
				this.global_ui.closeLoader();
			});
		}
		else if(activeStep === 1){
			this.global_ui.openLoader();
			if(this.props.match.params.id === 'new'){
				this.updateStep(activeStep + 1);
				this.global_ui.closeLoader();
			} else {
				await this.unitVerifyStore.update(this.props.match.params.id,{
					test_result : this.state.partOneData
				},false).catch(err=>{
					this.global_ui.closeLoader();
					return this.props.actions.showMessage(
						MessageBoxType.DANGER,
						err.message || err,
						()=>console.log('OK Clicked')
					);
				});
				this.updateStep(activeStep + 1);
				this.global_ui.closeLoader();
			}
		}
		else if(activeStep === 2){
			this.global_ui.openLoader();
			if(this.props.match.params.id === 'new'){
				//saveee
				const body = {
					...this.state.partIDData,
					test_result : {
						...this.state.partOneData,
						...this.state.partTwoData
					}
				}
				await this.unitVerifyStore.saveNewData(body).catch(err=>{
					this.global_ui.closeLoader();
					return this.props.actions.showMessage(
						MessageBoxType.DANGER,
						err.message || err,
						()=>console.log('OK Clicked')
					);
				});
				this.updateStep(activeStep + 1);
				this.global_ui.closeLoader();
			} else {
				await this.unitVerifyStore.update(this.props.match.params.id,{
					test_result : this.state.partTwoData
				},false).then(res=>{
					this.updateStep(activeStep + 1);
					this.global_ui.closeLoader();	
				}).catch(err=>{
					this.global_ui.closeLoader();
					return this.props.actions.showMessage(
						MessageBoxType.DANGER,
						err.message || err,
						()=>console.log('OK Clicked')
					);
				});

			}
		}
		else{
			this.updateStep(this.state.activeStep + 1);
		}
	};

	handleBack = ()=>{
		this.updateStep(this.state.activeStep -1);
		// this.setState({activeStep : this.state.activeStep - 1});
	};

	updateStep = (val)=>{
		this.setState({activeStep : val});
		if(!this.props.readonly){
			this.props.updateStep(val);
		}
	};

	async componentDidMount(){
		this.global_ui.openLoader();
		if(get(this.props,'match.params.id',false)){
			// if(this.props.match.params.id === 'new'){
			// 	await this.props.appstate.unit_verification.getNewData().catch(err=>{
			// 		this.props.actions.showMessage(
			// 			MessageBoxType.DANGER,
			// 			err,
			// 			()=>this.props.history.push('/login')
			// 		);
			// 	});
			// } else {
				await this.props.appstate.unit_verification.getDetail(this.props.match.params.id).catch(err=>{
					this.props.actions.showMessage(
						MessageBoxType.DANGER,
						err,
						()=>this.props.history.push('/login')
					);
				});
			// 	console.log(this.props.appstate.unit_verification.selectedData,'~~~~~~');
			// }
		}
        this.global_ui.closeLoader();
	}

	onUpdatePartOne = (val)=>{
		this.setState({partOneData : val})
	};

	onUpdatePartTwo = (val)=>{
		this.setState({partTwoData : val})
	};

	getStepContent = (stepIndex)=>{
		switch (stepIndex) {
			case 0:
				return (!this.unitVerifyStore.selectedData.verification) ? <div></div> : <UserData defaultValue={get(this.unitVerifyStore.selectedData,'verification',{})} getInstance={ref=>this.id_form = ref} readonly={this.props.readonly} id={this.props.match.params.id}/>;
			case 1:
				return <PartOne onUpdate={this.onUpdatePartOne} data={this.unitVerifyStore.selectedData.verification.test_result.filter(it=>it.type ==='five_option')} readonly={this.props.readonly}/>;
				// return <BagianSatu/>;
			case 2:
				return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.unitVerifyStore.selectedData.verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				// return <FinishPage/>;
			case 3:
				return <FinishPage/>;
			default:
				return <FinishPage/>;
		}
	};

	render(){
		const {activeStep} = this.state;
		const {classes} = this.props;
		return(
			<div>
				<Stepper activeStep={activeStep} alternativeLabel>
				{this.steps.map((label,index) => {
					return (
						<Step key={index}>
							<StepLabel>{label}</StepLabel>
						</Step>
					);
				})}
				</Stepper>
				<div>
					{
						this.getStepContent(activeStep)
					}
				</div>
				<div className={classes.stepButtons}>
					<Button variant="contained" color="primary" onClick={this.handleNext} disabled={activeStep === (this.steps.length-1)}>
						{activeStep === this.steps.length - 1 ? 'Finish' : 'Next'}
					</Button>
					<Button
						disabled={activeStep === 0 || (!this.props.readonly && activeStep === (this.steps.length-1))}
						onClick={this.handleBack}
						className={classes.backButton}
					>
						Back
					</Button>
				</div>
			</div>
		)
	}
}

VerifForm.defaultProps = {
	readonly : false
}
export default withStyles(verificationStyle)(VerifForm);