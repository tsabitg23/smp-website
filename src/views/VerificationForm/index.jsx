import React from 'react';
import verificationStyle from '../../layouts/verificationStyle.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import {inject, observer} from 'mobx-react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import MessageBox from '../../components/MessageBox';
import messageBoxActions from '../../actions/MessageBox';
import Identification from './Identification';
import {get} from 'lodash';
import VerificationPartOne from './PartOne';
import VerificationPartTwo from './PartTwo';
import FinishPage from './FinishPage';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class VerificationForm extends React.Component{

	constructor(props){
		super(props);
		this.unitVerifyStore = props.appstate.unit_verification;
		this.global_ui = props.appstate.global_ui;
		this.state = {
			activeStep : 0,
			partOneData : {},
			partTwoData : {},
		};
	}

	async componentDidMount(){
		this.global_ui.openLoader();
		if(this.props.match && this.props.match.params.id){
	        await this.props.appstate.unit_verification.getDetail(this.props.match.params.id).catch(err=>{
				this.props.actions.showMessage(
	                MessageBoxType.DANGER,
	                err,
	                ()=>this.props.history.push('/login')
	            );
	        });
		}
        this.global_ui.closeLoader();
	}

	handleNext = async ()=>{
		const {activeStep} = this.state;
		if(activeStep === 0){
			this.id_form.validate().then(async res=>{
				this.global_ui.openLoader();
				await this.unitVerifyStore.update(this.props.match.params.id,res,false).then(res=>{
					this.updateStep(activeStep + 1);
					// this.setState({activeStep : activeStep + 1});
	                this.global_ui.closeLoader();
				}).catch(err=>{
                    this.global_ui.closeLoader();
                    return this.props.actions.showMessage(
                        MessageBoxType.DANGER,
                        err.message || err,
                        ()=>console.log('OK Clicked')
                    );
				});
            });
		}
		else if(activeStep === 1){
			this.global_ui.openLoader();
			await this.unitVerifyStore.update(this.props.match.params.id,{
				test_result : this.state.partOneData
			},false).catch(err=>{
                this.global_ui.closeLoader();
                return this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    err.message || err,
                    ()=>console.log('OK Clicked')
                );
			});
            this.updateStep(activeStep + 1);
            this.global_ui.closeLoader();
		}
		else if(activeStep === 2){
			this.global_ui.openLoader();
			await this.unitVerifyStore.update(this.props.match.params.id,{
				test_result : this.state.partTwoData
			},false).catch(err=>{
                this.global_ui.closeLoader();
                return this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    err.message || err,
                    ()=>console.log('OK Clicked')
                );
			});
            this.updateStep(activeStep + 1);
            this.global_ui.closeLoader();	
		}
	};

	handleBack = ()=>{
		this.updateStep(this.state.activeStep -1);
		// this.setState({activeStep : this.state.activeStep - 1});
	};

	updateStep = (val)=>{
		this.setState({activeStep : val});
		this.props.updateStep(val);
	};

	onUpdatePartOne = (val)=>{
		this.setState({partOneData : val})
	};

	onUpdatePartTwo = (val)=>{
		this.setState({partTwoData : val})
	}

	getStepContent = (stepIndex)=>{
		switch (stepIndex) {
			case 0:
				return (!this.unitVerifyStore.selectedData.verification) ? <div></div> : <Identification defaultValue={get(this.unitVerifyStore.selectedData,'verification',{})} getInstance={ref=>this.id_form = ref}/>;
			case 1:
				return <VerificationPartOne onUpdate={this.onUpdatePartOne} data={this.unitVerifyStore.selectedData.verification.test_result.filter(it=>it.type ==='five_option')}/>;
			case 2:
				return <VerificationPartTwo onUpdate={this.onUpdatePartTwo} data={this.unitVerifyStore.selectedData.verification.test_result.filter(it=>it.type === 'true_false')}/>;
			case 3:
				return <FinishPage/>;
			default:
				return <FinishPage/>;
		}
	};

	render(){
		const {activeStep} = this.state;
		const {classes} = this.props;
		const steps = [
			'Identitas Diri',
			'Bagian 1',
			'Bagian 2',
			'Finish'
		];
		return (
			<div>
				<Stepper activeStep={activeStep} alternativeLabel>
				{steps.map(label => {
					return (
						<Step key={label}>
							<StepLabel>{label}</StepLabel>
						</Step>
					);
				})}
				</Stepper>
				<div>
					{
						this.getStepContent(activeStep)
					}
				</div>
				<div className={classes.stepButtons}>
					<Button variant="contained" color="primary" onClick={this.handleNext} disabled={activeStep === (steps.length-1)}>
						{activeStep === steps.length - 1 ? 'Finish' : 'Next'}
					</Button>
					<Button
						disabled={activeStep === 0 || activeStep === (steps.length-1)}
						onClick={this.handleBack}
						className={classes.backButton}
					>
						Back
					</Button>
				</div>
			</div>

		);
	}
}

export default withStyles(verificationStyle)(VerificationForm);