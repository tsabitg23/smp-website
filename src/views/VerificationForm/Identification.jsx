import React from 'react';
// import {inject, observer} from 'mobx-react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import schema from 'async-validator';
import {get} from 'lodash';
const jenisKelamin =[
    {
        value : 'male',
        label : 'Laki-laki'
    },
    {
        value : 'female',
        label : 'Perempuan'
    }
];
let form_items = [
    {
        key : 'name',
        label : 'Nama Lengkap',
        type: 'text',
        disabled : this.props.readonly
    },
    {
        key : 'ktp_number',
        label : 'Nomor KTP',
        type: 'number',
        disabled : this.props.readonly
    },
    {
        key : 'gender',
        label : 'Jenis Kelamin',
        type: 'select',
        items : jenisKelamin.map(it=>it),
        disabled : this.props.readonly
    },
    {
        key : 'city',
        label : 'Kota Domisili',
        type: 'text',
        disabled : this.props.readonly
    },
    {
        key : 'occupation',
        label : 'Pekerjaan',
        type: 'text',
        disabled : this.props.readonly
    },
    {
        key : 'created_at',
        label : 'Tanggal Pengisian Skala',
        type: 'date',
        disabled : true
    }
];

// @inject('appstate')
// @observer
class Identification extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formErrors : [],
			formData : {}
		};
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    validate = async ()=>{
        let rules = {};

        form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new schema(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	onFormUpdate = (formData)=>{
        this.setState({formData});
    };

	render(){
        let defaultData = this.props.defaultValue;
        if(!this.props.defaultValue.created_at){
            Object.assign(this.props.defaultValue,{created_at : moment().format('YYYY-MM-DD')});
        }
		return (
            <div>
                <span>Dengan ini saya menyatakan bahwa saya akan mengisi data identitas diri dengan jujur dan bertanggungjawab sebagaimana keadaan sebenarnya. </span>
    			<FormBuilder 
    				forms={form_items} 
    				value={defaultData}
    				onFormUpdate={this.onFormUpdate}
    				formErrors={this.state.formErrors}/>
            </div>
        );
	}
}
Identification.defaultProps = {
    readonly : false,
    defaultValue : {},
    getInstance : (e)=>{}
}

export default Identification;