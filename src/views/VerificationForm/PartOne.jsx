import React from 'react';
import Table from '../../components/Table/index';
import {inject, observer,Observer} from 'mobx-react';
import {startCase,get,upperCase} from 'lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

@inject('appstate')
@observer
class VerificationPartOne extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};

        let data = {};
        this.props.data.map((it,index)=>{
            let value = this.getValue(it.is_favorable,it.point);
            data[`five_option-${it.order}`] = value
        })
        this.state = data;
        this.props.onUpdate(data);
    }

    getValue = (is_favorable,value)=>{
        if(is_favorable){
            if(value === 5){
                return 'ss'
            }
            else if(value === 4){
                return 's'
            }
            else if(value === 3){
                return 'n'
            }
            else if(value === 2){
                return 'ts'
            }
            else if(value === 1){
                return 'sts'
            }
            else {
                return ''
            }
        }
        else {
            if(value === 1){
                return 'ss'
            }
            else if(value === 2){
                return 's'
            }
            else if(value === 3){
                return 'n'
            }
            else if(value === 4){
                return 'ts'
            }
            else if(value === 5){
                return 'sts'
            }
            else {
                return ''
            }   
        }
    }

    changeVal = (key,val)=>{
        this.setState({[key] : val},()=>this.props.onUpdate(this.state))
    };

    get dataTable(){
        return this.props.data.map((it,index)=>{

            return {
                id : it.id,
                name : it.name,
                key : index,
                response : <RadioGroup
                    aria-label="Response"
                    name={`group${index}`}
                    value={this.state[`five_option-${it.order}`]}
                    onChange={(e)=>this.changeVal(`five_option-${it.order}`,e.target.value)}
                    row={true}
                  >
                    <FormControlLabel disabled={this.props.readonly} value="ss" control={<Radio color={'primary'} />} label="SS" />
                    <FormControlLabel disabled={this.props.readonly} value="s" control={<Radio color={'primary'} />} label="S" />
                    <FormControlLabel disabled={this.props.readonly} value="n" control={<Radio color={'primary'} />} label="N" />
                    <FormControlLabel disabled={this.props.readonly} value="ts" control={<Radio color={'primary'} />} label="TS" />
                    <FormControlLabel disabled={this.props.readonly} value="sts" control={<Radio color={'primary'} />} label="STS" />
                  </RadioGroup>
            };
        });
    }

    render(){
        const configActionColumns = [];
        let headerColumn=[
            {label : 'Pertanyaan',key : 'name'},
            {label : 'Response',key : 'response'}
        ];

        let color = '#000';

        return(
            <div style={{marginBottom : 20}}>
                <span style={{color : color}}><b>PETUNJUK</b></span><br/>
                <span style={{color : color}}>
                    Pada bagian ini, Bapak/ Ibu diminta untuk menanggapi setiap pernyataan yang tersedia dengan cara memberikan centang (V) di salah satu respon jawaban yang tersedia dengan ketentuan sebagai berikut:
                </span>
                <ul style={{color : color}}>
                    <ol>SS : Sangat Setuju</ol>
                    <ol>S : Setuju</ol>
                    <ol>N : Netral</ol>
                    <ol>TS : Tidak Setuju</ol>
                    <ol>STS : Sangat Tidak Setuju</ol>
                </ul>
                <span style={{color : color}}>Isikan respon berdasarkan keadaan Bapak/ Ibu sehari-hari karena tidak ada jawaban benar atau jawaban salah. Jawaban terbaik adalah jawaban yang paling mencerminkan keadaan Bapak/ Ibu yang sebenarnya.  
                Tidak ada batasan waktu pengerjaan, pastikan bahwa setiap pernyataan dibaca dengan baik dan dipahami isinya. Jika telah selesai mengerjakan teliti kembali untuk memastikan semua pernyataan telah diisi. 
                </span>
                <Table
                    actionColumns={configActionColumns}
                    tableHeaderColor="primary"
                    tableHead={headerColumn}
                    tableData={this.dataTable}
                    pagination={false}
                    stripped={true}
                    group={'radio'}
                >
                </Table>
            </div>
        );
    }
}

VerificationPartOne.defaultProps = {
    readonly : false
}
export default VerificationPartOne;