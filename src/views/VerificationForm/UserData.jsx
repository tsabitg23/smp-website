import React from 'react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import {get, clone} from 'lodash';
import asyncScheme from 'async-validator';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import {inject, observer} from 'mobx-react';
const styles={
	text : {color: '#000',marginBottom : '20px',marginTop : '20px'}
}

@inject('appstate')
@observer
class UserData extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : {},
			formErrors : []
		}
		this.unitStore = props.appstate.unit;
		this.agentStore = props.appstate.agent;
		this.globalUI = props.appstate.global_ui;
	}

	async componentDidMount(){
		this.props.getInstance(this);
		if(this.props.id === 'new'){
			this.globalUI.openLoader();
			await this.agentStore.getAll();
			await this.unitStore.getAll();
			this.globalUI.closeLoader();
		}
    }

	jenisKelamin = [
	    {
	        value : 'male',
	        label : 'Laki-laki'
	    },
	    {
	        value : 'female',
	        label : 'Perempuan'
	    }
	];

	form_items = [
	    {
	        key : 'name',
	        label : 'Nama Lengkap',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'ktp_number',
	        label : 'Nomor KTP',
	        type: 'number',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'gender',
	        label : 'Jenis Kelamin',
	        type: 'select',
	        items : this.jenisKelamin.map(it=>it),
	        disabled : this.props.readonly
	    },
	    {
	        key : 'city',
	        label : 'Kota Domisili',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'occupation',
	        label : 'Pekerjaan',
	        type: 'text',
	        disabled : this.props.readonly
	    },
	    {
	        key : 'created_at',
	        label : 'Tanggal Pengisian Skala',
	        type: 'date',
	        disabled : true
	    }
	];

	validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve(this.state.formData);
                }
            });
        });
    };

	onFormUpdate = (formData)=>{
        this.setState({formData});
	};
	
	get getFormItems(){
		if(this.props.id === 'new'){
			const newColumn = [
				{
					key : 'unit_id',
					label : 'Unit',
					type: 'autocomplete',
					items: this.unitStore.data.map(it=>{
						return {
							value : it.id,
							label : it.kavling_number
						};
					}),
					required : true
				},
				{
					key : 'agent_id',
					label : 'Agent bersangkutan',
					type: 'autocomplete',
					items: this.agentStore.data.map(it=>{
						return {
							value : it.id,
							label : it.name
						};
					}),
					required : true
				},
				{
					key : 'booking_date',
					label : 'Booking Date',
					type : 'date',
					required : false
				},
				{
					key : 'verification_date',
					label : 'Verification Date',
					type : 'date',
					required : false
				},
				{
					key : 'akad_date',
					label : 'Akad Date',
					type : 'date',
					required : false
				}]
			const newForm = this.form_items.slice(0,this.form_items.length -1).concat(newColumn).concat(this.form_items.slice(this.form_items.length-1));
			return newForm;
		}
		return this.form_items;
	}

	render(){

		let defaultData = this.props.defaultValue;
        if(!get(this.props.defaultValue,'created_at',false)){
            Object.assign(this.props.defaultValue,{created_at : moment().format('YYYY-MM-DD')});
        }
		return (
			<div>
				<span style={styles.text}>Dengan ini saya menyatakan bahwa saya akan mengisi data identitas diri dengan jujur dan bertanggungjawab sebagaimana keadaan sebenarnya. </span>
				<FormBuilder 
					forms={this.getFormItems} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate}
					formErrors={this.state.formErrors}/>
				<span style={styles.text}>
					Selanjutnya, saya akan mengisi Skala Verifikasi Calon Konsumen Perumahan Syariah sesuai dengan keadaan diri sebenarnya dan pengetahuan yang saya miliki.  Apabila terdapat bagian yang terlewat belum diisi saya bersedia dihubungi kembali untuk melengkapi respon dan jawaban hingga tuntas.
				</span>
			</div>
		);
	}
}
UserData.defaultProps = {
    readonly : false,
    defaultValue : {},
    getInstance : (e)=>{}
}

export default UserData;