import React from 'react';

export default class FinishPage extends React.Component{
	render(){
		let linkStyle = {
			color : '#6772e5',
			fontSize : '9pt',
			fontWeight : 'bold',
			'&:hover' : {
			    color : '#8991e5',
			    cursor : 'pointer'
			},
			textTransform : 'none'
		}
		return (
			<div style={{textAlign : 'center'}}>
				<h3>Verifikasi Data Sukses</h3>
				<a href={'/login'} style={linkStyle}>Klik Disini untuk kembali ke login</a>
			</div>
		)
	}
}