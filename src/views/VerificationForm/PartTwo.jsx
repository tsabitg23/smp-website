import React from 'react';
import Table from '../../components/Table/index';
import {inject, observer,Observer} from 'mobx-react';
import {startCase,get,upperCase} from 'lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

@inject('appstate')
@observer
class VerificationPartTwo extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};

        let data = {};
        this.props.data.map((it,index)=>{
            let value = '';
            if(it.point > -1){
                if(it.is_favorable){
                    if(it.point === 1){
                        value= 'benar'
                    }
                    else if(it.point === 0){
                        value= 'salah'
                    }
                }
                else{
                    if(it.point === 0){
                        value= 'benar'
                    }
                    else if(it.point === 1){
                        value= 'salah'
                    }
                }
            }
            data[`true_false-${it.order}`] = value
        })
        console.log(data,'this is dataaa');
        this.state = data;
        this.props.onUpdate(data)
    }

    changeVal = (key,val)=>{
        this.setState({[key] : val},()=>this.props.onUpdate(this.state))
    };

    get dataTable(){
        return this.props.data.map(it=>{

            return {
                id : it.id,
                name : it.name,
                key : it.key,
                benar :<FormControlLabel disabled={this.props.readonly} checked={this.state[`true_false-${it.order}`] === "benar"} onClick={(event)=>this.changeVal(`true_false-${it.order}`,event.target.value)} value="benar" control={<Radio color={'primary'} />} label="B" />,
                salah : <FormControlLabel disabled={this.props.readonly} checked={this.state[`true_false-${it.order}`] === "salah"} onClick={(event)=>this.changeVal(`true_false-${it.order}`,event.target.value)} value="salah" control={<Radio color={'primary'} />} label="S" />,
            };
        });
    }

    render(){
        const configActionColumns = [];
        let headerColumn=[
            {label : 'Pertanyaan',key : 'name'},
            {label : 'Benar (B)',key : 'benar'},
            {label : 'Salah (S)',key : 'salah'}
        ];

        return(
            <div style={{marginBottom : 20}} id={'rightContainer'}>
                <span style={{color : '#000'}}>Pada bagian ini, Bapak/ Ibu diminta untuk memberikan jawaban Benar (B) atau Salah (S) untuk setiap pernyataan yang tersedia sesuai dengan pengetahuan yang dimiliki. </span>
                <Table
                    actionColumns={configActionColumns}
                    tableHeaderColor="primary"
                    tableHead={headerColumn}
                    tableData={this.dataTable}
                    pagination={false}
                    stripped={true}
                />
            </div>
        );
    }
}

VerificationPartTwo.defaultProps = {
    readonly : false
}
export default VerificationPartTwo;