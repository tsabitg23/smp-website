import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import ImportDialog from './ImportData';
import CloudUpload from '@material-ui/icons/CloudUpload';
import DefaultChart from '../../components/Chart';
import StackedBar from '../../components/Chart/StackedBar';
import Typography from '@material-ui/core/Typography';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Agent extends React.Component{
    constructor(props){
        super(props);
        this.agentStore = props.appstate.agent;
        this.agencyStore = props.appstate.agency;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            importDialogOpen: false,
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('agents'),
            numberOfMonthChart : 4
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.agencyStore.setRequestQuery({show_all : true});
        await this.agentStore.getAll();
        await this.agentStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
        await this.agentStore.getChartTwo({numberOfMonth: this.state.numberOfMonthChart});
        await this.agencyStore.getAll();
        this.global_ui.closeLoader();
    }

    // componentWillUnmount(){
    //     this.agencyStore.setRequestQuery({});
    // }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            this.agencyStore.setRequestQuery({show_all : true});
            await this.agentStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.agentStore.getChart({numberOfMonth: this.state.numberOfMonthChart});
            await this.agentStore.getChartTwo({numberOfMonth: this.state.numberOfMonthChart});
            await this.agencyStore.getAll();
            this.global_ui.closeLoader();    
        }
    }

    closeImportDialog = ()=>{
        this.setState({
            importDialogOpen : false
        })
    }

    openImportDialog = ()=>{
        this.setState({
            importDialogOpen : true
        })
    }

    onEditClick = async (rowData)=>{
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        if(this.agencyStore.data.length == 0){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Agency data not found, please add agency data first before insert agent data',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this agent?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.agentStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.agentStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete agent',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        let res = this.state.openMode === 'create' ? this.agentStore.create(data) : this.agentStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.agentStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} agent`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    refreshChart = async (value, closeLoader=true) => {
        this.global_ui.openLoader();
        await this.agentStore.getChart(value);
        await this.agentStore.getChartTwo(value);
        this.global_ui.closeLoader();
    }
    
    getFilteredAgent = async (val)=>{
        if(val){
            this.global_ui.openLoader();
            await this.agentStore.getAll(false,{show_all:true, agency_id : val});
            this.global_ui.closeLoader();
        } else {
            await this.agentStore.getAll(false,{show_all:true});
        }
    }

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete'},
        ];

        const tools = [
            <Fab onClick={this.openImportDialog} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudUpload />
            </Fab>,
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'name',
                label : 'Name',
                type: 'text'
            },
            {
                key : 'agency_id',
                label : 'Agency',
                type: 'autocomplete',
                items: this.agencyStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required : false
            },
            {
                key : 'phone',
                label : 'Phone',
                type: 'number',
            },
            {
                key : 'email',
                label : 'Email',
                type: 'email',
            },
            {
                key : 'address',
                label : 'Address',
                type: 'multiline',
            },
            {
                key : 'join_date',
                label : 'Join Date',
                type: 'date',
            },
        ];

        let topStatusData = [{
            type : 'count',
            title : "Mitra pemasaran",
            icon : "person",
            subtitle : "Total agents",
            subtitle_icon : "assignment",
            value : this.agentStore.data.length
        }];

        const extraFilterChart = [
            {
                key : 'agency_id',
                label : 'Nama Agency',
                option : this.agencyStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                required: false,
                onSelected : (val)=>this.getFilteredAgent(val)
            },
            {
                key : 'agent_id',
                label : 'Nama Agent',
                option : this.agentStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name
                    };
                }),
                disable_first : true,
                undisable_after: 'agency_id',
                required: false
            }
        ]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Mitra Pemasaran`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <DefaultChart 
                    subtitle={'Grafik Pencapaian Mitra'} 
                    changeFilter={this.refreshChart}
                    extraFilter={extraFilterChart}
                    data={get(this.agentStore.chart,'chart_one',[])}>
                    
                    <div>
                        <div style={{padding:20}}>
                        <Typography variant="subtitle2" style={{fontWeight:'bold'}} color="inherit">
                            Pencapaian Agency
                        </Typography>
                        </div>
                        <StackedBar
                            data={get(this.agentStore.chart_two,'chart_two',[])}
                        />
                    </div>

                </DefaultChart>
                <GenericPage
                    title={'Mitra Pemasaran'}
                    subtitle={'Daftar mitra pemasaran'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[
                        {label : 'Name',key : 'name'},
                        {label : 'Agency',key : 'agency.name'},
                        {label : 'Phone',key : 'phone'},
                        {label : 'Email',key : 'email'},
                        {label : 'Address',key : 'address'},
                        {label : 'Join Date',key : 'join_date'},
                        {key : 'unit_sold',label : 'Unit Sold'}]
                    }
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.agentStore.data}
                    tools={tools}
                />
                <ImportDialog
                    open={this.state.importDialogOpen}
                    handleClose={this.closeImportDialog}
                />
            </div>
        );
    }

}

export default Agent;
