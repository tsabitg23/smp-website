import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import * as moment from 'moment';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class AccountView extends React.Component{
    constructor(props){
        super(props);
        this.accountStore = props.appstate.accounts;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('rekening_bank')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.accountStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.accountStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();
        }
    }

    onEditClick = async (rowData)=>{
        this.global_ui.openLoader();
        await this.accountStore.getParent();
        await this.accountStore.getDetail(rowData.id).catch(err=>{
            this.global_ui.closeLoader();
        });
        if(this.accountStore.selectedData.account_header2){
            await this.accountStore.getByParent(this.accountStore.selectedData.account_header1);
        }
        this.global_ui.closeLoader();
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : this.accountStore.selectedData});
    };

    onClickAdd = async ()=>{
        //check if agency is empty
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.global_ui.openLoader();
        await this.accountStore.getParent();
        this.global_ui.closeLoader();
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this data?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.accountStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.accountStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete data',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        const reqBody = data;
        reqBody.parent_id = null;
        if(reqBody.account_header1){
            reqBody.parent_id = reqBody.account_header2;
        }
        if(reqBody.account_header2){
            reqBody.parent_id = reqBody.account_header2;
        }
        delete reqBody.account_header1;
        delete reqBody.account_header2;
        let res = this.state.openMode === 'create' ? this.accountStore.create(reqBody) : this.accountStore.update(reqBody.id,reqBody);
        res.then(async res=>{
            this.onDialogClose();
            await this.accountStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} data`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    onChangeParentAccount = async (val)=>{
        this.global_ui.openLoader();
        await this.accountStore.getByParent(val);
        this.global_ui.closeLoader();
    }


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            // { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete'},
        ];

        const tools = [
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'account_header1',
                label : 'Jenis Account',
                type: 'autocomplete',
                items: this.accountStore.parent1.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.code} - ${it.name}`
                    };
                }),
                required : false,
                empty_key : ['account_header2','account_header2-autocomplete'],
                onChange : this.onChangeParentAccount
            },
            {
                key : 'account_header2',
                label : 'Jenis Account Lanjutan',
                type: 'autocomplete',
                items: this.accountStore.parent2.map(it=>{
                    return {
                        value : it.id,
                        label : `${it.code} - ${it.name}`
                    };
                }),
                required : false
            },
            {
                key : 'code',
                label : 'Kode Akun',
                type: 'text'
            },
            {
                key : 'name',
                label : 'Nama Akun',
                type: 'text'
            },
            {
                key : 'position',
                label : 'Saldo Normal',
                type: 'text'
            },
            {
                key : 'category',
                label : 'Golongan',
                type: 'text'
            },
            // {
            //     key : 'bank_name',
            //     label : 'Nama bank',
            //     type: 'text'
            // },
            // {
            //     key : 'account_number',
            //     label : 'Nomor rekening',
            //     type: 'number'
            // },
            // {
            //     key : 'on_behalf',
            //     label : 'Atas nama',
            //     type: 'text'
            // },
            // {
            //     key : 'balance',
            //     label : 'Saldo',
            //     type: 'number'
            // },
        ];

        let header = [
            {
                key : 'code',
                label : 'Kode Akun',
                type: 'text'
            },
            {
                key : 'name',
                label : 'Nama Akun',
                type: 'text'
            },
            {
                key : 'position',
                label : 'Saldo Normal',
                type: 'text'
            },
            {
                key : 'category',
                label : 'Golongan',
                type: 'text'
            },
        ];

        let topStatusData = [{
            type : 'count',
            title : 'Akun',
            icon : 'note_add',
            subtitle : 'Jumlah akun',
            subtitle_icon : 'assignment',
            value : this.accountStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} data`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Akun'}
                    subtitle={'Daftar Akun'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[ {
                        key: 'no',
                        label: 'No'
                    }].concat(header)}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.accountStore.data}
                    tools={tools}
                />
            </div>
        );
    }

}

export default AccountView;
