import React from 'react';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import Table from '../../components/Table/index';
import TextField from '@material-ui/core/TextField';

const styles={
	text : {color: '#000',marginBottom : '20px',marginTop : '20px'}
}
class DataFinansial extends React.Component{
	constructor(props){
		super(props);
		let form = {};
		props.form.map(it=>{
			form[it.id] = get(props.defaultValue,it.id,'');
		})
		this.state = {
			formData : Object.assign({},form),
			formErrors : {}
		}
	}

	componentDidMount(){
        this.props.getInstance(this);
    }

    changeText = (val,key)=>{
    	let formData = this.state.formData;
    	formData[key] = parseInt(val);
		this.setState({formData});
	};

	validate = async ()=>{
		let rules = {};

        this.props.form.map(it=>{
            let type = 'number';

            rules[it.id] = [
                {
                    required : true,
                    message : `Enter valid score, min 1 max 4`,
                    type : type,
                    min: 1,
                    max: 4
                }
            ];
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate(this.state.formData, (errs, f) => {
                // this.setState({formErrors : errs ? errs : []});
                if (errs) {
                	let data = {}
                	errs.map(it=>{
                		 data[it.field+"-error"] = true;
                		 data[it.field+"-error-message"] = it.message;
                	})
                	this.setState({
                		formErrors : data
                	})
                	console.log(data,this.state.formErrors)
                    reject(errs);
                } else {
                	this.setState({
                		formErrors : {}
                	})
                    resolve(this.state.formData);
                }
            });
        });
    };

    get dataTable(){
        return this.props.form.map((it,index)=>{

            return {
                id : it.id,
                name : it.name,
                key : it.id,
                score : <TextField
					value={this.state.formData[it.id]}
					onChange={(e)=>this.changeText(e.target.value,it.id)}
					margin="normal"
					required={true}
					type={'number'}
					fullWidth
					error={get(this.state.formErrors,`${it.id}-error`,false)}
					helperText={get(this.state.formErrors,`${it.id}-error`,false) ? get(this.state.formErrors,`${it.id}-error-message`): null}
				/>
            };
        });
    }

	render(){
        const configActionColumns = [];
        let headerColumn=[
            {label : 'Aspek',key : 'name'},
            {label : 'Score',key : 'score'}
        ];

        let color = '#000';

        return(
            <div style={{marginBottom : 20}}>
            	<span>Score berisi angka dari 1 - 4 dengan kriteria, 1 : Buruk. 2 : Sedang. 3 : Baik. 4 : Sangat Baik</span>
                <Table
                    actionColumns={configActionColumns}
                    tableHeaderColor="primary"
                    tableHead={headerColumn}
                    tableData={this.dataTable}
                    pagination={false}
                    stripped={true}
                >
                </Table>
            </div>
        );
    }
}
DataFinansial.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default DataFinansial;