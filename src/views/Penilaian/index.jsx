import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import CloudUpload from '@material-ui/icons/CloudUpload';
import CloudDownload from '@material-ui/icons/CloudDownload';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get,startCase} from 'lodash';
import TopStatus from '../../components/TopStatus';
import Dialog from '../../components/Dialog/DialogOnly';
import AssessementForm from './form';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import { appConfig } from '../../../configs/app';

let titlePage = "data";
@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Penilaian extends React.Component{
    constructor(props){
        super(props);
        this.employeeScoreStore = props.appstate.employee_score;
        this.userData = props.appstate.userData;
        this.employeeStore = props.appstate.employee;
        this.assessmentStore = props.appstate.assessment_aspect;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('employee_score')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        this.employeeStore.setRequestQuery({show_all : true});
        await this.employeeScoreStore.getAll();
        await this.employeeStore.getAll();
        await this.assessmentStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.employeeScoreStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();  
        }
    }

    componentWillUnmount() {
        this.employeeScoreStore.setRequestQuery({
            show_all: true
        });
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.join_date = moment(data.join_date).format('YYYY-MM-DD');
        // console.log(rowData,'inii');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        if(this.employeeStore.data.length == 0){
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Employee data not found, please add employee data first before insert this data',
                ()=>console.log('OK Clicked')
            );
            return 0;
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            `Are you sure you want to delete this ${titlePage}?`,
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.employeeScoreStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.employeeScoreStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success delete ${titlePage}`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();

        let res = this.state.openMode === 'create' ? this.employeeScoreStore.create(data) : this.employeeScoreStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.employeeScoreStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} ${titlePage}`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    onViewDetail = async (rowData)=>{
      this.props.history.push('/app/penilaian/'+rowData.id);
    };


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    applyFilter = (data) => {
        this.employeeScoreStore.setRequestQuery({
            ...data,
            start_key : 'periode_start',
            end_key : 'periode_end',
            show_all : true
        });
        this.global_ui.openLoader();
        this.employeeScoreStore.getAll().then(res=>{
            this.global_ui.closeLoader();
        }).catch((er)=>{
            this.global_ui.closeLoader();
        });
    }

    downloadData = ()=>{
        this.global_ui.openLoader();
        this.employeeScoreStore.getExportData().then(res=>{
            this.global_ui.closeLoader();
            let tempLink = document.createElement('a');
            tempLink.href = appConfig.apiUrl + '/download_template/'+ res.file;
            tempLink.setAttribute('download', res.file);
            tempLink.click();
        })
    };

    render(){
        const configActionColumns = [
            { Icon: RemoveRedEye, Tooltip: 'View Detail', Color: 'gray', Callback: this.onViewDetail ,key : 'read'},
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            // <Fab onClick={this.openImportDialog} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
            //     <CloudUpload />
            // </Fab>,
            <Fab onClick={this.downloadData} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudDownload />
            </Fab>,
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'employee.name',
                label : 'Nama',
                type: 'text'
            },
            {
                key : 'employee.position',
                label : 'Jabatan',
                type: 'text'
            },
            {
                key : 'employee.departemen',
                label : 'Departemen',
                type: 'text'
            },
            {
                key : 'periode_start,periode_end',
                label : 'Period',
                type: 'range'
            },
            {
                key : 'total_score',
                label : 'Score',
                type: 'text'    
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : "Penilaian",
            icon : "assessment",
            subtitle : "Total data",
            subtitle_icon : "assignment",
            value : this.employeeScoreStore.data.length
        }]

        const filters = [
            {
                type : 'range'
            }
        ];

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} ${titlePage}`}
                    onClose={this.onDialogClose}
                >
                    <AssessementForm 
                        onClose={()=>this.onDialogClose()} 
                        openMode={this.state.openMode} 
                        onSave={this.save} 
                        defaultValue={this.state.defaultValue}/>
                </Dialog>
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Penilaian'}
                    subtitle={'Penilaian kinerja karyawan'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={form_items}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.employeeScoreStore.data}
                    tools={tools}
                    filter={filters}
                    onFilter={this.applyFilter}
                />
            </div>
        );
    }

}

export default Penilaian;
