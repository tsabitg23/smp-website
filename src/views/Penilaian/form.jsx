import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import {inject, observer} from 'mobx-react';
import styles from './styles.jsx';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import MessageBox from '../../components/MessageBox';
import messageBoxActions from '../../actions/MessageBox';
import FormDetail from './FormDetail';
import FormEmployee from './FormEmployee';
import {get, omit,pick,snakeCase} from 'lodash';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Form extends React.Component{
	constructor(props){
		super(props);
		let stateData ={
			activeStep : 0,
			pegawai_form: {},
			sikap_form: {},
			tanggung_jawab_form: {},
			kompetensi_form: {},
			perencanaan_form: {},
			pengorganisasian_form: {},
			pengarahan_form: {},
			pemecahan_masalah_form: {},
			kemampuan_interpersonal_form: {},
			kemampuan_berkomunikasi_form: {},
			isLoading : false,
			id : get(props.defaultValue,'id','null')
		}
		this.assessmentStore = props.appstate.assessment_aspect;
		this.employeeScoreStore = props.appstate.employee_score;
		this.global_ui = props.appstate.global_ui;
		if(this.props.openMode === 'edit'){
			const pegawai_form = pick(props.defaultValue, ['employee_id','periode_start','periode_end']);
			stateData.pegawai_form = pegawai_form;
			stateData.sikap_form= props.defaultValue.score_list || {}
			stateData.tanggung_jawab_form = props.defaultValue.score_list || {}
			stateData.kompetensi_form = props.defaultValue.score_list || {}
			stateData.perencanaan_form = props.defaultValue.score_list || {}
			stateData.pengorganisasian_form = props.defaultValue.score_list || {}
			stateData.pengarahan_form = props.defaultValue.score_list || {}
			stateData.pemecahan_masalah_form = props.defaultValue.score_list || {}
			stateData.kemampuan_interpersonal_form = props.defaultValue.score_list || {}
			stateData.kemampuan_berkomunikasi_form = props.defaultValue.score_list || {}
		}
		this.state = stateData;
	};

	steps = [
		'Pegawai',
		'Sikap',
		'Tanggung Jawab',
		'Kompetensi',
		'Perencanaan',
		'Pengorganisasian',
		'Pengarahan',
		'Pemecahan Masalah',
		'Kemampuan Interpersonal',
		'Kemampuan Berkomunikasi'
	];

	ref = this.steps.reduce((cur,val)=>{
		cur.push(snakeCase(val)+"_form");
		return cur;
	},[]);

	handleNext = async ()=>{
		const {activeStep} = this.state;

		if(this.props.readonly){
			this.updateStep(activeStep + 1);
			return;
		}
		this[this.ref[activeStep]].validate().then(async res=>{
			this.saveFormData(this.ref[activeStep],res)
			if(activeStep < (this.steps.length -1) && this.props.openMode === 'create'){
				this.updateStep(activeStep + 1);
			}
			else{
				this.save(activeStep)
			}
        });
	};


	handleBack = ()=>{

		this.updateStep(this.state.activeStep -1);

		// this.setState({activeStep : this.state.activeStep - 1});/
	};


	save = async (activeStep)=>{
		const {
			pegawai_form,
			sikap_form,
			tanggung_jawab_form,
			kompetensi_form,
			perencanaan_form,
			pengorganisasian_form,
			pengarahan_form,
			pemecahan_masalah_form,
			kemampuan_interpersonal_form,
			kemampuan_berkomunikasi_form
		} = this.state
		if(this.props.openMode === 'create'){
			// let data = this.changeFormatData({...data_pribadi_form,...data_pekerjaan_form,...data_kontak_form,...data_finansial_form,...data_unit_form});
			const score_list = {
				...sikap_form,
				...tanggung_jawab_form,
				...kompetensi_form,
				...perencanaan_form,
				...pengorganisasian_form,
				...pengarahan_form,
				...pemecahan_masalah_form,
				...kemampuan_interpersonal_form,
				...kemampuan_berkomunikasi_form
			}
			const total_score = Object.keys(score_list).reduce((acc,val) => acc+=score_list[val],0);
			const data = {
				...pegawai_form,
				score_list,
				total_score
			};
			this.props.onSave(data)
		}
		else{
			this.global_ui.openLoader();
			let data = {};
			if(activeStep == 0){
				data = this.state[this.ref[activeStep]]
			}
			else{
				const score_list = Object.assign({}, this.props.defaultValue.score_list, this.state[this.ref[activeStep]])
				const total_score = Object.keys(score_list).reduce((acc,val) => acc+=score_list[val],0);
				data = {
					score_list,
					total_score
				};
			}
			// let data = this.changeFormatData({...this.state[this.ref[activeStep]]});
			this.employeeScoreStore.update(this.state.id,data,false).then(async res=>{
	            this.global_ui.closeLoader();
	            if(activeStep === (this.steps.length -1)){
	            	this.props.onClose();
	            	this.props.actions.showMessage(
	            	    MessageBoxType.SUCCESS,
	            	    `Success edit data`,
	            	    ()=>console.log('OK Clicked')
	            	);
	            }
	            else{
		            this.updateStep(activeStep + 1)
	            }
	        }).catch(err=>{
	            this.global_ui.closeLoader();
	            this.props.actions.showMessage(
	                MessageBoxType.DANGER,
	                err.message,
	                ()=>console.log('OK Clicked')
	            );
	        });
		}
	};

	updateStep = (val)=>{
		this.setState({activeStep : val,isLoading : true});
		setTimeout(()=>{
			this.setState({isLoading : false})
		},5);
		if(!this.props.readonly){
			this.props.updateStep(val);
		}
	};

	async componentDidMount(){
		this.global_ui.openLoader();
		if(get(this.props,'match.params.id',false)){
	        await this.props.appstate.employee_score.getDetail(this.props.match.params.id).catch(err=>{
				this.props.actions.showMessage(
	                MessageBoxType.DANGER,
	                err,
	                ()=>this.props.history.push('/login')
	            );
	        });
		}
        this.global_ui.closeLoader();
	}

	saveFormData = (key,val)=>{
		this.setState({[key] : val});
	}

	getStepContent = (stepIndex)=>{
		switch (stepIndex) {
			case 0:
				return this.state.isLoading ? <div></div> :
					<FormEmployee
						defaultValue={this.state.pegawai_form}
						getInstance={ref=>this.pegawai_form = ref}/>;
			case 1:
				return this.state.isLoading ? <div></div> : <FormDetail
					form={this.assessmentStore.data[0].children}
					defaultValue={this.state.sikap_form}
					getInstance={ref=>this.sikap_form = ref}/>;
			case 2:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[1].children}
						defaultValue={this.state.tanggung_jawab_form}
						getInstance={ref=>this.tanggung_jawab_form = ref}/>;
			case 3:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[2].children}
						defaultValue={this.state.kompetensi_form}
						getInstance={ref=>this.kompetensi_form = ref}/>;
			case 4:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[3].children}
						defaultValue={this.state.perencanaan_form}
						getInstance={ref=>this.perencanaan_form = ref}/>;
			case 5:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[4].children}
						defaultValue={this.state.pengorganisasian_form}
						getInstance={ref=>this.pengorganisasian_form = ref}/>;
			case 6:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[5].children}
						defaultValue={this.state.pengarahan_form}
						getInstance={ref=>this.pengarahan_form = ref}/>;
			case 7:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[6].children}
						defaultValue={this.state.pemecahan_masalah_form}
						getInstance={ref=>this.pemecahan_masalah_form = ref}/>;
			case 8:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[7].children}
						defaultValue={this.state.kemampuan_interpersonal_form}
						getInstance={ref=>this.kemampuan_interpersonal_form = ref}/>;
			case 9:
				// return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
				return this.state.isLoading ? <div></div> :
					<FormDetail
						form={this.assessmentStore.data[8].children}
						defaultValue={this.state.kemampuan_berkomunikasi_form}
						getInstance={ref=>this.kemampuan_berkomunikasi_form = ref}/>;
			default:
				return <div></div>;
		}
	};

	render(){
		const {activeStep} = this.state;
		const {classes} = this.props;
		return(
			<div>
				<Stepper activeStep={activeStep} alternativeLabel>
				{this.steps.map((label,index) => {
					return (
						<Step key={index}>
							<StepLabel>{label}</StepLabel>
						</Step>
					);
				})}
				</Stepper>
				<div>
					{
						this.getStepContent(activeStep)
					}
				</div>
				<div className={classes.stepButtons}>
					<Button variant="contained" color="primary" onClick={this.handleNext}>
						{activeStep === this.steps.length - 1 ? 'Save' : 'Next'}
					</Button>
					<Button
						onClick={this.handleBack}
						className={classes.backButton}
						disabled={activeStep === 0}
					>
						Back
					</Button>
				</div>
			</div>
		)
	}
}

Form.defaultProps = {
	updateStep : (val)=>{}
}

export default withStyles(styles)(Form);
