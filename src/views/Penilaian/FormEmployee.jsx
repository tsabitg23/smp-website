import React from 'react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import {inject, observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';

@inject('appstate')
@observer
class FormEmployee extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			formData : {},
			formData2 : {},
			formErrors : [],
			employeeData : {}
		}
		this.globalUI = props.appstate.global_ui;
		this.employeeStore = props.appstate.employee;
	}

	async componentDidMount(){
		await this.globalUI.openLoader();
        await this.employeeStore.getAll().catch(err=>{
			this.globalUI.closeLoader();
        })
        this.props.getInstance(this);
		await this.globalUI.closeLoader();
    }

    form_items = [
    	{
    	    key : 'employee_id',
    	    label : 'Employee',
    	    type: 'autocomplete'
    	},
    	{
    	    key : 'periode_start',
    	    label : 'Periode Penilaian Dari',
    	    type: 'date',
    	},
    	{
    	    key : 'periode_end',
    	    label : 'Periode Penilaian Sampai',
    	    type: 'date',
    	}
    ];

    validate = async ()=>{
        let rules = {};

        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate({...this.state.formData,...this.state.formData2}, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve({...this.state.formData,...this.state.formData2});
                }
            });
        });
    };

	onFormUpdate = (formData)=>{
		let employeeData = this.employeeStore.data.find(it=>it.id === formData['employee_id']);
        this.setState({formData});
		if(employeeData){
			this.setState({employeeData : employeeData})
		}
    };
    onFormUpdate2 = (formData2)=>{
        this.setState({formData2});
    };


	render(){
	    let form_items = [
	        {
	            key : 'employee_id',
	            label : 'Employee',
	            type: 'autocomplete',
	            items: this.employeeStore.data.map(it=>{
	                return {
	                    value : it.id,
                        label : `${it.name} - ${it.nik}`
	                };
	            }),
	        }
	    ];
	    let form_items2 = [
	        {
	            key : 'periode_start',
	            label : 'Periode Penilaian Dari',
	            type: 'date',
	        },
	        {
	            key : 'periode_end',
	            label : 'Periode Penilaian Sampai',
	            type: 'date',
	        }
		];
		
		let defaultData = this.props.defaultValue;
		return (
			<div>
				{
					form_items[0].items.length > 0 ?
					<FormBuilder 
						forms={form_items} 
						value={defaultData}
						onFormUpdate={this.onFormUpdate}
						formErrors={this.state.formErrors}/>
					:
					<div></div>	
				}
				<TextField
					id={'nama'}
					label={'Nama'}
					value={get(this.state.employeeData,'name','')}
					margin="normal"
					disabled={true}
					type={'text'}
					fullWidth
					// error={get(this.state,`${form.key}-error`,false)}
					// helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
				<TextField
					id={'jabatan'}
					label={'Jabatan'}
					value={get(this.state.employeeData,'position','')}
					margin="normal"
					disabled={true}
					type={'text'}
					fullWidth
					// error={get(this.state,`${form.key}-error`,false)}
					// helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
				<TextField
					id={'division'}
					label={'Divisi'}
					value={get(this.state.employeeData,'division','')}
					margin="normal"
					disabled={true}
					type={'text'}
					fullWidth
					// error={get(this.state,`${form.key}-error`,false)}
					// helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
				<FormBuilder 
					forms={form_items2} 
					value={defaultData}
					onFormUpdate={this.onFormUpdate2}
					formErrors={this.state.formErrors}/>
			</div>
		);
	}
}
FormEmployee.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default FormEmployee;