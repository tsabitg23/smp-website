import React from 'react';
import GenericPage from '../../components/GenericPage';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {inject, observer} from 'mobx-react';
import Fab from '@material-ui/core/Fab';
import Dialog from '../../components/Dialog';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import messageBoxActions from '../../actions/MessageBox';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import {get} from 'lodash';
import TopStatus from '../../components/TopStatus';
import CloudDownload from '@material-ui/icons/CloudDownload';
import {appConfig} from "../../../configs/app";
import ImportDialog from './ImportData';
import CloudUpload from "@material-ui/icons/CloudUpload";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Employee extends React.Component{
    constructor(props){
        super(props);
        this.employeeStore = props.appstate.employee;
        this.userData = props.appstate.userData;
        this.global_ui = props.appstate.global_ui;
        this.state = {
            isDialogOpen : false,
            openMode : 'create',
            defaultValue : {},
            importDialogOpen: false,
            project_id : this.userData.selected_project,
            permission : props.appstate.getPermission('employees')
        };
    }

    async componentDidMount(){
        this.global_ui.openLoader();
        await this.employeeStore.getAll();
        this.global_ui.closeLoader();
    }

    async componentDidUpdate(prev){
        if(prev.appstate.project.selectedProject !== this.state.project_id){
            this.setState({project_id : prev.appstate.project.selectedProject});
            this.global_ui.openLoader();
            await this.employeeStore.getAll().catch((err)=>{
                this.global_ui.closeLoader();
            });
            this.global_ui.closeLoader();  
        }
    }

    onEditClick = async (rowData)=>{
        // let data = rowData;
        // data.join_date = moment(data.join_date).format('YYYY-MM-DD');
        this.setState({isDialogOpen : true,openMode : 'edit',defaultValue : rowData});
    };

    onClickAdd = ()=>{
        if(this.props.appstate.project.selectedProject === 'not-found') {
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Project not found, please contact admin',
                ()=>console.log('OK Clicked')
            );
            return 0;    
        }
        this.setState({isDialogOpen : true,openMode : 'create',defaultValue : {}});
    };

    onDeleteClick = rowData => {
        this.props.actions.showMessage(
            MessageBoxType.CONFIRM,
            'Are you sure you want to delete this employee?',
            (event)=>{
                const target = event.currentTarget || event.target;
                if(target.value === 'OK'){
                    this.delete(rowData.id);
                }
            }
        );
    };

    delete = async (id)=>{
        this.global_ui.openLoader();
        this.employeeStore.delete(id).then(async res=>{
            this.onDialogClose();
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success delete agency',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
    };

    save = async (data)=>{
        this.global_ui.openLoader();
        // console.log(this.state.permissions,this.employeeStore.selectedData.permissions);
        // return;

        let res = this.state.openMode === 'create' ? this.employeeStore.create(data) : this.employeeStore.update(data.id,data);
        res.then(async res=>{
            this.onDialogClose();
            await this.employeeStore.getAll();
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                `Success ${this.state.openMode === 'create' ? 'add' : 'edit'} agency`,
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.global_ui.closeLoader();
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });

    };

    downloadData = ()=>{
        this.global_ui.openLoader();
        this.employeeStore.exportData().then(res=>{
            this.global_ui.closeLoader();
            let tempLink = document.createElement('a');
            tempLink.href = appConfig.apiUrl + '/download_template/'+ res.file;
            tempLink.setAttribute('download', res.file);
            tempLink.click();
        });
    };

    closeImportDialog = ()=>{
        this.setState({
            importDialogOpen : false
        });
    }

    openImportDialog = ()=>{
        this.setState({
            importDialogOpen : true
        });
    }


    onDialogClose = ()=>this.setState({isDialogOpen : false,permissions :[]});

    render(){
        const configActionColumns = [
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: this.onEditClick ,key : 'update'},
            { Icon: Delete, Tooltip: 'Delete', Color: 'danger', Callback: this.onDeleteClick,key : 'delete' },
        ];

        const tools = [
            <Fab onClick={this.openImportDialog} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudUpload />
            </Fab>,
            <Fab onClick={this.downloadData} style={{display : this.state.permission.create ? 'inherit' : 'none'}} color="default" aria-label="Add">
                <CloudDownload />
            </Fab>,
            <Fab onClick={this.onClickAdd} style={{display : this.state.permission.create ? 'inherit' : 'none'}}  color="default" aria-label="Add">
                <Add />
            </Fab>
        ];

        let form_items = [
            {
                key : 'name',
                label : 'Name',
                type: 'text'
            },
            {
                key : 'nik',
                label : 'NIK',
                type: 'text'
            },
            {
                key : 'position',
                label : 'Position',
                type : 'text'
            },
            {
                key : 'division',
                label : 'Division',
                type : 'text'
            },
            {
                key : 'superior',
                label : 'Atasan Langsung',
                type : 'text'
            },
            {
                key : 'join_date',
                label : 'Join Date',
                type : 'date'
            }
        ];

        let topStatusData = [{
            type : 'count',
            title : 'Karyawan',
            icon : 'assignment_ind',
            subtitle : 'Jumlah karyawan',
            subtitle_icon : 'assignment',
            value : this.employeeStore.data.length
        }]

        return  (
            <div>
                <Dialog
                    open={this.state.isDialogOpen}
                    title={`${this.state.openMode === 'create' ? 'Add New' : 'Edit'} Employee`}
                    onClose={this.onDialogClose}
                    forms={form_items}
                    onSave={this.save}
                    value={this.state.defaultValue}
                />
                <TopStatus data={topStatusData}/>
                <GenericPage
                    title={'Karyawan'}
                    subtitle={'daftar karyawan'}
                    project_id={this.props.appstate.project.selectedProject}
                    headerColumn={[{key:'no',label:'No'}].concat(form_items)}
                    actionColumn={configActionColumns.filter(it=>this.state.permission[it.key])}
                    data={this.employeeStore.data}
                    tools={tools}
                />
                <ImportDialog
                    open={this.state.importDialogOpen}
                    handleClose={this.closeImportDialog}
                />
            </div>
        );
    }

}

export default Employee;
