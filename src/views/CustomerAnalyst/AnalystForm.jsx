import React from 'react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import {inject, observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import MessageBoxType from "../../components/MessageBox/MessageBoxType";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import messageBoxActions from "../../actions/MessageBox";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class FormAnalyst extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            formData : {},
            formData2 : {},
            formErrors : [],
            employeeData : {}
        }
        this.globalUI = props.appstate.global_ui;
        this.customerStore = props.appstate.customer;
        this.unitStore = props.appstate.unit;
    }

    async componentDidMount(){
        await this.globalUI.openLoader();
        await this.customerStore.getAll().catch(err=>{
            this.globalUI.closeLoader();
        })
        if(this.props.defaultValue.customer_id){
            await this.onChangeCustomer(this.props.defaultValue.customer_id);
        }
        this.props.getInstance(this);
        await this.globalUI.closeLoader();
    }

    onChangeCustomer = async (val)=>{
        this.globalUI.openLoader();
        await this.unitStore.getUnitByCustomer(val);
        this.globalUI.closeLoader();
    }

    form_items = [
        {
            key : 'akad_date',
            label : 'Tanggal Akad',
            type: 'date'
        },
        {
            key : 'customer_id',
            label : 'Nama Debitur',
            type: 'autocomplete',
        },
        {
            key : 'unit_id',
            label : 'Unit',
            type: 'autocomplete',
        },
        {
            key : 'recommendation',
            label : 'Rekomendasi',
            type: 'select',
        },
        {
            key : 'unit_price',
            label : 'Harga Unit',
            type: 'number'
        },
        {
            key : 'payment_scheme',
            label : 'Skema Bayar',
            type: 'select',

        },
        {
            key : 'tenor',
            label : 'Lama Bulan',
            type: 'number'
        },
        {
            key : 'downpayment',
            label : 'Total DP',
            type: 'number'
        },
        {
            key : 'first_downpayment',
            label : 'DP Pertama',
            type: 'number'
        },
        {
            key : 'next_downpayment',
            label : 'DP Selanjutnya',
            type: 'number'
        },
        {
            key : 'next_dp_payment',
            label : 'Jumlah Angsuran DP',
            type: 'number'
        },
        {
            key : 'dp_count',
            label : 'Jumlah DP Sesuai Angsuran',
            type: 'autofill_operator',
        },
        {
            key : 'total_price',
            label : 'Harga Total Jual',
            type: 'number'
        },
        {
            key : 'total_debt',
            label : 'Nominal Hutang',
            type: 'autofill_operator',
        },
        {
            key : 'installment',
            label : 'Jumlah Angsuran',
            type: 'number'
        },
        {
            key : 'total_installment',
            label : 'Total Nominal Angsuran',
            type: 'autofill_operator',
        },
        {
            key : 'mortgage_history',
            label : 'Riwayat Pinjaman',
            type: 'select',
        },
        {
            key : 'desc',
            label : 'Keterangan',
            type: 'multiline'
        },
    ];

    validate = async ()=>{
        let rules = {};


        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else if(it.type === 'percentage'){
                type = 'number';
            } else if(it.type === 'time'){
                type = 'string';
            } else if(it.type.includes('autofill')){
                return;
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }

            if(it.type === 'percentage'){
                rules[it.key][0].min = 0;
                rules[it.key][0].max = 100;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate({...this.state.formData}, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve({...this.state.formData});
                }
            });
        }).catch(err=>{
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Isi form dengan benar',
                ()=>console.log('OK Clicked')
            );
            throw err;
        });
    };

    onFormUpdate = (formData)=>{
        // let employeeData = this.customerStore.data.find(it=>it.id === formData['employee_id']);
        this.setState({formData});
        // if(employeeData){
        //     this.setState({employeeData : employeeData})
        // }
    };
    onFormUpdate2 = (formData2)=>{
        this.setState({formData2});
    };


    render(){
        const form_items = [
            {
                key : 'akad_date',
                label : 'Tanggal Akad',
                type: 'date'
            },
            {
                key : 'customer_id',
                label : 'Nama Debitur',
                type: 'autocomplete',
                items: this.customerStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.name,
                    };
                }),
                empty_key : ['unit_id','unit_id-autocomplete'],
                onChange : this.onChangeCustomer
            },
            {
                key : 'unit_id',
                label : 'Unit',
                type: 'autocomplete',
                items: this.unitStore.data.map(it=>{
                    return {
                        value : it.id,
                        label : it.kavling_number,
                    };
                }),
            },
            {
                key : 'recommendation',
                label : 'Rekomendasi',
                type: 'select',
                items : [
                    {
                        value : 'Ya',
                        label : 'Ya'
                    },
                    {
                        value : 'Tidak',
                        label : 'Tidak'
                    },
                ],
            },
            {
                key : 'unit_price',
                label : 'Harga Unit',
                type: 'number'
            },
            {
                key : 'payment_scheme',
                label : 'Skema Bayar',
                type: 'select',
                items : [
                    {
                        value : 'Cash',
                        label : 'Cash'
                    },
                    {
                        value : 'Cash Bertahap',
                        label : 'Cash Bertahap'
                    },
                    {
                        value : 'Kredit',
                        label : 'Kredit'
                    },
                ],
            },
            {
                key : 'tenor',
                label : 'Lama Bulan',
                type: 'number'
            },
            {
                key : 'downpayment',
                label : 'Total DP',
                type: 'number'
            },
            {
                key : 'first_downpayment',
                label : 'DP Pertama',
                type: 'number'
            },
            {
                key : 'next_downpayment',
                label : 'DP Selanjutnya',
                type: 'number'
            },
            {
                key : 'next_dp_payment',
                label : 'Jumlah Angsuran DP',
                type: 'number'
            },
            {
                key : 'dp_count',
                label : 'Jumlah DP Sesuai Angsuran',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'next_downpayment'
                },{
                    operator : 'div',
                    key : 'next_dp_payment'
                }],
                placeholder: ''
            },
            {
                key : 'total_price',
                label : 'Harga Total Jual',
                type: 'number'
            },
            {
                key : 'total_debt',
                label : 'Nominal Hutang',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_price'
                },{
                    operator : 'sub',
                    key : 'downpayment'
                }],
                placeholder: ''
            },
            {
                key : 'installment',
                label : 'Jumlah Angsuran',
                type: 'number'
            },
            {
                key : 'total_installment',
                label : 'Total Nominal Angsuran',
                type: 'autofill_operator',
                formula : [{
                    operator : 'add',
                    key : 'total_price'
                },{
                    operator : 'sub',
                    key : 'downpayment'
                },{
                    operator : 'div',
                    key : 'installment'
                }],
                placeholder: ''
            },
            {
                key : 'mortgage_history',
                label : 'Riwayat Pinjaman',
                type: 'select',
                items : [
                    {
                        value : 'Pernah Meminjam',
                        label : 'Pernah Meminjam'
                    },
                    {
                        value : 'Tidak Pernah',
                        label : 'Tidak Pernah'
                    }
                ],
            },
            {
                key : 'desc',
                label : 'Keterangan',
                type: 'multiline'
            },
        ];


        let defaultData = this.props.defaultValue;
        return (
            <div>
                {
                    form_items[1].items.length > 0 ?
                        <FormBuilder
                            forms={form_items}
                            value={defaultData}
                            onFormUpdate={this.onFormUpdate}
                            formErrors={this.state.formErrors}/>
                        :
                        <div></div>
                }
            </div>
        );
    }
}
FormAnalyst.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default FormAnalyst;
