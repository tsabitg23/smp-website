import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import {inject, observer} from 'mobx-react';
import styles from './styles.jsx';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../../components/MessageBox/MessageBoxType';
import MessageBox from '../../components/MessageBox';
import messageBoxActions from '../../actions/MessageBox';
import {get, omit,pick,snakeCase} from 'lodash';
import FormAnalyst from './AnalystForm';
import FormDebtor from './DebtorForm';
import FormAccount from './AccountForm';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Form extends React.Component{
    constructor(props){
        super(props);
        let stateData ={
            activeStep : 0,
            analisa_form: {},
            debitur_form: {},
            mutasi_rekening_form: {},
            isLoading : false,
            id : get(props.defaultValue,'id','null')
        }
        this.customerAnalyst = props.appstate.customer_analyst;
        this.global_ui = props.appstate.global_ui;
        if(this.props.openMode === 'edit'){
            const analisa = pick(this.customerAnalyst.selectedData,['akad_date','customer_id','unit_id','recommendation','unit_price','payment_scheme','tenor','downpayment','first_downpayment','next_downpayment','next_dp_payment','dp_count','total_price','total_debt','installment','total_installment','mortgage_history','desc']);
            const debitur = pick(this.customerAnalyst.selectedData,['debtor_dob','spouse_name','occupation','spouse_occupation','work_duration','spouse_work_duration','children','payroll_desc','debtor_asset']);
            const mutasi_rekening = this.customerAnalyst.selectedData.accounts;
            stateData.analisa_form = analisa;
            stateData.debitur_form = debitur;
            stateData.mutasi_rekening_form = mutasi_rekening;
        }
        this.state = stateData;
    };

    steps = [
        'Analisa',
        'Debitur',
        'Mutasi Rekening',
    ];

    ref = this.steps.reduce((cur,val)=>{
        cur.push(snakeCase(val)+'_form');
        return cur;
    },[]);

    handleNext = async ()=>{
        const {activeStep} = this.state;

        if(this.props.readonly){
            this.updateStep(activeStep + 1);
            return;
        }
        this[this.ref[activeStep]].validate().then(async res=>{
            this.saveFormData(this.ref[activeStep],res)
            if(activeStep < (this.steps.length -1) && this.props.openMode === 'create'){
                this.updateStep(activeStep + 1);
            }
            else{
                this.save(activeStep);
            }
        });
    };


    handleBack = ()=>{

        this.updateStep(this.state.activeStep -1);

        // this.setState({activeStep : this.state.activeStep - 1});/
    };


    save = async (activeStep)=>{
        const {
            analisa_form,
            debitur_form,
            mutasi_rekening_form,
        } = this.state;
        if(this.props.openMode === 'create'){
            // let data = this.changeFormatData({...data_pribadi_form,...data_pekerjaan_form,...data_kontak_form,...data_finansial_form,...data_unit_form});
            // const score_list = {
            //     ...sikap_form,
            //     ...tanggung_jawab_form,
            //     ...kompetensi_form,
            //     ...perencanaan_form,
            //     ...pengorganisasian_form,
            //     ...pengarahan_form,
            //     ...pemecahan_masalah_form,
            //     ...kemampuan_interpersonal_form,
            //     ...kemampuan_berkomunikasi_form
            // }
            // const total_score = Object.keys(score_list).reduce((acc,val) => acc+=score_list[val],0);
            // const data = {
            //     ...pegawai_form,
            //     score_list,
            //     total_score
            // };
            this.props.onSave({
                ...analisa_form,
                ...debitur_form,
                accounts : mutasi_rekening_form
            });
        }
        else{
            if(activeStep !== (this.steps.length -1)){
                this.updateStep(activeStep + 1)
                return;
            }
            const {
                analisa_form,
                debitur_form,
                mutasi_rekening_form,
            } = this.state;
            let data = {
                ...analisa_form,
                ...debitur_form,
                accounts : mutasi_rekening_form
            };
            data = omit(data, ['dp_count','total_debt','total_installment'])
            this.global_ui.openLoader();
            // let data = {};
            // if(activeStep == 0){
            //     data = this.state[this.ref[activeStep]]
            // }
            // else{
            //     const score_list = Object.assign({}, this.props.defaultValue.score_list, this.state[this.ref[activeStep]])
            //     const total_score = Object.keys(score_list).reduce((acc,val) => acc+=score_list[val],0);
            //     data = {
            //         score_list,
            //         total_score
            //     };
            // }
            // // let data = this.changeFormatData({...this.state[this.ref[activeStep]]});
            this.customerAnalyst.update(this.state.id,data,false).then(async res=>{
                this.global_ui.closeLoader();
                if(activeStep === (this.steps.length -1)){
                    this.props.onClose();
                    this.props.actions.showMessage(
                        MessageBoxType.SUCCESS,
                        `Success edit data`,
                        ()=>console.log('OK Clicked')
                    );
                }
                else{
                    this.updateStep(activeStep + 1)
                }
            }).catch(err=>{
                this.global_ui.closeLoader();
                this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    err.message,
                    ()=>console.log('OK Clicked')
                );
            });
        }
    };

    updateStep = (val)=>{
        this.setState({activeStep : val,isLoading : true});
        setTimeout(()=>{
            this.setState({isLoading : false});
        },5);
        if(!this.props.readonly){
            this.props.updateStep(val);
        }
    };

    async componentDidMount(){
        this.global_ui.openLoader();
        if(get(this.props,'match.params.id',false)){
            await this.customerAnalyst.getDetail(this.props.match.params.id).catch(err=>{
                this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    err,
                    ()=>this.props.history.push('/login')
                );
            });
        }
        this.global_ui.closeLoader();
    }

    saveFormData = (key,val)=>{
        this.setState({[key] : val});
    }

    getStepContent = (stepIndex)=>{
        switch (stepIndex) {
            case 0:
                return this.state.isLoading ? <div></div> :
                    <FormAnalyst
                        defaultValue={this.state.analisa_form}
                        getInstance={ref=>this.analisa_form = ref}/>;
            case 1:
                return this.state.isLoading ? <div></div> : <FormDebtor
                    defaultValue={this.state.debitur_form}
                    getInstance={ref=>this.debitur_form = ref}/>;
            case 2:
                // return <PartTwo onUpdate={this.onUpdatePartTwo} data={this.state..verification.test_result.filter(it=>it.type === 'true_false')} readonly={this.props.readonly}/>;
                return this.state.isLoading ? <div></div> :
                    <FormAccount
                        defaultValue={this.state.mutasi_rekening_form}
                        getInstance={ref=>this.mutasi_rekening_form = ref}/>;
            default:
                return <div></div>;
        }
    };

    render(){
        const {activeStep} = this.state;
        const {classes} = this.props;
        return(
            <div>
                <Stepper activeStep={activeStep} alternativeLabel>
                    {this.steps.map((label,index) => {
                        return (
                            <Step key={index}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
                <div>
                    {
                        this.getStepContent(activeStep)
                    }
                </div>
                <div className={classes.stepButtons}>
                    <Button variant="contained" color="primary" onClick={this.handleNext}>
                        {activeStep === this.steps.length - 1 ? 'Save' : 'Next'}
                    </Button>
                    <Button
                        onClick={this.handleBack}
                        className={classes.backButton}
                        disabled={activeStep === 0}
                    >
                        Back
                    </Button>
                </div>
            </div>
        );
    }
}

Form.defaultProps = {
    updateStep : (val)=>{}
}

export default withStyles(styles)(Form);
