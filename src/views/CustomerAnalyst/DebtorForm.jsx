import React from 'react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import {inject, observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import MessageBoxType from "../../components/MessageBox/MessageBoxType";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import messageBoxActions from "../../actions/MessageBox";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class FormDebtor extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            formData : {},
            formErrors : [],
        }
        this.globalUI = props.appstate.global_ui;
        this.customerStore = props.appstate.customer;
        this.unitStore = props.appstate.unit;
    }

    async componentDidMount(){
        await this.globalUI.openLoader();
        await this.customerStore.getAll().catch(err=>{
            this.globalUI.closeLoader();
        })
        this.props.getInstance(this);
        await this.globalUI.closeLoader();
    }

    onChangeCustomer = async (val)=>{
        this.globalUI.openLoader();
        await this.unitStore.getUnitByCustomer(val);
        this.globalUI.closeLoader();
    }



    validate = async ()=>{
        let rules = {};


        this.form_items.map(it=>{
            let type = 'string';
            if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
                type = 'string';
            }
            else if(it.type === 'autocomplete_multi'){
                type = 'array';
            }
            else if(it.type === 'percentage'){
                type = 'number';
            } else if(it.type === 'time'){
                type = 'string';
            } else if(it.type.includes('autofill')){
                return;
            }
            else{
                type = it.type;
            }

            rules[it.key] = [
                {
                    required : get(it,'required',true),
                    message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
                    type : type
                }
            ];
            if(it.type === 'password'){
                rules[it.key][0].min = 6;
            }

            if(it.type === 'percentage'){
                rules[it.key][0].min = 0;
                rules[it.key][0].max = 100;
            }
        });

        const validator = new asyncScheme(rules);
        return new Promise((resolve,reject)=>{
            validator.validate({...this.state.formData}, (errs, f) => {
                this.setState({formErrors : errs ? errs : []});
                if (errs) {
                    reject(errs);
                } else {
                    resolve({...this.state.formData});
                }
            });
        }).catch(err=>{
            console.log(this.state.formData);
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                'Isi form dengan benar',
                ()=>console.log('OK Clicked')
            );
            throw err;
        });
    };

    form_items = [
        {
            key : 'debtor_dob',
            label : 'Tanggal Lahir debitor',
            type: 'date'
        },
        {
            key : 'spouse_name',
            label : 'Nama Istri',
            type: 'text'
        },
        {
            key : 'occupation',
            label : 'Pekerjaan debitor',
            type: 'text'
        },
        {
            key : 'spouse_occupation',
            label : 'Pekerjaan istri debitor',
            type: 'text'
        },
        {
            key : 'work_duration',
            label : 'Lama bekerja debitor',
            type: 'text'
        },
        {
            key : 'spouse_work_duration',
            label : 'Lama bekerja istri debitor',
            type: 'text'
        },
        {
            key : 'children',
            label : 'Jumlah anak',
            type: 'text'
        },
        {
            key : 'payroll_desc',
            label : 'Deskripsi Gaji',
            type: 'text'
        },
        {
            key : 'debtor_asset',
            label : 'Asset',
            type: 'text'
        }
    ];

    onFormUpdate = (formData)=>{
        // let employeeData = this.customerStore.data.find(it=>it.id === formData['employee_id']);
        this.setState({formData});
        // if(employeeData){
        //     this.setState({employeeData : employeeData})
        // }
    };
    onFormUpdate2 = (formData2)=>{
        this.setState({formData2});
    };


    render(){



        let defaultData = this.props.defaultValue;
        return (
            <div>
                        <FormBuilder
                            forms={this.form_items}
                            value={defaultData}
                            onFormUpdate={this.onFormUpdate}
                            formErrors={this.state.formErrors}/>
            </div>
        );
    }
}
FormDebtor.defaultProps = {
    defaultValue : {},
    getInstance : (e)=>{}
}
export default FormDebtor;
