import React from 'react';
import FormBuilder from '../../components/FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import asyncScheme from 'async-validator';
import {inject, observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import TableInput from "../../components/TableInput";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import messageBoxActions from "../../actions/MessageBox";
import MessageBoxType from "../../components/MessageBox/MessageBoxType";

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class FormAccount extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            formData : {},
            account : this.props.defaultValue
        }
        this.globalUI = props.appstate.global_ui;
    }

    async componentDidMount(){
        await this.globalUI.openLoader();
        this.props.getInstance(this);
        await this.globalUI.closeLoader();
    }


    validate = async ()=>{

        return new Promise((resolve,reject)=>{
            if(this.state.account.length > 0){
                resolve(this.state.account);
            } else {
                reject();
                this.props.actions.showMessage(
                    MessageBoxType.DANGER,
                    'Isi form dengan benar',
                    ()=>console.log('OK Clicked')
                );
            }
        });
    };

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    changeFunds = (data)=>{
        this.setState({
            account :data
        });
    }

    render(){
        const form_items = [
            {
                key : 'bank_name',
                placeholder: '',
                label : 'Nama Bank',
                type: 'text'
            },
            {
                key : 'month',
                placeholder: 'exp: agustus 2018',
                label : 'Bulan - Tahun',
                type: 'text'
            },
            {
                key : 'start_balance',
                placeholder: '',
                label : 'Saldo Awal',
                type: 'number'
            },
            {
                key : 'credit',
                placeholder: '',
                label : 'Mutasi Kredit',
                type: 'number'
            },
            {
                key : 'debit',
                placeholder: '',
                label : 'Mutasi Debit',
                type: 'number'
            },
            {
                key : 'end_balance',
                placeholder: '',
                label : 'Saldo Akhir',
                type: 'number'
            },
        ];
        const summary = [{
            key:'total',
            name : 'Total',
            operation : 'add',
            column_index : [3,4]
        },{
            key:'rata_rata',
            name : 'Rata-rata',
            operation : 'mean',
            column_index : [3,4,5]
        }];

        return (
            <div style={{marginBottom:20}}>
                <TableInput
                    title={'Mutasi Rekening'}
                    form={form_items}
                    summary={summary}
                    defaultData={this.props.defaultValue}
                    onChangeData={this.changeFunds}
                />
            </div>
        );
    }
}
FormAccount.defaultProps = {
    defaultValue : [],
    getInstance : (e)=>{}
}
export default FormAccount;
