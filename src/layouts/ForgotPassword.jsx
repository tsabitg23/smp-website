import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// core components
import loginStyle from './loginStyle.jsx';
import {appConfig} from '../../configs/app';
import {LINKS} from '../routes';
import {inject, observer} from 'mobx-react';
//Actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
//Import may not working with Reserved proxy so using require instead.
const logo = require('../assets/img/react_logo.svg');

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email : ''
        };
        this.authStore = props.appstate.auth;
    }

    componentDidMount(){

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    getReward = async ()=>{
        this.props.appstate.global_ui.openLoader();
        await this.authStore.forgotPassword(this.state).then(res=>{
            this.props.actions.showMessage(
                MessageBoxType.SUCCESS,
                'Success, Please check your email',
                ()=>console.log('OK Clicked')
            );
        }).catch(err=>{
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log('OK Clicked')
            );
        });
        this.props.appstate.global_ui.closeLoader();
    };

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                        aria-describedby="dialog-loading">
                    <DialogContent>
                        <CircularProgress/>
                    </DialogContent>
                </Dialog>
                <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
                <Grid container spacing={0} className={classes.layout}>
                    <Grid item xs={3}>
                        <div className={classes.logoContainer}>
                            <img src={logo} className={classes.logo}/>
                        </div>
                        <Paper className={classes.paper}>
                            <div style={{marginBottom: '20px'}}>
                                <Typography variant="h5" gutterBottom>
                                    Forgot password
                                </Typography>
                                <span>
                                    Insert email of your account and we'll send the link to reset your password
                                </span>
                            </div>
                            <TextField
                                id="email"
                                label={'Email'}
                                value={this.state.email}
                                onChange={this.handleChange('email')}
                                fullWidth
                                className={classes.inputs}
                            />
                            <Button variant="contained" color="primary" onClick={this.getReward} className={classes.button}>
                                Send Reset Email
                            </Button>
                            <span className={classes.forgot_password} onClick={()=>this.props.history.push(LINKS.LOGIN)}>
                                Back to login
                            </span>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(loginStyle)(ForgotPassword);
