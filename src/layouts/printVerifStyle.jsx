const printStyle = theme => ({
    logo_container : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-between',
        marginBottom : 20
    },
    logo : {
        width : '80px',
        height : '80px'
    },
    subtext : {
        textTransform : 'none'
    },
    data1 : {
        paddingBottom : 20,
        borderBottom : '2px solid #EAEAEA'
    },
    subtitle1:{
        fontSize : '13pt',
        textTransform : 'none'
    },
    subtitle2 : {
        textTransform : 'none'
    },
    subheader : {
        marginTop : 20,
    },
    data2 : {
        marginTop : 20
    },
    bag1subtitle : {
        marginBottom : '-24px'
    }
});

export default printStyle;
