import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as firebase from "firebase";
import 'perfect-scrollbar/css/perfect-scrollbar.css';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// core components
import Header from '../components/Header/Header.jsx';
import Footer from '../components/Footer/Footer.jsx';
import Sidebar from '../components/Sidebar/Sidebar.jsx';
import MessageBox from '../components/MessageBox';
import {appConfig} from '../../configs/app';
import dashboardRoutes from 'routes/dashboard.jsx';
import dashboardStyle from './dashboardStyle.jsx';
import appRoute from '../routes/app';
//Actions
import NotificationActions from '../actions/Notifications';
import {getRouteDestination} from '../utils/notification_router';
// import { getImgSrc } from '../commons/commonFuncs';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
import {inject, observer} from 'mobx-react';
//Import may not working with Reserved proxy so using require instead.
// const image = getImgSrc(require('../assets/img/sidebar-2.jpg'));
// const image = require('../assets/img/sidebar-2.jpg');
// const logo = require('../assets/img/rumahdps-logo2.jpeg');
import image from '../assets/img/sidebar-2.jpg';
import logo from '../assets/img/logo.png';

let classAction = {};
let navigate = ()=>{};
let dashNotificationStore = {};
let projectStore = {};
firebase.messaging().onMessage((payload) => {
  console.log("Message received.", payload);
  classAction.notify(
    'info',
    payload.data.description, //notif message
    payload.data.title, //title
    'New Notification', //New Notification
    () => {
      projectStore.setProject(payload.data.project_id);
      dashNotificationStore.read(payload.data.message_id);
      navigate(dashNotificationStore.getRouteDestination(payload.data.type,payload.data.additional_data))
    }
  )
});
//Connect component to Redux store.
@connect(
  state => ({
    messageBox: state.messageBox || {},
    notifications: state.notifications || []
  }),
  dispatch => ({
    actions: bindActionCreators(NotificationActions, dispatch)
  })
)
@inject('appstate')
@observer
class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.global_ui = props.appstate.global_ui;
    this.userPermission = props.appstate.user_permission;
    this.routeStore = props.appstate.routes;
    this.state = {
      mobileOpen: false
    };
    classAction = props.actions;
    navigate = props.history.push;
    dashNotificationStore = props.appstate.notification
    projectStore = props.appstate.project;
  }

   switchRoutes = ()=>(
        <Switch>
            {this.routeStore.dashboard.map(
                (prop, key) =>
                    prop.redirect ? (
                        <Redirect from={prop.path} to={prop.to} key={key} />
                    ) : (
                        <Route path={prop.path} exact component={prop.component} key={key} />
                    )
            )}
            {
                appRoute.map((prop,key)=>{
                    return (
                        <Route path={prop.path} component={prop.component} key={key}/>
                    );
                })
            }
        </Switch>
    );

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  getRoute() {
    return this.props.location.pathname !== '/maps';
  }
  async componentDidMount() {
    this.global_ui.openLoader();
    await this.loadUnreadNotif();
    this.global_ui.closeLoader();
    if (navigator.platform.indexOf('Win') <= -1) return;
  }

  loadUnreadNotif = async ()=>{
    let res = await dashNotificationStore.getUnread().catch(err=>{
      this.global_ui.closeLoader();
    });
    this.props.actions.setUnreadNotif(res.data,this.unreadNotifOnClick);
  }

  unreadNotifOnClick = (data)=>{
      projectStore.setProject(data.notification.project_id);
      dashNotificationStore.read(data.notification.id);
      navigate(dashNotificationStore.getRouteDestination(data.notification.type,data.notification.data))
  }

  componentDidUpdate(e) {
    if (e.history.location.pathname === e.location.pathname) return;

    this.refs.mainPanel.scrollTop = 0;
    if (this.state.mobileOpen) this.setState({ mobileOpen: false });
  }

  onNotificationChange = items => {
    this.props.actions.addOrUpdateNotifications(items);
  };

  onNotificationDelete = items => {
    // this.props.actions.getNotifications();
    this.props.actions.deleteNotifications(items);
  };

  render() {
    const { classes, notifications, messageBox, ...rest } = this.props;
    // console.log(this.routeStore.dashboard.concat(appRoute));
    return (
      <div className={classes.wrapper}>
        <MessageBox {...messageBox} open={messageBox.open || false} />
          <Dialog open={this.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                  aria-describedby="dialog-loading">
              <DialogContent>
                  <CircularProgress/>
              </DialogContent>
          </Dialog>
        <Sidebar
          routes={this.routeStore.dashboard}
          logoText={appConfig.project_name}
          logo={logo}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={appConfig.dashboard_color}
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
          <Header
            routes={this.routeStore.dashboard.concat(appRoute)}
            handleDrawerToggle={this.handleDrawerToggle}
            notifications={notifications}
            onNotificationChange={this.onNotificationChange}
            onNotificationDelete={this.onNotificationDelete}
            notificationBackgroundImage={image}
            {...rest}
          />
          {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{this.switchRoutes()}</div>
            </div>
          ) : (
            <div className={classes.map}>{this.switchRoutes}</div>
          )}
          {this.getRoute() ? <Footer /> : null}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(App);
