import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import printVerifStyle from './printVerifStyle.jsx';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {appConfig} from '../../configs/app';
import {inject, observer} from 'mobx-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import logo from '../assets/img/rumahdps-logo2.jpeg';
import moment from 'moment';
import {get} from 'lodash';
import Table from '../components/Table/';
import TableHorizontal from '../components/Table/TableHorizontal';
import classNames from 'classnames';

moment.locale('id');
@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class PrintVerification extends React.Component{
	constructor(props) {
        super(props);
        this.global_ui = props.appstate.global_ui;
        this.unitVerify = props.appstate.unit_verification;
        this.state = {
        };
    }

    async componentDidMount(){
    	this.global_ui.openLoader();
        await this.props.appstate.unit_verification.getDetail(this.props.match.params.id).catch(err=>{
			this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err,
                ()=>this.props.history.goBack()
            );
        });
        this.global_ui.closeLoader();
        window.print().close();
    }

    get partOneSum(){
        let column = [
            {
                key : 'total_score',
                label : "Total Score",
                valueBold : true
            },
            {
                key : 'category',
                label : "Category",
                valueBold : true
            },
            {
                key : 'desc',
                label : "Description",
                valueBold : true
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerify.selectedData.verification,`scores[0][${it.key}]`,'');
            return it;
        });
    }

    get partTwoSum(){
        let column = [
            {
                key : 'total_score',
                label : "Total Score",
                valueBold : true
            },
            {
                key : 'category',
                label : "Category",
                valueBold : true
            },
            {
                key : 'desc',
                label : "Description",
                valueBold : true
            }
        ];
        return column.map(it=>{
            it.value = get(this.unitVerify.selectedData.verification,`scores[1][${it.key}]`,'');
            return it;
        });
    }

	render(){
		const {classes} = this.props;
		let partOneHeader = [
            {
                key : 'category',
                label : "Category"
            },
            {
                key : 'value',
                label : "Score"
            },
            {
                key : 'desc',
                label : "Description"
            }
        ]

        let partOneData = get(this.unitVerify.selectedData,'verification.scores[0].data',[]);

		return (
			<div style={{background : 'white'}}>
			<MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
			<Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading" aria-describedby="dialog-loading">
			    <DialogContent>
			        <CircularProgress/>
			    </DialogContent>
			</Dialog>
			<Grid container spacing={0} style={{padding : 20}}>
                <Grid item xs={12}>
                    <div className={classes.logo_container}>
	                    <div>
	                    	<Typography variant="h5" gutterBottom>
	                            Verifikasi User
	                        </Typography>
	                        <Typography variant="subtitle1" gutterBottom>
	                            <code>{this.props.match.params.id}</code>
	                        </Typography>
	                    </div>
	                    <div>
	                    	<img src={logo} className={classes.logo}/>
	                    </div>
                    </div>
                    <div className={classes.data1}>
                    	<Grid container spacing={0}>
	                    	<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Projek</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'unit.project.name','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Tanggal Booking</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{!get(this.unitVerify.selectedData,'booking_date',false) ? "-" : moment(get(this.unitVerify.selectedData,'booking_date','-')).format('dddd, DD-MM-YYYY')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>No. Kavling</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'unit.kavling_number','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Tanggal Verifikasi</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{!get(this.unitVerify.selectedData,'verification_date',false) ? "-" : moment(get(this.unitVerify.selectedData,'verification_date','-')).format('dddd, DD-MM-YYYY')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Agent</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'agent.name','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Tanggal Akad</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{!get(this.unitVerify.selectedData,'akad_date',false) ? "-" : moment(get(this.unitVerify.selectedData,'akad_date','-')).format('dddd, DD-MM-YYYY')}
	                		</Grid>
                    	</Grid>
                    </div>
                    <div className={classes.data2}>
                    	<Grid container spacing={0}>
                    		<Grid item xs={12}>
                    			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>
	                            	Hasil Verifikasi
		                        </Typography>
                    		</Grid>
                    		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Tanggal Pengisian</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{!get(this.unitVerify.selectedData,'verification.created_at',false) ? "-" : moment(get(this.unitVerify.selectedData,'verification.created_at','-')).format('dddd, DD-MM-YYYY')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Nomor KTP</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'verification.ktp_number','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Nama</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'verification.name','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Kota</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'verification.city','-')}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Jenis Kelamin</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'verification.gender','-') === 'male' ? "Laki-laki" : "Perempuan"}
	                		</Grid>
	                		<Grid item xs={3}>
	                			<Typography variant="subtitle2" gutterBottom className={classes.subtitle2}>Pekerjaan</Typography>
	                		</Grid>
	                		<Grid item xs={3}>
	                			{get(this.unitVerify.selectedData,'verification.occupation','-')}
	                		</Grid>
                    	</Grid>
                		<Grid item xs={12} className={classes.subheader}>
                			<Typography variant="subtitle2" gutterBottom className={classNames(classes.subtitle1,classes.bag1subtitle)}>
                            	Bagian 1
	                        </Typography>
                		</Grid>
                		<Grid item xs={12}>
                			<Table
                			    actionColumns={[]}
                			    tableHeaderColor="primary"
                			    tableHead={partOneHeader}
                			    tableData={partOneData}   
                			    pagination={false}
                			/>
                		</Grid>
                		<Grid item xs={12}>
                			<TableHorizontal
                			    data={this.partOneSum}/>
                		</Grid>
                		<Grid item xs={12} className={classes.subheader}>
                			<Typography variant="subtitle2" gutterBottom className={classNames(classes.subtitle1,classes.bag1subtitle)}>
                            	Bagian 2
	                        </Typography>
                		</Grid>
                		<Grid item xs={12}>
                			<TableHorizontal
                			    data={this.partTwoSum}/>
                		</Grid>
                    </div>
                </Grid>
            </Grid>
			</div>
		)
	}
}

export default withStyles(printVerifStyle)(PrintVerification);