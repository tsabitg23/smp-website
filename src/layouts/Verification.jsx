import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// core components
import verificationStyle from './verificationStyle.jsx';
import {appConfig} from '../../configs/app';
import {LINKS} from '../routes';
import {inject, observer} from 'mobx-react';
//Actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
import VerifForm from '../views/VerificationForm';

//Import may not working with Reserved proxy so using require instead.
const logo = require('../assets/img/react_logo.svg');

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class Verification extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            code : '',
            step : 0
        };
    }

    componentDidMount(){

    }

    componentDidUpdate(prevProps,prevState){
        // let rightThing = document.getElementById('rightContainer');
        // console.log(rightThing.scrollHeight,'hasdahsd');
        // if(rightThing){
        //     rightThing.scrollTop = 0;
        // }
        // if(prevState.step === this.state.step){
        //     return ;
        // }
        window.scrollTo(0, 0);
    }

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root} id={'rightContainer'}>
                <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
                <Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                        aria-describedby="dialog-loading">
                    <DialogContent>
                        <CircularProgress/>
                    </DialogContent>
                </Dialog>
                <Grid container spacing={0} className={classes.layout}>
                    <Grid item xs={12} md={10} lg={10}>
                        <Paper className={classes.paper}>
                            <div style={{marginBottom: '20px',textAlign:'center'}}>
                                <Typography variant="h5" gutterBottom>
                                    {appConfig.project_name} - Verifikasi
                                </Typography>
                            </div>
                            <VerifForm match={this.props.match} history={this.props.history} updateStep={(val)=>this.setState({step : val})}/>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(verificationStyle)(Verification);
