import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import verificationStyle from './verificationStyle.jsx';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {appConfig} from '../../configs/app';
import {inject, observer} from 'mobx-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';

import VerifForm from '../views/VerificationForm/VerifForm';

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)
@inject('appstate')
@observer
class VerifUser extends React.Component{
	constructor(props) {
        super(props);

        this.state = {
            step : 0
        };
    }

    componentDidUpdate(prevProps,prevState){
        window.scrollTo(0, 0);
    }
	render(){
		const {classes} = this.props;
		return (
			<div className={classes.root}>
            <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
			<Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading" aria-describedby="dialog-loading">
			    <DialogContent>
			        <CircularProgress/>
			    </DialogContent>
			</Dialog>
			<Grid container spacing={0} className={classes.layout}>
                <Grid item xs={12} md={11} lg={11}>
                    <Paper className={classes.paper}>
                        <div style={{marginBottom: '20px',textAlign:'center'}}>
                            <Typography variant="h5" gutterBottom>
                                {appConfig.project_name} - Verifikasi
                            </Typography>
                        </div>
                        <VerifForm match={this.props.match} updateStep={(val)=>this.setState({step : val})} history={this.props.history}/>
                    </Paper>
                </Grid>
            </Grid>
			</div>
		)
	}
}

export default withStyles(verificationStyle)(VerifUser);