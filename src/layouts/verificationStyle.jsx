const verifStyle = theme => ({
    root: {
        flexGrow: 1,
        width: '100vw',
        minHeight : '100vh',
        backgroundColor : '#F1F5F9',
        // paddingTop : '20px',
        // overflow : 'hidden'
    },
    layout : {
        minHeight : '100vh',
        justifyContent : 'center',
        // paddingTop : '50px'
    },
    linkStyle :{
        color : '#6772e5',
        fontSize : '9pt',
        fontWeight : 'bold',
        '&:hover' : {
            color : '#8991e5',
            cursor : 'pointer'
        }
    },
    button : {
        marginTop : '40px',
        marginBottom : '10px'
    },
    logo :{
      width : '200px',
    },
    logoContainer : {
        textAlign : 'center'
    },
    inputs : {
      marginTop : '5px',
      marginBottom : '5px',
    },
    paper: {
        padding: theme.spacing.unit * 2,
        // textAlign: 'center',
        marginTop : 20,
        color: theme.palette.text.secondary,
        display:'flex',
        flexDirection : 'column',
    },
    stepButtons : {
        display : 'flex',
        flexDirection : 'row-reverse',
        alignSelf : 'flex-end',
        // backgroundColor : 'yellow',
        // paddingLeft : 20,
        // paddingRight : 20,
    }
});

export default verifStyle;
