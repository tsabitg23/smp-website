import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// core components
import loginStyle from './loginStyle.jsx';
import {appConfig} from '../../configs/app';
import {LINKS} from '../routes';
import {inject, observer} from 'mobx-react';
//Actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
//Import may not working with Reserved proxy so using require instead.
const logo = require('../assets/img/react_logo.svg');

@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class Reward extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            code : ''
        };
        this.rewardStore = props.appstate.reward;
    }

    componentDidMount(){

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    getReward = async ()=>{
        this.props.appstate.global_ui.openLoader();
        let res = await this.rewardStore.claimReward(this.state.code).then(res=>{
            // let arrBuffer = this.base64ToArrayBuffer(res.data);
            let data = new Blob([res.data], {type: res.headers['content-type']});
            let filename = res.headers['content-disposition'];
            filename = filename.split('"')[1];
            let csvURL = window.URL.createObjectURL(data);
            let tempLink = document.createElement('a');
            tempLink.href = csvURL;
            tempLink.setAttribute('download', filename);
            tempLink.click();
        }).catch(err=>{
            this.props.actions.showMessage(
                MessageBoxType.DANGER,
                err.message,
                ()=>console.log(err)
            );
        });
        this.props.appstate.global_ui.closeLoader();
    };

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
                <Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                        aria-describedby="dialog-loading">
                    <DialogContent>
                        <CircularProgress/>
                    </DialogContent>
                </Dialog>
                <Grid container spacing={0} className={classes.layout}>
                    <Grid item xs={12} md={3} lg={3}>
                        <div className={classes.logoContainer}>
                            <img src={logo} className={classes.logo}/>
                        </div>
                        <Paper className={classes.paper}>
                            <div style={{marginBottom: '20px'}}>
                                <Typography variant="h5" gutterBottom>
                                    {appConfig.project_name}
                                </Typography>
                                <span>
                                    Enter unique code to get your reward
                                </span>
                            </div>
                            <TextField
                                id="code"
                                label={'Code'}
                                value={this.state.code}
                                onChange={this.handleChange('code')}
                                fullWidth
                                className={classes.inputs}
                            />
                            <Button variant="contained" color="primary" onClick={this.getReward} className={classes.button}>
                                Claim Reward
                            </Button>
                            {/*<span className={classes.forgot_password}>
                                Forgot password
                            </span>*/}
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(loginStyle)(Reward);
