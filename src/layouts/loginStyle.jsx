const appStyle = theme => ({
    root: {
        flexGrow: 1,
        width: '100vw',
        height : '100vh',
        backgroundColor : '#F1F5F9'
    },
    layout : {
        height : '100vh',
        justifyContent : 'center',
        paddingTop : '50px'
    },
    button : {
        marginTop : '20px',
        marginBottom : '10px'
    },
    forgot_password :{
        color : '#6772e5',
        fontSize : '9pt',
        fontWeight : 'bold',
        '&:hover' : {
            color : '#8991e5',
            cursor : 'pointer'
        }
    },
    logo :{
      width : '200px',
    },
    logoContainer : {
        textAlign : 'center'
    },
    inputs : {
      marginTop : '5px',
      marginBottom : '5px',
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        display:'flex',
        flexDirection : 'column',
    },
});

export default appStyle;
