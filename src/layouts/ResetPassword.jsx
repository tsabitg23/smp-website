import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// core components
import loginStyle from './loginStyle.jsx';
import {LINKS} from '../routes';
import {inject, observer} from 'mobx-react';
import FormBuilder from '../components/FormBuilder';
//Actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
//Import may not working with Reserved proxy so using require instead.
const logo = require('../assets/img/react_logo.svg');
const queryString = require('query-string');
import schema from 'async-validator';
const form_items = [
    {
        key : 'new_password',
        label : 'New Password',
        type: 'password'
    },
    {
        key : 'confirm_new_password',
        label : 'Confirm New Password',
        type : 'password'
    }
];
@connect(
    state => ({
        messageBox: state.messageBox || {},
    }),
    dispatch => ({
        actions: bindActionCreators(
            //Combined both messageBoxActions and NotificationActions into 1 object
            { ...messageBoxActions },
            dispatch
        )
    })
)

@inject('appstate')
@observer
class ResetPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email : '',
            formErrors : [],
            resetForm : false
        };
        this.authStore = props.appstate.auth;
        this.global_ui = props.appstate.global_ui;
    }

    componentDidMount(){
        this.props.appstate.global_ui.openLoader();
        let token = queryString.parse(this.props.location.search, { ignoreQueryPrefix: true }).token;
        let data = JSON.parse(atob(token.split('.')[1]));
        this.setState({
            ...data
        });
        this.props.appstate.global_ui.closeLoader();
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    changePassword = async ()=>{
        let rules = {
            new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ],
            confirm_new_password : [
                {
                    required : true,
                    type : 'string',
                    min : 6
                }
            ]
        };

        const validator = new schema(rules);
        validator.validate(this.state.formData, (errs, f) => {
            this.setState({formErrors : errs ? errs : []});
            if (errs) {
                console.log(errs);
            } else {
                if(this.state.formData.new_password !== this.state.formData.confirm_new_password){
                    this.setState({formErrors : [{message: 'New password and confirm did not match', field: 'confirm_new_password'}]});
                }
                else{
                    this.global_ui.openLoader();
                    this.authStore.resetPassword(Object.assign({id : this.state.id},this.state.formData)).then(async res=>{
                        // this.setState({resetForm : true});
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.SUCCESS,
                            'Success change password',
                            ()=>this.props.history.push(LINKS.LOGIN)
                        );
                    }).catch(err=>{
                        this.global_ui.closeLoader();
                        this.props.actions.showMessage(
                            MessageBoxType.DANGER,
                            err.message,
                            ()=>console.log('OK Clicked')
                        );
                    });
                }
            }
        });
    };

    onFormUpdate = (formData)=>{
        this.setState({formData});
    };

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                        aria-describedby="dialog-loading">
                    <DialogContent>
                        <CircularProgress/>
                    </DialogContent>
                </Dialog>
                <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
                <Grid container spacing={0} className={classes.layout}>
                    <Grid item xs={3}>
                        <div className={classes.logoContainer}>
                            <img src={logo} className={classes.logo}/>
                        </div>
                        <Paper className={classes.paper}>
                            <div style={{marginBottom: '20px'}}>
                                <Typography variant="h5" gutterBottom>
                                    Reset Password
                                </Typography>
                                <span>
                                    Fill this form to reset your password
                                </span>
                            </div>
                            <TextField
                                id="email"
                                label={'Email'}
                                value={this.state.email}
                                onChange={this.handleChange('email')}
                                fullWidth
                                disabled
                                className={classes.inputs}
                            />
                            <FormBuilder forms={form_items} reset={this.state.resetForm} afterReset={()=>this.setState({reset: false})} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                            <Button variant="contained" color="primary" onClick={this.changePassword} className={classes.button}>
                                Change Password
                            </Button>
                            {/*<span className={classes.forgot_password} onClick={()=>this.props.history.push(LINKS.LOGIN)}>*/}
                                {/*Back to login*/}
                            {/*</span>*/}
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(loginStyle)(ResetPassword);
