import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// core components
import loginStyle from './loginStyle.jsx';
import {appConfig} from '../../configs/app';
import {LINKS} from '../routes';
import {inject, observer} from 'mobx-react';
//Actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MessageBoxType from '../components/MessageBox/MessageBoxType';
import MessageBox from '../components/MessageBox';
import messageBoxActions from '../actions/MessageBox';
import * as firebase from 'firebase';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Dialog from '@material-ui/core/Dialog/Dialog';
//Import may not working with Reserved proxy so using require instead.
// const logo = require('../assets/img/rumahdps-logo.jpeg');
import logo from '../assets/img/logo.png';
var Recaptcha = require('react-recaptcha');

@connect(
  state => ({
    messageBox: state.messageBox || {},
  }),
  dispatch => ({
    actions: bindActionCreators(
      //Combined both messageBoxActions and NotificationActions into 1 object
      { ...messageBoxActions },
      dispatch
    )
  })
)
@inject('appstate')
@observer
class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email : '',
            password :'',
            token : '',
            captcha : '',
            url : window.location.href
        };
        this.recaptchaInstance;
        this.authStore = props.appstate.auth;
    }

    componentDidMount(){
        firebase.messaging().getToken().then(token=>{
            this.setState({token });
        });
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    login = async ()=>{
        this.props.appstate.global_ui.openLoader();
        if(!this.state.captcha && appConfig.useCaptcha){
            this.props.appstate.global_ui.closeLoader();
            this.props.actions.showMessage(
              MessageBoxType.DANGER,
              'Please verify by clicking the captcha',
              ()=>console.log('OK Clicked')
            );
            return
        };
        this.authStore.login(this.state).then(res=>{
            this.props.appstate.global_ui.closeLoader();
            this.props.history.push(LINKS.DASHBOARD);
        }).catch(err=>{
            this.props.appstate.global_ui.closeLoader();
            this.props.actions.showMessage(
              MessageBoxType.DANGER,
              err.message,
              ()=>console.log('OK Clicked')
            );
        });
    };

    forgotPassword = ()=>{
        this.props.history.push(LINKS.FORGOT_PASSWORD);
    };

    callback = () => {
      console.log('Done!!!! captcha');
    };

    verifyCallback = (response) => {
      console.log(response,'response captcha');
      this.setState({captcha : response})
    };

    expiredCallback = () => {
      console.log(`Recaptcha expired`);
      this.setState({captcha : ''})
    };

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <MessageBox {...this.props.messageBox} open={this.props.messageBox.open || false} />
                <Dialog open={this.props.appstate.global_ui.is_loading} onClose={()=>false} disableBackdropClick aria-labelledby="dialog-loading"
                        aria-describedby="dialog-loading">
                    <DialogContent>
                        <CircularProgress/>
                    </DialogContent>
                </Dialog>
                <Grid container spacing={0} className={classes.layout}>
                    <Grid item xs={12} md={3} lg={3}>
                        <div className={classes.logoContainer}>
                            <img src={logo} className={classes.logo}/>
                        </div>
                        <Paper className={classes.paper}>
                            <div style={{marginBottom: '20px'}}>
                                <Typography variant="h5" gutterBottom>
                                    {appConfig.project_name}
                                </Typography>
                                <span>
                                    Sign in to application
                                </span>
                            </div>
                            <TextField
                                id="email"
                                label={'Email'}
                                value={this.state.email}
                                onChange={this.handleChange('email')}
                                fullWidth
                                className={classes.inputs}
                            />
                            <TextField
                                id="password"
                                label={'Password'}
                                value={this.state.password}
                                type="password"
                                fullWidth
                                className={classes.inputs}
                                onChange={this.handleChange('password')}
                            />
                            <div style={{marginTop : '20px'}}>
                                <Recaptcha
                                    ref={e => this.recaptchaInstance = e}
                                    sitekey="6LcXNIUUAAAAAGzFqrP92BoU0ypI6CqG5hERQ8mI"
                                    render="explicit"
                                    verifyCallback={this.verifyCallback}
                                    onloadCallback={this.callback}
                                    expiredCallback={this.expiredCallback}
                                />
                            </div>
                            <Button variant="contained" color="primary" onClick={this.login} className={classes.button}>
                                Sign In
                            </Button>
                            <span className={classes.forgot_password} onClick={this.forgotPassword}>
                                Forgot password
                            </span>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(loginStyle)(Login);
