/*eslint no-console: ["off", { allow: ["warn", "error"] }] */
import linq from 'linq';
import uuidv4 from 'uuid/v4';
import Default from './constants.js';
//correct URL for Reserved proxy
//Get the millisecond of current time.
export var GetBaseUrl = function () {
    var key = 'BASE_URL';
    var base = window.sessionStorage.getItem(key) ||
        document.getElementsByTagName('base')[0].getAttribute('href') ||
        '/';
    window.sessionStorage.setItem(key, base);
    /* devblock:start */
    console.log("base URL is " + base);
    /* devblock:end */
    return base;
};
export var newGuid = function () { return uuidv4(); };
/**
 *getImgSrc for both normal hosting and Reverse proxy
 *
 * @export
 * @param {string} url the relative image url
 * @returns real url
 */
export function getImgSrc(url) {
    //if (typeof url !== 'string') return url;
    var base = GetBaseUrl();
    return !base || base === '/' || url.indexOf(base) >= 0
        ? url
        : base + "/" + url;
}
/**
 *Merge second array to first array if existed then update.
 *
 * @export
 * @param {Array} [fistArray=[]]
 * @param {Array} [secondArray=[]]
 * @param {Function} [selector=i => i.id]
 */
export function Merge(fistArray, secondArray, selector) {
    if (fistArray === void 0) { fistArray = []; }
    if (secondArray === void 0) { secondArray = []; }
    if (selector === void 0) { selector = function (i) { return i.id; }; }
    var firstQuery = linq.from(fistArray);
    var secondQuery = linq.from(secondArray);
    var news = secondQuery.where(function (i) {
        return firstQuery.all(function (f) { return selector(f) !== selector(i); });
    });
    var exists = secondQuery.where(function (i) {
        return firstQuery.any(function (f) { return selector(f) === selector(i); });
    });
    return firstQuery
        .select(function (i) {
        var found = exists.firstOrDefault(function (e) { return selector(e) === selector(i); });
        return found ? Object.assign({}, i, found) : i;
    })
        .union(news)
        .toArray();
}
export function getAvatar(avatar) {
    var tmp = avatar
        ? avatar.includes('data:image')
            ? avatar
            : "data:image/png;base64," + avatar
        : Default.DefaultAvatar;
    return tmp;
}
export function isValidEmail(email) {
    if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
        return false;
    }
    return true;
}
export function convertToFormData(object) {
    if (!object)
        return undefined;
    var formData = new FormData();
    for (var key in object)
        formData.append(key, object[key]);
    return formData;
}
