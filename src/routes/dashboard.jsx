// @material-ui/icons
import Dashboard from '@material-ui/icons/Dashboard';
import Person from '@material-ui/icons/Person';
import Build from '@material-ui/icons/Build';
import ContentPaste from '@material-ui/icons/Assessment';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import BubbleChart from '@material-ui/icons/BubbleChart';
import LocationOn from '@material-ui/icons/LocationOn';
import Notifications from '@material-ui/icons/Notifications';
import MessageBox from '@material-ui/icons/Message';
import People from '@material-ui/icons/People';
import ListAlt from '@material-ui/icons/ListAlt';
import Book from '@material-ui/icons/Book';
import AccountBox from '@material-ui/icons/AccountBox';
import PlaylistAddCheck from '@material-ui/icons/PlaylistAddCheck';
import Face from '@material-ui/icons/Face';
import Home from '@material-ui/icons/Home';
import Assignment from '@material-ui/icons/AssignmentInd';
import Timeline from '@material-ui/icons/Timeline';
import TrendingUp from '@material-ui/icons/TrendingUp';
import Description from '@material-ui/icons/Description';
import NoteAdd from '@material-ui/icons/NoteAdd';
import AttachMoney from '@material-ui/icons/AttachMoney';
import CheckBox from '@material-ui/icons/CheckBox';
import Report from '@material-ui/icons/Report';
import Class from '@material-ui/icons/Class';
import Calendar from '@material-ui/icons/CalendarToday';
import Event from '@material-ui/icons/Event';
import Search from '@material-ui/icons/Search';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import Feedback from '@material-ui/icons/Feedback';

// core components/views
//import DashboardPage from '../views/Dashboard/Dashboard.jsx';
//import UserProfile from '../views/UserProfile/UserProfile.jsx';
//import Use    rListing from '../views/UserProfile/UserListing.jsx';
//import TableList from '../views/TableList/TableList.jsx';
//import Typography from '../views/Typography/Typography.jsx';
//import Icons from '../views/Icons/Icons.jsx';
//import Maps from '../views/Maps/Maps.jsx';
//import NotificationsPage from '../views/Notifications/Notifications.jsx';
//import MessageBoxPage from '../views/MessageBox';
//import UpgradeToPro from '../views/UpgradeToPro/UpgradeToPro.jsx';
import {LINKS} from './index';

import Loader from '../views/loaders';
import EventsView from '../views/Events';
import {AccountBalance, AccountBalanceWallet} from "@material-ui/icons";
import BankAccountView from "../views/BankAccount";
import AccountView from "../views/AccountsView";

const DashboardPage = Loader(() =>
  import(/* webpackChunkName: "DashboardView" */ '../views/Dashboard/Dashboard.jsx')
);

const Users = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Users/index.jsx')
);
const Project = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Project/index.jsx')
);
const Agency = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Agency/index.jsx')
);
const Agent = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Agent/index.jsx')
);
const GuestBook = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/GuestBook/index.jsx')
);
const Employee = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Employee/index.jsx')
);
const MarktingPlan = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/MarketingPlan/index.jsx')
);
const Unit = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Unit/index.jsx')
);
const UnitVerification = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/UnitVerification/index.jsx')
);

const Customer = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Customer/index.jsx')
);

const ProjectTimeline = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectTimeline/index.jsx')
);

const UnitProgress = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/UnitProgress/index.jsx')
);

const ProjectPlanning = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectPlanning/index.jsx')
);

const ProjectJob = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectJob/index.jsx')
);

const ProjectPaymentRequest = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectPaymentRequest/index.jsx')
);

const ProjectQualityControl = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectQualityControl/index.jsx')
);

const ProjectReport = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectReport/index.jsx')
);

const MinuteOfMeeting = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/MinuteOfMeeting/index.jsx')
);

const ManagerQualityControl = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ManagerQualityControl/index.jsx')
);

const ProjectSupervision = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/ProjectSupervision/index.jsx')
);

const MRReport = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/MRReport/index.jsx')
);

const MRProjectJob = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/MRProjectJob/index.jsx')
);

const MRPaymentRequest = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/MRPaymentRequest/index.jsx')
);

const Penilaian = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Penilaian/index.jsx')
);

const Commission = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/Commission/index.jsx')
);

const CommissionRequest = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/PengajuanKomisi/index.jsx')
);

const CommissionReport = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/LaporanKomisi/index.jsx')
);

const CustomerAnalyst = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/CustomerAnalyst/index.jsx')
);

const HRDReports = Loader(() =>
    import(/* webpackChunkName: "DashboardView" */ '../views/HRDReports/index.jsx')
);
// const UserListing = Loader(() =>
//   import(/* webpackChunkName: "UserListingView" */ '../views/UserProfile/UserListing.jsx')
// );

// const TableList = Loader(() =>
//   import( webpackChunkName: "TableListView"  '../views/TableList/TableList.jsx')
// );

const Typography = Loader(() =>
  import(/* webpackChunkName: "TypographyView" */ '../views/Typography/Typography.jsx')
);

const Icons = Loader(() =>
  import(/* webpackChunkName: "IconsView" */ '../views/Icons/Icons.jsx')
);

const Maps = Loader(() =>
  import(/* webpackChunkName: "MapsView" */ '../views/Maps/Maps.jsx')
);

const NotificationsPage = Loader(() =>
  import(/* webpackChunkName: "NotificationsView" */ '../views/Notifications/Notifications.jsx')
);

const MessageBoxPage = Loader(() =>
  import(/* webpackChunkName: "MessageBoxView" */ '../views/MessageBox')
);

const UpgradeToPro = Loader(() =>
  import(/* webpackChunkName: "UpgradeToProView" */ '../views/UpgradeToPro/UpgradeToPro.jsx')
);

export default [
  {
    path: LINKS.DASHBOARD,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: Dashboard,
    component: DashboardPage,
    permission_key : 'for_all',
    // role_key : 'admin'
  },
  // {
  //   path: '/app/users_listing',
  //   sidebarName: 'User Listing',
  //   navbarName: 'User Listing',
  //   icon: Person,
  //   component: UserListing
  // },
    {
        path: LINKS.PROJECT,
        sidebarName: 'Project',
        navbarName: 'Project',
        icon: Build,
        component: Project,
        permission_key : 'projects',
        role_key : 'admin'
    },
    {
        path: LINKS.USER,
        sidebarName: 'Users',
        navbarName: 'Users',
        icon: Person,
        component: Users,
        permission_key : 'users',
        role_key : 'admin'
    },
    {
        path: LINKS.UNIT,
        sidebarName: 'Unit',
        navbarName: 'Unit',
        icon: Home,
        component: Unit,
        permission_key : 'units',
        role_key : 'marketing'
    },
    {
        path: LINKS.AGENCY,
        sidebarName: 'Agency',
        navbarName: 'Agency',
        icon: People,
        component: Agency,
        permission_key : 'agencies',
        role_key : 'marketing'
    },
    {
        path: LINKS.AGENT,
        sidebarName: 'Mitra Pemasaran',
        navbarName: 'Mitra Pemasaran',
        icon: Assignment,
        component: Agent,
        permission_key : 'agents',
        role_key : 'marketing'
    },
    {
        path: LINKS.EVENTS,
        sidebarName: 'Event',
        navbarName: 'Event',
        icon: Event,
        component: EventsView,
        permission_key : 'events',
        role_key : 'marketing'
    },
    {
        path: LINKS.COMMISSION_REQUEST,
        sidebarName: 'Pengajuan Komisi',
        navbarName: 'Pengajuan Komisi',
        icon: NoteAdd,
        component: CommissionRequest,
        permission_key : ['komisi', 'manager_marketing', 'manager_keuangan', 'manager_pimpro'],
        role_key : 'marketing'
    },
    {
        path: LINKS.COMMISSION_REPORT,
        sidebarName: 'Laporan Komisi',
        navbarName: 'Laporan Komisi',
        icon: NoteAdd,
        component: CommissionReport,
        permission_key : ['komisi', 'manager_marketing', 'manager_keuangan', 'manager_pimpro'],
        role_key : 'marketing'
    },
    {
        path: LINKS.KOMISI,
        sidebarName: 'Komisi',
        navbarName: 'Komisi',
        icon: AttachMoney,
        component: Commission,
        permission_key : 'komisi',
        role_key : 'marketing'
    },
    {
        path: LINKS.EVENTS,
        sidebarName: 'Event',
        navbarName: 'Event',
        icon: Event,
        component: EventsView,
        permission_key : 'approval_events',
        role_key : 'pimpro'
    },
    {
        path: LINKS.GUEST_BOOK,
        sidebarName: 'Guest Book',
        navbarName: 'Guest Book',
        icon: Book,
        component: GuestBook,
        permission_key : 'guest_book',
        role_key : 'marketing'
    },
    {
        path: LINKS.MARKETING_PLAN,
        sidebarName: 'Marketing Plan',
        navbarName: 'Marketing Plan',
        icon: ListAlt,
        component: MarktingPlan,
        permission_key : 'marketing_plans',
        role_key : 'marketing'
    },
    {
        path: LINKS.CUSTOMER,
        sidebarName: 'Customer',
        navbarName: 'Customer',
        icon: Face,
        component: Customer,
        permission_key : 'customers',
        role_key : 'hrd'
    },
    {
        path: LINKS.EMPLOYEE,
        sidebarName: 'Karyawan',
        navbarName: 'Karyawan',
        icon: AccountBox,
        component: Employee,
        permission_key : 'employees',
        role_key : 'hrd'
    },
    {
        path: LINKS.EMPLOYEE_SCORES,
        sidebarName: 'Penilaian',
        navbarName: 'Penilaian',
        icon: ContentPaste,
        component: Penilaian,
        permission_key : 'employee_score',
        role_key : 'hrd'
    },
    {
        path: LINKS.UNIT_VERIFICATION,
        sidebarName: 'Customer Verfication',
        navbarName: 'Customer Verfication',
        icon: PlaylistAddCheck,
        component: UnitVerification,
        permission_key : 'customer_verification',
        role_key : 'hrd'
    },
    {
        path: LINKS.PROJECT_TIMELINE,
        sidebarName: 'Project Timeline',
        navbarName: 'Project Timeline',
        icon: Timeline,
        component: ProjectTimeline,
        permission_key : 'timeline_project',
        role_key : 'supervisor'
    },
    {
        path: LINKS.UNIT_PROGRESS,
        sidebarName: 'Unit Progress',
        navbarName: 'Unit Progress',
        icon: TrendingUp,
        component: UnitProgress,
        permission_key : 'unit_progress',
        role_key : 'supervisor'
    },
    {
        path: LINKS.PROJECT_PLANNING,
        sidebarName: 'Pengawasan Project',
        navbarName: 'Pengawasan Rencana Project',
        icon: Description,
        component: ProjectPlanning,
        permission_key : 'project_plannings',
        role_key : 'supervisor'
    },
    {
        path: LINKS.PROJECT_JOB,
        sidebarName: 'Kebutuhan Project',
        navbarName: 'Kebutuhan Rencana Project',
        icon: NoteAdd,
        component: ProjectJob,
        permission_key : 'project_jobs',
        role_key : 'supervisor'
    },
    {
        path: LINKS.PROJECT_PAYMENT_REQUEST,
        sidebarName: 'Pembiayaan Project',
        navbarName: 'Pembiayaan Project',
        icon: AttachMoney,
        component: ProjectPaymentRequest,
        permission_key : 'project_payment_request',
        role_key : 'supervisor'
    },
    {
        path: LINKS.PROJECT_QUALITY_CONTROL,
        sidebarName: 'Temuan',
        navbarName: 'Temuan',
        icon: CheckBox,
        component: ProjectQualityControl,
        permission_key : 'project_quality_control',
        role_key : 'supervisor'
    },
    {
        path: LINKS.PROJECT_REPORT,
        sidebarName: 'Report',
        navbarName: 'Report',
        icon: Report,
        component: ProjectReport,
        permission_key : ['project_report','recommend_project_report'],
        role_key : 'supervisor'
    },
    {
        path: LINKS.MR_QUALITY_CONTROL,
        sidebarName: 'Temuan',
        navbarName: 'Temuan',
        icon: Search,
        component: ManagerQualityControl,
        permission_key : 'quality_control',
        role_key : 'pimpro'
    },
    {
        path: LINKS.PROJECT_SUPERVISION,
        sidebarName: 'Pengawasan',
        navbarName: 'Pengawasan',
        icon: RemoveRedEye,
        component: ProjectSupervision,
        permission_key : 'project_supervision',
        role_key : 'pimpro'
    },
    {
        path: LINKS.MR_PROJECT_REPORT,
        sidebarName: 'Rekomendasi',
        navbarName: 'Rekomendasi',
        icon: Feedback,
        component: MRReport,
        permission_key : 'recommend_project_report',
        role_key : 'pimpro'
    },
    {
        path: LINKS.MR_PROJECT_JOB,
        sidebarName: 'Kebutuhan Project',
        navbarName: 'Kebutuhan Project',
        icon: NoteAdd,
        component: MRProjectJob,
        permission_key : 'approve_project_jobs',
        role_key : 'pimpro'
    },
    {
        path: LINKS.MR_PROJECT_PAYMENT_REQUEST,
        sidebarName: 'Pembiayaan',
        navbarName: 'Pembiayaan',
        icon: AttachMoney,
        component: MRPaymentRequest,
        permission_key : 'approve_project_payment_request',
        role_key : 'pimpro'
    },
    {
        path: LINKS.MINUTE_OF_MEETING,
        sidebarName: 'Notulen',
        navbarName: 'Notulen',
        icon: Class,
        component: MinuteOfMeeting,
        permission_key : ['minutes_of_meetings', 'manager_pimpro'],
        role_key : 'pimpro'
    },{
        path: LINKS.CUSTOMER_ANALYST,
        sidebarName: 'Analisa Customer',
        navbarName: 'Analisa Customer',
        icon: Class,
        component: CustomerAnalyst,
        permission_key : ['customer_analyst', 'manager_hrd'],
        role_key : 'hrd'
    },
    {
        path: LINKS.HRD_REPORT,
        sidebarName: 'Laporan Mingguan',
        navbarName: 'Laporan Mingguan',
        icon: Calendar,
        component: HRDReports,
        permission_key : ['report_mingguan', 'manager_pimpro','manager_hrd'],
        role_key : 'hrd'
    },
    {
        path: LINKS.BANK_ACCOUNT,
        sidebarName: 'Rekening',
        navbarName: 'Rekening',
        icon: AccountBalanceWallet,
        component: BankAccountView,
        permission_key : 'rekening_bank',
        role_key : 'keuangan'
    },
    {
        path: LINKS.ACCOUNTS,
        sidebarName: 'Akun',
        navbarName: 'Akun',
        icon: AccountBalance,
        component: AccountView,
        permission_key : 'akun',
        role_key : 'keuangan'
    },
  // {
  //   path: '/app/table',

  //   sidebarName: 'Table List',
  //   navbarName: 'Table List',
  //   icon: ContentPaste,
  //   component: TableList
  // },
//   {
//     path: '/app/typography',
//     sidebarName: 'Typography',
//     navbarName: 'Typography',
//     icon: LibraryBooks,
//     component: Typography
//   },
  // {
  //   path: '/app/icons',
  //   sidebarName: 'Icons',
  //   navbarName: 'Icons',
  //   icon: BubbleChart,
  //   component: Icons
  // },
  // {
  //   path: '/app/maps',
  //   sidebarName: 'Maps',
  //   navbarName: 'Map',
  //   icon: LocationOn,
  //   component: Maps
  // },
//   {
//     path: '/app/messagebox',
//     sidebarName: 'Message Box',
//     navbarName: 'Message Box',
//     icon: MessageBox,
//     component: MessageBoxPage
//   },
  // {
  //   path: '/upgrade-to-pro',
  //   sidebarName: 'Upgrade To PRO',
  //   navbarName: 'Upgrade To PRO',
  //   icon: Unarchive,
  //   component: UpgradeToPro
  // },
  { redirect: true, path: '/', to: LINKS.DASHBOARD, navbarName: 'Redirect' }
];
