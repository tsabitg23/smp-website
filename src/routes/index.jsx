//import Dashboard from '../layouts/Dashboard.jsx';
import Loader from '../views/loaders';

const Dashboard = Loader(() =>
  import(/* webpackChunkName: "Dashboard" */ '../layouts/Dashboard.jsx')
);

const Login = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/Login.jsx')
);

const Reward = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/Reward.jsx')
);

const ForgotPassword = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/ForgotPassword.jsx')
);

const ResetPassword = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/ResetPassword.jsx')
);

const Verification = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/VerificationUser.jsx')
);

const PrintVerif = Loader(() =>
    import(/* webpackChunkName: "Dashboard" */ '../layouts/PrintVerification.jsx')
);

export default [
        { path: '/app', component: Dashboard ,type : 'private'},
        { path: '/login', component: Login},
        { path: '/reward', component: Reward},
        { path: '/cashflow', component: Reward},
        { path: '/forgot_password', component: ForgotPassword},
        { path: '/reset_password', component: ResetPassword},
        { path: '/verification_user/:id', component: Verification},
        { path: '/print_verification/:id', component: PrintVerif},
    ];

export const LINKS = {
    LOGIN : '/login',
    APP : '/app',
    FORGOT_PASSWORD : '/forgot_password',
    RESET_PASSWORD : '/reset_password',
    DASHBOARD : '/app/dashboard',
    USER : '/app/users',
    PROJECT : '/app/project',
    UNIT: '/app/unit',
    UNIT_DETAIL: '/app/unit/:id',
    UNIT_VERIFICATION : '/app/customer_verify',
    UNIT_VERIFICATION_DETAIL : '/app/customer_verify/:id',
    AGENCY : '/app/agency',
    AGENT :  '/app/agent',
    GUEST_BOOK : '/app/guest_book',
    MARKETING_PLAN : '/app/marketing_plan',
    MARKETING_PLAN_DETAIL : '/app/marketing_plan/:id',
    EMPLOYEE : '/app/employee',
    EVENTS : '/app/events',
    EVENT_DETAIL : '/app/events/:id',
    ACCOUNT : '/app/account',
    REWARD : '/reward',
    CUSTOMER : '/app/customers',
    CUSTOMER_DETAIL : '/app/customers/:id',
    PROJECT_TIMELINE : '/app/project_timeline',
    UNIT_PROGRESS : '/app/unit_progress',
    PROJECT_PLANNING : '/app/project_planning',
    PROJECT_JOB : '/app/project_requirement',
    PROJECT_PAYMENT_REQUEST : '/app/project_payment_request',
    PROJECT_QUALITY_CONTROL : '/app/quality_control',
    PROJECT_REPORT : '/app/project_report',
    MINUTE_OF_MEETING : '/app/notulen',
    MINUTE_OF_MEETING_DETAIL : '/app/notulen/:id',
    MR_QUALITY_CONTROL : '/app/temuan',
    PROJECT_SUPERVISION : '/app/supervision',
    MR_PROJECT_REPORT : '/app/recommendation',
    MR_PROJECT_REPORT_DETAIL : '/app/recommendation/:id',
    MR_PROJECT_JOB : '/app/approve_job',
    EMPLOYEE_SCORES : '/app/penilaian',
    EMPLOYEE_SCORE_DETAIL : '/app/penilaian/:id',
    MR_PROJECT_JOB_DETAIL : '/app/approve_job/:id',
    MR_PROJECT_PAYMENT_REQUEST : '/app/approve_request_payment',
    MR_PROJECT_PAYMENT_REQUEST_DETAIL : '/app/approve_request_payment/:id',
    KOMISI : '/app/komisi',
    COMMISSION_REQUEST : '/app/pengajuan_komisi',
    COMMISSION_REQUEST_DETAIL : '/app/pengajuan_komisi/:id',
    COMMISSION_REPORT : '/app/laporan_komisi',
    COMMISSION_REPORT_DETAIL : '/app/laporan_komisi/:id',
    CUSTOMER_ANALYST : '/app/customer_analyst',
    CUSTOMER_ANALYST_DETAIL : '/app/customer_analyst/:id',
    HRD_REPORT : '/app/hrd_report',
    HRD_REPORT_DETAIL : '/app/hrd_report/:id',
    BANK_ACCOUNT : '/app/bank_account',
    ACCOUNTS : '/app/akun',
};
