import {LINKS} from './index';
import Loader from '../views/loaders';
const Account = Loader(() =>
    import('../views/Account/index.jsx')
);
const UnitDetail = Loader(() =>
    import('../views/UnitDetail/index.jsx')
);
const UnitVerificationDetail = Loader(() =>
    import('../views/UnitVerificationDetail/test.jsx')
);

const CustomerDetail = Loader(() =>
    import('../views/CustomerDetail/index.jsx')
);

const MRProjectJobDetail = Loader(() =>
    import('../views/MRProjectJob/Detail.jsx')
);

const MRReportDetail = Loader(() =>
    import('../views/MRReport/Detail.jsx')
);

const MRPaymentRequestDetail = Loader(() =>
    import('../views/MRPaymentRequest/Detail.jsx')
);

const PenilaianDetail = Loader(() =>
    import('../views/PenilaianDetail/index.jsx')
);

const PengajuanKomisiDetail = Loader(() =>
    import('../views/PengajuanKomisiDetail/index.jsx')
);

const LaporanKomisiDetail = Loader(() =>
    import('../views/LaporanKomisiDetail/index.jsx')
);

const NotulenDetail = Loader(() =>
    import('../views/MinuteOfMeeting/Detail.jsx')
);

const EventDetail = Loader(() =>
    import('../views/EventDetail/index.jsx')
);

const MarketingPlanDetail = Loader(() =>
    import('../views/MarketingPlan/detail.jsx')
);

const CustomerAnalystDetail = Loader(() =>
    import('../views/CustomerAnalystDetail/index.jsx')
);


const HRDReportDetail = Loader(() =>
    import('../views/HRDReports/Detail.jsx')
);

const NotificationsPage = Loader(() =>
  import(/* webpackChunkName: "NotificationsView" */ '../views/Notifications/index.jsx')
);

export default [
    {
        path: LINKS.ACCOUNT,
        navbarName : 'Account',
        component: Account
    },
    {
        path: LINKS.UNIT_DETAIL,
        navbarName : 'Unit Detail',
        component: UnitDetail,
        includes : LINKS.UNIT
    },
    {
        path: LINKS.UNIT_VERIFICATION_DETAIL,
        navbarName : 'Unit Verification Detail',
        component: UnitVerificationDetail,
        includes : LINKS.UNIT_VERIFICATION
    },
    {
        path: LINKS.CUSTOMER_DETAIL,
        navbarName : 'Customer Detail',
        component: CustomerDetail,
        includes : LINKS.CUSTOMER
    },
    {
        path: LINKS.MR_PROJECT_JOB_DETAIL,
        navbarName : 'Kebutuhan Detail',
        component: MRProjectJobDetail,
        includes : LINKS.MR_PROJECT_JOB
    },
    {
        path: LINKS.MR_PROJECT_REPORT_DETAIL,
        navbarName : 'Rekomendasi Detail',
        component: MRReportDetail,
        includes : LINKS.MR_PROJECT_REPORT
    },
    {
        path: LINKS.MR_PROJECT_PAYMENT_REQUEST_DETAIL,
        navbarName : 'Pembiayaan Detail',
        component: MRPaymentRequestDetail,
        includes : LINKS.MR_PROJECT_PAYMENT_REQUEST
    },
    {
        path: LINKS.EMPLOYEE_SCORE_DETAIL,
        navbarName : 'Penilaian Detail',
        component: PenilaianDetail,
        includes : LINKS.EMPLOYEE_SCORES
    },
    {
        path: LINKS.EVENT_DETAIL,
        navbarName : 'Event Detail',
        component: EventDetail,
        includes : LINKS.EVENTS
    },
    {
        path: LINKS.COMMISSION_REQUEST_DETAIL,
        navbarName : 'Permintaan Komisi Detail',
        component: PengajuanKomisiDetail,
        includes : LINKS.COMMISSION_REQUEST
    },
    {
        path: LINKS.COMMISSION_REPORT_DETAIL,
        navbarName : 'Laporan Distribusi Komisi',
        component: LaporanKomisiDetail,
        includes : LINKS.COMMISSION_REPORT
    },
    {
        path: LINKS.MINUTE_OF_MEETING_DETAIL,
        navbarName : 'Notulen',
        component: NotulenDetail,
        includes : LINKS.MINUTE_OF_MEETING
    },
    {
        path: LINKS.MARKETING_PLAN_DETAIL,
        navbarName : 'Marketing Plan',
        component: MarketingPlanDetail,
        includes : LINKS.MARKETING_PLAN
    },
    {
        path: LINKS.CUSTOMER_ANALYST_DETAIL,
        navbarName : 'Customer Analyst',
        component: CustomerAnalystDetail,
        includes : LINKS.CUSTOMER_ANALYST
    },
    {
        path: LINKS.HRD_REPORT_DETAIL,
        navbarName : 'Laporan Mingguan',
        component: HRDReportDetail,
        includes : LINKS.HRD_REPORT
    },
    {
        path: '/app/notifications',
        navbarName: 'Notifications',
        component: NotificationsPage,
        includes : '/app/notification'
      },


];
