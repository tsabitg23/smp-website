import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class UnitAkadData extends BaseStore{
    mode = 'multi';
    url = '/unit_akad_data';
    reqQuery = {
        show_all : true
    }

    constructor(props){
        super(props);
    }
}
