import {action,computed} from 'mobx';
import {BaseStore} from './base_store';
import {get} from 'lodash';

export class CustomerUnit extends BaseStore{
    mode = 'multi';
    constructor(props){
        super(props);
    }

    get url(){
        return `/customer/${get(this.context.customer.selectedData,'id','937a8f9c-7ebf-44a1-9d29-ef64963bd1c0')}/unit`;
    }
}