import {BaseStore} from './base_store';
import {computed} from 'mobx';

export class AssessmentAspect extends BaseStore{
    mode = 'multi';
    url = '/assessment_aspect';

    constructor(props){
        super(props);
    }

}