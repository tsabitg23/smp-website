import {BaseStore} from './base_store';
import {action, computed} from 'mobx';
import {pick} from 'lodash';
export class Customer extends BaseStore{
    mode = 'multi';
    url = '/customer';

    constructor(props){
        super(props);
    }

    @action
    async getCustomerAkad(){
        return this.http.get('customer_akad')
            .then(res=>{
                this.data = res.data;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

}
