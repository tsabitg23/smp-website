import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectQualityControl extends BaseStore{
    mode = 'multi';
    url = '/project_quality_control';
    reqQuery = {
    	show_all : true,
    }
    
    constructor(props){
        super(props);
    }
}