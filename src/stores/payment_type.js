import {BaseStore} from './base_store';

export class PaymentType extends BaseStore{
    mode = 'multi';
    url = '/payment_type';
    reqQuery = {
        show_all : false,
        order_by : 'name_asc',
    };

    constructor(props){
        super(props);
    }
}