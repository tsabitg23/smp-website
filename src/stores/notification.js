import {BaseStore} from './base_store';
import {
    action
} from 'mobx';
import {has} from 'lodash';
import {LINKS} from '../routes/index';

export class Notification extends BaseStore{
    mode = 'multi';
    url = '/notification';

    constructor(props){
        super(props);
    }

    @action
    read(id,afterUpdate=false){
    	this.isLoading = true;
        return this.http.put(this.url + '/read/' + id, {})
            .then(res => {
                this.isLoading = false;
                if(afterUpdate){
                    this.getAll();
                }
                return res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    getUnread(id,afterUpdate=false){
      this.isLoading = true;
        return this.http.get(this.url + '?read_at=null&show_all=true', {})
            .then(res => {
                this.isLoading = false;
                return res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    getRouteDestination(type,additional_data='{}'){
      let data = JSON.parse(additional_data);
      
      if(type === 'project_jobs'){
        let id = has(data,'project_job_id') ? data.project_job_id : '' 
        return LINKS.MR_PROJECT_JOB +'/'+ id
      }
      else if(type === 'project_jobs_approved' || type === 'project_jobs_rejected'){
        let id = has(data,'project_job_id') ? data.project_job_id : '' 
        return LINKS.MR_PROJECT_JOB +'/'+ id
      }
      else if(type === 'project_payment_request'){
        let id = has(data,'project_payment_request_id') ? data.project_payment_request_id : '' 
        return LINKS.MR_PROJECT_PAYMENT_REQUEST +'/'+ id
      }
      else if(type === 'project_payment_request_approved' || type === 'project_payment_request_rejected'){
        let id = has(data,'project_payment_request_id') ? data.project_payment_request_id : '' 
        return LINKS.MR_PROJECT_PAYMENT_REQUEST +'/'+ id
      }
      else if(type === 'project_report'){
        let id = has(data,'project_report_id') ? data.project_report_id : '' 
        return LINKS.MR_PROJECT_REPORT +'/'+ id
      }
      else if(type === 'project_report_answered'){
        let id = has(data,'project_report_id') ? data.project_report_id : '' 
        return LINKS.MR_PROJECT_REPORT +'/'+ id
      }
      else{
        return '/app/notifications'
      }
    }

}