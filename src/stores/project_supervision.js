import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectSupervision extends BaseStore{
    mode = 'multi';
    url = '/project_supervision';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}