import {BaseStore} from './base_store';
import {action} from 'mobx';

export class CommissionReport extends BaseStore{
    mode = 'multi';
    url = '/commission_report';

    constructor(props){
        super(props);
    }
}