import {BaseStore} from './base_store';
import {action, observable} from 'mobx';

export class Accounts extends BaseStore{
    mode = 'multi';
    url = '/accounts';
    @observable parent1 = [];
    @observable parent2 = [];
    reqQuery = {
        show_all : true,
        order_by : 'code_asc'
    }

    constructor(props){
        super(props);
    }

    @action
    async getParent(){
        await this.http.get('accounts_header')
            .then(res=>{
                this.parent1 = res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    async getByParent(val){
        await this.http.get(`accounts_child?parent_id=${val}`)
            .then(res=>{
                this.parent2 = res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }
}
