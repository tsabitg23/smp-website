import {action} from 'mobx';
import {BaseStore} from './base_store';
import {queryStringBuilder} from "../utils/query_builder";

export class Unit extends BaseStore{
    mode = 'multi';
    url = '/unit';
    constructor(props){
        super(props);
    }

    @action
    async uploadFile(file){
    	return this.http.upload(file);
    }

    @action
    async downloadCSV(file){
        return this.http.post('/download_unit_template',{
            filename : file
        });
    }

    @action
    async exportData(){
        return this.http.get('/unit_export');
    }

    @action
    async getUnitByAgentIdKomisi(agentId){
        await this.http.get(`get_unit_akad_by_agent_id/${agentId}`)
            .then(res=>{
                this.data = res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    async getUnitByCustomer(customerId){
        await this.http.get(`get_customer_unit/${customerId}`)
            .then(res=>{
                this.data = res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    async getUnitBookedByPeriod(range){
        const q = queryStringBuilder(range);
        return this.http.get(`unit_akad_by_period?${q}`)
            .then(res=>{
                return res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    setUnitEmpty(){
        this.data = [];
    }
}
