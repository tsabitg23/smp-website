import { observable,computed,action } from 'mobx';

export class Authentication {
	constructor(context){
		this.context = context;
		this.http = context.http;
	}

	@action
	async login(data){
		let res = await this.http.post('/authentication/login',data);
		await this.context.setToken(res.token);
		await this.context.setPermission();
		return res;
	}

    @action
    async logout(){
        let res = await this.http.post('/authentication/logout',{token : this.context.token});
        this.context.setToken('');
        return res;
    }

    @action
    async changePassword(data){
        return await this.http.post('/authentication/change_password',data);
    }

    @action
    async changePasswordAdmin(data){
        return await this.http.post('/authentication/change_password_admin',data);
    }

    @action
	async forgotPassword(data){
        return await this.http.post('/authentication/forgot_password',data);
	}

    @action
    async resetPassword(data){
        return await this.http.post('/authentication/reset_password',data);
    }
}