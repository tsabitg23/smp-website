import {BaseStore} from './base_store';
import {computed, observable} from 'mobx';

export class EmployeeScore extends BaseStore{
    mode = 'multi';
    url = '/employee_score';
    @observable pegawai_form = {};
    @observable sikap_form = {};
    @observable tanggung_jawab_form = {};
    @observable kompetensi_form = {};
    @observable perencanaan_form = {};
    @observable pengorganisasian_form = {};
    @observable pengarahan_form = {};
    @observable pemecahan_masalah_form = {};
    @observable kemampuan_interpersonal = {};
    @observable kemampuan_berkomunikasi = {};
    constructor(props){
        super(props);
    }
}