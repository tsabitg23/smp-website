import {BaseStore} from './base_store';

export class HRDReports extends BaseStore{
    mode = 'multi';
    url = '/hrd_reports';

    constructor(props){
        super(props);
    }

}
