import {BaseStore} from './base_store';

export class CustomerAnalyst extends BaseStore{
    mode = 'multi';
    url = '/customer_analyst';

    constructor(props){
        super(props);
    }

}
