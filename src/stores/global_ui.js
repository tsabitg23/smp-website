import {action,observable} from 'mobx';

export class GlobalUI {
    constructor(context){
        this.context = context;
    }
    @observable active_route = '/';
    @observable is_loading = false;

    @action
    openLoader(){
        this.is_loading = true;
    }

    @action
    closeLoader(){
        this.is_loading = false;
    }
}