import {BaseStore} from './base_store';

export class BankAccounts extends BaseStore{
    mode = 'multi';
    url = '/bank_accounts';
    reqQuery = {
        show_all : true
    }

    constructor(props){
        super(props);
    }
}
