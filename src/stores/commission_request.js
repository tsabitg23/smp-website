import {BaseStore} from './base_store';
import {action} from 'mobx';

export class CommissionRequest extends BaseStore{
    mode = 'multi';
    url = '/commission_request';

    constructor(props){
        super(props);
    }
}