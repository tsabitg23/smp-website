import {BaseStore} from './base_store';
import {action} from 'mobx';

export class UnitVerification extends BaseStore{
    mode = 'multi';
    url = '/unit_akad';

    constructor(props){
        super(props);
    }

    @action
    async sendEmail(id,email){
        return this.http.post(this.url + '/'+id+'/send_verification',{
            email_to : email
        });
    }

    @action
    async getNewData(){
        return this.http.get(this.url+"_new").then(res=>{
            this.selectedData = {
                ...res
            }
        })
    }

    @action
    async saveNewData(data){
        return this.http.post(this.url+"_new_save", data).then(res=>{
            return res;
        })
    }
}