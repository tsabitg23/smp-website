import {BaseStore} from './base_store';
import {action, observable} from 'mobx';
import { queryStringBuilder } from '../utils/query_builder';

export class Agent extends BaseStore{
    mode = 'multi';
    url = '/agent';

    constructor(props){
        super(props);
    }

    @observable chart_two = [];

    @action
    async uploadFile(file){
        return this.http.upload(file, this.url+'_import');
    }

    @action
    async getChartTwo(query={}) {
        this.isLoading = true;
        const q = queryStringBuilder(query);

        const res = await this.http.get(`${this.url}_chart_two?${q}`)
            .catch(err => {
                this.isLoading = false;
                throw err;
            });

        this.isLoading = false;
        this.chart_two = res;
        return res;
    }

}