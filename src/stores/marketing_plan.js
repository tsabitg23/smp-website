import {BaseStore} from './base_store';

export class MarketingPlan extends BaseStore{
    mode = 'multi';
    url = '/marketing_plan';

    constructor(props){
        super(props);
    }

}