import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class User extends BaseStore{
    mode = 'multi';
    url = '/user';
    constructor(props){
        super(props);
    }
}