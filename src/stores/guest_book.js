import {BaseStore} from './base_store';
import {action} from 'mobx';
export class GuestBook extends BaseStore{
    mode = 'multi';
    url = '/guest_book';

    constructor(props){
        super(props);
    }

    @action
    async uploadFile(file){
    	return this.http.upload(file, this.url+'_import');
    }

    @action
    async exportData(){
        return this.http.get('/guest_book_export');
    }
}