import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectPaymentRequest extends BaseStore{
    mode = 'multi';
    url = '/project_payment_request';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}