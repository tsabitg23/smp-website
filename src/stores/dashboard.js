import {BaseStore} from './base_store';

export class Dashboard extends BaseStore{
    mode = 'multi';
    url = '/dashboard';

    constructor(props){
        super(props);
    }

}