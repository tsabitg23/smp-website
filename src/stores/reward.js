import {action} from 'mobx';
export class Reward {
    constructor(context){
        this.context = context;
    }

    @action
    claimReward(code){
        return this.context.http.download('/reward/claim',{
            code : code
        });
    }
}