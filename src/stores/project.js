import {computed,action} from 'mobx';
import {BaseStore} from './base_store';
import * as jwt from 'jsonwebtoken';
import {appConfig} from '../../configs/app';

export class Project extends BaseStore{
    mode = 'multi';
    url = '/project';
    
    @computed get selectedProject() {
        return this.context.userData.selected_project || "not-found";
    }

    constructor(props){
        super(props);
    }

    @action
    setProject(id){
        // this.selectedProject = id;
        let tokenData = this.context.userData;
        tokenData.selected_project = id;
        const token = jwt.sign(tokenData, appConfig.secret);
        this.context.setToken(token);
    }
}