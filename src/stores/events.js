import {BaseStore} from './base_store';
import {action} from 'mobx';

export class Events extends BaseStore{
    mode = 'multi';
    url = '/events';
    form = {
        evaluation : '',
        achievement : '',
        target : '',
        funds : [],
        crew : []
    };

    constructor(props){
        super(props);
    }

    @action
    updateForm(key, val){
        this.form[key] = val;
    }

}
