import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class Role extends BaseStore{
    mode = 'multi';
    url = '/role';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}