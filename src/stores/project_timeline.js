import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectTimeline extends BaseStore{
    mode = 'multi';
    url = '/project_timeline';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}