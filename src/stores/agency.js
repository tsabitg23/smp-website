import {BaseStore} from './base_store';

export class Agency extends BaseStore{
    mode = 'multi';
    url = '/agency';

    constructor(props){
        super(props);
    }

}