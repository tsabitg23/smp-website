import {BaseStore} from './base_store';
import {action} from "mobx";

export class Employee extends BaseStore{
    mode = 'multi';
    url = '/employee';

    constructor(props){
        super(props);
    }

    @action
    async uploadFile(file){
        return this.http.upload(file, this.url+'_import');
    }

    @action
    async exportData(){
        return this.http.get('/employee_export');
    }
}