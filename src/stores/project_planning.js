import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectPlanning extends BaseStore{
    mode = 'multi';
    url = '/project_planning';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}