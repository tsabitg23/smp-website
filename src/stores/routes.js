import {computed} from 'mobx';
import dashboardRoute from '../routes/dashboard';

export class Routes{
    constructor(context){
        this.context = context;
    }

    filterRoutes = (route)=>{
        
        if(typeof route.permission_key == 'string' && route.permission_key === 'for_all'){
            return true;
        }
        let data;
        if(typeof route.permission_key == 'string'){
            data = this.context.user_permission.data.find(it=>it.permission.key === route.permission_key);
        } else if(typeof route.permission_key == 'object'){
            data = this.context.user_permission.data.find(it=>route.permission_key.indexOf(it.permission.key) > -1);
        } else {
            return false
        };

        if(data){
            return data.read;
        }else{
            return false;
        }
    };

    @computed get dashboard(){
        return dashboardRoute.filter(this.filterRoutes);
    }
}