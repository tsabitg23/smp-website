import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectReport extends BaseStore{
    mode = 'multi';
    url = '/project_report';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }

    
    async reject(id, data, afterUpdate){
        return this.http.put(this.url + '/' + id+'/reject' , data)
            .then(res => {
                this.isLoading = false;
                if(afterUpdate){
                    this.getAll();
                }
                return res;
            })
            .catch(err => {
                this.isLoading = false;
                throw err;
            });
    }
}