import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class UnitProgress extends BaseStore{
    mode = 'multi';
    url = '/unit_progress';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}