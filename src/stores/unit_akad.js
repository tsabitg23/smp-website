import {action,computed} from 'mobx';
import {BaseStore} from './base_store';
import {get} from 'lodash';

export class UnitAkad extends BaseStore{
    mode = 'multi';
    constructor(props){
        super(props);
    }

    get url(){
        return `/unit/${get(this.context.unit.selectedData,'id','937a8f9c-7ebf-44a1-9d29-ef64963bd1c0')}/akad`;
    }
}