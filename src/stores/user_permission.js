import {BaseStore} from './base_store';

export class UserPermission extends BaseStore{
    mode = 'multi';
    url = '/user_permission';
    reqQuery = {
        show_all : true
    };

    constructor(props){
        super(props);
    }
}