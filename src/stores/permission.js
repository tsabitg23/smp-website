import {BaseStore} from './base_store';

export class Permission extends BaseStore{
    mode = 'multi';
    url = '/permission';
    reqQuery = {
        show_all : false,
        order_by : 'name_asc',
    };

    constructor(props){
        super(props);
    }
}