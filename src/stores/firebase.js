/**
 * Created by 322 on 01/04/2017.
 */
import * as firebase from "firebase";
import {observable} from 'mobx';


export class Firebase {

  @observable tokenMessage = '';

  constructor(context) {
    this.context = context;
    this.init();
  }

  init() {
    const config = {
      apiKey: "AIzaSyBXNwyMTmeNOU79nMufi9rFmhMsg0E2j9o",
      authDomain: "smp-project-1579f.firebaseapp.com",
      databaseURL: "https://smp-project-1579f.firebaseio.com",
      projectId: "smp-project-1579f",
      storageBucket: "smp-project-1579f.appspot.com",
      messagingSenderId: "11826847359"
    };
    firebase.initializeApp(config);
    const messaging = firebase.messaging();
    messaging.requestPermission()
      .then(function() {
        console.log('Notification permission granted.');
      })
      .catch(function(err) {
        console.log('Unable to get permission to notify.', err);
      });

    
  }

}
