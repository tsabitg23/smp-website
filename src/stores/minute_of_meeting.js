import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class MinuteOfMeeting extends BaseStore{
    mode = 'multi';
    url = '/minute_of_meeting';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}