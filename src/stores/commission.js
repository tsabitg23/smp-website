import {BaseStore} from './base_store';
import {action} from 'mobx';

export class Commission extends BaseStore{
    mode = 'multi';
    url = '/commission';

    constructor(props){
        super(props);
    }
}