import {observable} from 'mobx';
import {BaseStore} from './base_store';

export class ProjectJob extends BaseStore{
    mode = 'multi';
    url = '/project_job';
    reqQuery = {
    	show_all : true
    }
    
    constructor(props){
        super(props);
    }
}