import {action} from 'mobx';
import {BaseStore} from './base_store';

export class File extends BaseStore{
    constructor(props){
        super(props);
    }

    @action
    async uploadFile(file){
    	return this.http.upload(file);
    }

    @action
    async downloadCSV(file){
        return this.http.post('/download_template',{
            filename : file
        });
    }
}