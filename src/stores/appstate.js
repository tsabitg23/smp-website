import { observable,computed,action } from 'mobx';
import {Http} from './http';
import {Authentication} from './auth';
import {Firebase} from './firebase';
import {GlobalUI} from './global_ui';
import {User} from './user';
import {Role} from './role';
import {Permission} from './permission';
import {Project} from './project';
import {Agency} from './agency';
import {Agent} from './agent';
import {File} from './file';
import {GuestBook} from './guest_book';
import {UserPermission} from './user_permission';
import {Employee} from './employee';
import {Events} from './events';
import {Routes} from './routes';
import {MarketingPlan} from './marketing_plan';
import {Unit} from './unit';
import {Reward} from './reward';
import {UnitAkad} from './unit_akad';
import {Customer} from './customer';
import {Dashboard} from './dashboard';
import {UnitVerification} from './unit_verification';
import {PaymentType} from './payment_type';
import {CustomerUnit} from './customer_unit'
import {ProjectTimeline} from './project_timeline'
import {UnitProgress} from './unit_progress'
import {ProjectPlanning} from './project_planning'
import {ProjectJob} from './project_job'
import {ProjectPaymentRequest} from './project_payment_request'
import {ProjectQualityControl} from './project_quality_control'
import {ProjectReport} from './project_report'
import {MinuteOfMeeting} from './minute_of_meeting'
import {ProjectSupervision} from './project_supervision'
import {Notification} from './notification'
import {EmployeeScore} from './employee_score'
import {AssessmentAspect} from './assessment_aspect'
import {Commission} from './commission';
import {CommissionRequest} from './commission_request';
import {CommissionReport} from './commission_report';
import {CustomerAnalyst} from "./customer_analyst";
import {HRDReports} from "./hrd_reports";
import {UnitAkadData} from "./unit_akad_data";
import {BankAccounts} from "./bank_accounts";
import {Accounts} from "./accounts";

export default class Appstate {
	@observable token = '';
	@observable userPermissions = [];

  http = new Http(this);
  auth = new Authentication(this);
  firebase = new Firebase(this);
  global_ui = new GlobalUI(this);
  user = new User(this);
  role = new Role(this);
  permission = new Permission(this);
  project = new Project(this);
  dashboard = new Dashboard(this);
  agency = new Agency(this);
  agent = new Agent(this);
  guest_book = new GuestBook(this);
  user_permission = new UserPermission(this);
  routes = new Routes(this);
  employee = new Employee(this);
  marketing_plan = new MarketingPlan(this);
  unit = new Unit(this);
  reward = new Reward(this);
  unit_akad = new UnitAkad(this);
  customer = new Customer(this);
  customer_unit = new CustomerUnit(this);
  unit_verification = new UnitVerification(this);
  payment_type = new PaymentType(this);
  project_timeline = new ProjectTimeline(this);
  unit_progress = new UnitProgress(this);
  project_planning = new ProjectPlanning(this);
  project_job = new ProjectJob(this);
  project_payment_request = new ProjectPaymentRequest(this);
  project_quality_control = new ProjectQualityControl(this);
  project_report = new ProjectReport(this);
  minute_of_meeting = new MinuteOfMeeting(this);
  project_supervision = new ProjectSupervision(this);
  notification = new Notification(this);
  employee_score = new EmployeeScore(this);
  assessment_aspect = new AssessmentAspect(this);
  events = new Events(this);
  file = new File(this);
  commission = new Commission(this);
  commission_request = new CommissionRequest(this);
  commission_report = new CommissionReport(this);
  customer_analyst = new CustomerAnalyst(this);
  hrd_reports = new HRDReports(this);
  unit_akad_data = new UnitAkadData(this);
  bank_accounts = new BankAccounts(this);
  accounts = new Accounts(this);

	constructor(initialState) {
		this.token = initialState.token;
		if (typeof window !== 'undefined') {
			if (!this.token) {
			localStorage.removeItem('id_token');
			} else {
			localStorage.setItem('id_token', this.token);
			}
		}
		if(this.token){
            this.setPermission();
        }
	}

	@action
	setToken(token) {
		this.token = token;
		localStorage.setItem('id_token', token);
	}

	@action
    setPermission(){
        this.user_permission.getAll();
    }

	getPermission(key){
    let permissions;
    if(typeof key == 'string'){
      permissions = this.user_permission.data.find(it=>it.permission.key === key);
    } else if(typeof key == 'object') {
      permissions = this.user_permission.data.find(it=>{
        return key.indexOf(it.permission.key) > -1;
      });
    }
    if(permissions){
        return {
            read : permissions.read,
            create : permissions.create,
            delete : permissions.delete,
            update : permissions.update,
        };
    }
    else{
        return {
            read : false,
            create : false,
            update : false,
            delete : false
        };
    }


    }

	@computed get userData() {
      //really important
    const tokenCheck = this.token;
    const token = localStorage.getItem('id_token');
    if (!token) {
      return {
        user_id: '',
        role: ''
      };
    }

    let tokenData = JSON.parse(atob(token.split('.')[1]));

    return tokenData;
  }
}
