import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import AddIcon from '@material-ui/icons/Add';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import TextField from '@material-ui/core/TextField';
import {get} from 'lodash';

export default class TableInput extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data : props.defaultData,
        };
        props.form.map(prop=>{
            this.state[prop.key] = '';
        });
    }

    changeText = (val,form,number=false)=>{
		let data = number ? parseInt(val) : val;
		this.setState({[form.key] : data});
	};


    getForm = (form)=>{
		const {classes,theme} = this.props;
        const selectStyles = {
            input: base => ({
                ...base,
                color: '#000',
                '& input': {
                    font: 'inherit',
                },
            }),
        };

        if(form.type === 'email' ||form.type === 'text' || form.type === 'password' || form.type === 'number' || form.type === 'percentage'){
			return (
				<TextField
					id={form.key}
					value={get(this.state,form.key,undefined)}
					onChange={(e)=>this.changeText(e.target.value,form,(form.type === 'percentage'))}
					margin="normal"
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
			);
		}
		else if(form.type.includes('autocomplete')){
			let errorMessage = get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null;
			let errorLabelComponent = (errorMessage) ? <span className={classes.errorMessageFont}> ({errorMessage})</span> : <span/>;
			let isMulti = form.type.includes('multi');
			let options = (form.required === false) ?  [{
				value : null,
				label : "--EMPTY--"
			}] : [];
			return (
				<NoSsr>
                    <FormLabel error={get(this.state,`${form.key}-error`,false)} component="legend" className={classes.autocompleteMargin}>{form.label}{form.required ? '*' : ""}{errorLabelComponent}</FormLabel>
					<Select
						classes={classes}
						styles={selectStyles}
						options={options.concat(form.items)}
						components={components}
						value={get(this.state,form.key+'-autocomplete','')}
						onChange={(data)=>this.changeAutocomplete(data,form)}
						placeholder={get(form,'placeholder',null)}
						isMulti={isMulti}
					/>
				</NoSsr>
			);
		}
		else if(form.type === 'multiline'){
			return (
				<TextField
					id={form.key}
					value={get(this.state,form.key,undefined)}
					onChange={(e)=>this.changeText(e.target.value,form)}
					margin="normal"
					multiline
					fullWidth
					rows={get(form,'rows',4)}
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`) : null}
					// type={form.type}
				/>
			);
		}
		else if(form.type === 'select'){
			return (
				<TextField
					id={form.key}
					select
					// value={get(this.state,form.key,form.items[0].value)}
					value={get(this.state,form.key,'')}
					onChange={(e)=>this.changeText(e.target.value,form)}
					margin="normal"
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
					SelectProps={{
						MenuProps: {
							className: classes.menu,
						},
					}}
				>
					{form.items.map(option => (
						<MenuItem key={option.value} value={option.value}>
							{option.label}
							</MenuItem>
					))}
				</TextField>
			);
		}
		else if(form.type === 'date'){
			return (
                <TextField
                    id={form.key}
                    value={get(this.state,form.key,undefined)}
                    onChange={(e)=>this.changeText(e.target.value,form)}
                    margin="normal"
                    required={get(form,'required',true)}
                    disabled={get(form,'disabled',false)}
                    placeholder={get(form,'placeholder',null)}
                    type={form.type}
                    error={get(this.state,`${form.key}-error`,false)}
                    InputLabelProps={{
                        shrink: true,
                        style : {minWidth : '400px'}
                    }}
                    helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
                />
			);
		}
    };

    addData = () =>{
        const data = this.state.data;
        const pushObject = {};
        const resetObject ={}
        const error = [];
        this.props.form.map(prop=>{
            if(!this.state[prop.key] && (prop.required !== false)){
                const errorKey = prop.key+'-error';
                const errorMessageKey = prop.key+'-error-message';
                this.setState({
                    [errorKey] : true,
                    [errorMessageKey] : `${prop.label} tidak boleh kosong`
                })
                error.push(prop.key)
            } else {
                pushObject[prop.key] = this.state[prop.key];
                resetObject[prop.key] = '';
                resetObject[`${prop.key}-error`] = false;
                resetObject[`${prop.key}-error-message`] = '';
            }
        })
        if(error.length === 0){
            data.push(pushObject);
            this.setState({
                ...resetObject,
                data
            }, this.props.onChangeData(data));
        }
    }

    deleteData = (index) => {
        const data = this.state.data;
        data.splice(index, 1);
        this.setState({
            data
        });
    }

    renderSummaryRow = () => {
        return (
            <React.Fragment>
                {
                    this.props.summary.map((prop)=>{
                        return (
                            <TableRow key={prop.key}>
                                <TableCell>{prop.name}</TableCell>
                                {
                                    this.props.form.map((col,index)=>{
                                        if(prop.column_index.includes(index)){
                                            return <TableCell>BRUH</TableCell>;
                                        }else{
                                            return <TableCell></TableCell>;
                                        }
                                    })
                                }
                            </TableRow>
                        );
                    })
                }
            </React.Fragment>
        );
    }



    render(){
        const inputRow = (
            <TableRow>
                <TableCell component="th" scope="row" style={{width: '3%'}}>
                    <span style={{fontWeight : 'bold', fontSize: 24}}></span>
                </TableCell>
                {
                    this.props.form.map(prop=>{
                        return (
                            <TableCell key={prop.key}>
                                {this.getForm(prop)}
                            </TableCell>
                        );
                    })
                }
                <TableCell>
                    <IconButton onClick={this.addData}>
                        <AddIcon />
                    </IconButton>
                </TableCell>
            </TableRow>
        )
        return (
            <div>
                <Typography variant="h6" id="tableTitle" style={{marginBottom:20}}>
                    {this.props.title}
                </Typography>
                <Paper className={{width: '100%'}}>
                    <Table className={{minWidth:'700px'}}>
                        <TableHead>
                            <TableRow>
                                <TableCell style={{width: '3%'}}>No</TableCell>
                                {
                                    this.props.form.map(prop=>{
                                        return (
                                            <TableCell style={{fontWeight:'bold'}} key={prop.key}>{prop.label}</TableCell>
                                        )
                                    })
                                }
                                {
                                    !this.props.readonly && (<TableCell>Action</TableCell>)
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {this.state.data.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell component="th" scope="row" style={{width: '3%'}}>
                                    {index+1}
                                </TableCell>
                                {
                                    this.props.form.map(prop=>{
                                        return (
                                            <TableCell key={prop.key}>{row[prop.key]}</TableCell>
                                        )
                                    })
                                }
                                {
                                    !this.props.readonly && (
                                        <TableCell>
                                            <IconButton style={{color:'red'}} onClick={()=>this.deleteData(index)}>
                                                <DeleteIcon/>
                                            </IconButton>

                                        </TableCell>
                                    )
                                }
                            </TableRow>
                        ))}
                            {!this.props.readonly && inputRow}
                            {this.props.summary.length > 0 && this.props.summary.map((prop)=>{
                                return (
                                    <TableRow key={prop.key}>
                                        <TableCell style={{width: '10%'}}>{prop.name}</TableCell>
                                        {
                                            this.props.form.map((col,index)=>{
                                                if(prop.column_index.includes(index)){
                                                    let values = this.state.data.reduce((total,val)=>{
                                                        return total+= (+val[col.key]);
                                                    },0)
                                                    if(prop.operation === 'mean' && this.state.data.length > 0){
                                                        values = values/this.state.data.length;
                                                    }
                                                    return <TableCell key={index}>{values}</TableCell>;
                                                }else{
                                                    return <TableCell key={index}/>;
                                                }
                                            })
                                        }
                                    </TableRow>
                                )})
                            }
                        </TableBody>
                    </Table>
                    </Paper>
            </div>
        )
    }
}

TableInput.defaultProps = {
    defaultData : [],
    summary : [],
    readonly: false
}
