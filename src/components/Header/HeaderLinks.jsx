import React,{Component} from 'react';
//import { Manager, Target, Popper } from "react-popper";
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Popper from '@material-ui/core/Popper';
import Popover from '@material-ui/core/Popover';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
// @material-ui/icons
import Person from '@material-ui/icons/Person';
// core components
import Button from '../CustomButtons/Button.jsx';
import {get} from 'lodash';
import NotificationCenter from '../../components/Notification';
import headerLinksStyle from './headerLinksStyle';
import {Tooltip} from '@material-ui/core';
import {LINKS} from '../../routes';
import {inject, observer} from 'mobx-react';

@inject('appstate')
@observer
class HeaderLinks extends Component{
    state ={
        menuAccount : false
    };

    constructor(props){
        super(props);
        this.userData = props.appstate.userData;
        this.authStore = props.appstate.auth;
    }

    componentDidMount(){

    }

    handleClose = ()=>{
        if (this.buttonAccount.contains(event.target)) {
            return;
        }

        this.setState({ menuAccount: false });
    };

    handleLogout = async ()=>{
        await this.authStore.logout();
        this.props.history.push(LINKS.LOGIN);
    };

    openAccount = ()=>{
        this.props.history.push(LINKS.ACCOUNT);
    };

    render(){
        const {classes,
            notifications,
            onNotificationChange,
            onNotificationDelete,
            notificationBackgroundImage,
            others} = this.props;
        return (
            <div>
                <NotificationCenter
                    {...others}
                    ButtonProps={{
                        color: window.innerWidth > 959 ? 'transparent' : 'white',
                        justIcon: window.innerWidth > 959,
                        simple: !(window.innerWidth > 959),
                        classes: {
                            button: classes.buttonLink,
                            icon: classes.icons
                        }
                    }}
                    changePage={get(this.props.history,'push',()=>{})}
                    items={notifications}
                    onChange={onNotificationChange}
                    onDelete={onNotificationDelete}
                    image={notificationBackgroundImage}
                />
                {/*<Tooltip*/}
                    {/*classes={{ tooltip:'NotificationCenter-tooltip-153', popper: 'NotificationCenter-arrowPopper-154'}}*/}
                    {/*title="Account"*/}
                {/*>*/}
                    <Button
                        color={window.innerWidth > 959 ? 'transparent' : 'white'}
                        justIcon={window.innerWidth > 959}
                        simple={!(window.innerWidth > 959)}
                        aria-label="Person"
                        buttonRef={node=>{
                            this.buttonAccount = node;
                        }}
                        aria-owns={this.state.menuAccount ? 'menu-account' : null}
                        className={classes.buttonLink}
                        round={true}
                        onClick={(event)=>this.setState({menuAccount : !this.state.menuAccount})}
                        aria-haspopup="true"
                    >
                        <Person className={classes.icons} />
                    </Button>
                {/*</Tooltip>*/}
              <ClickAwayListener onClickAway={this.handleClose}>
                <Popover
                  open={this.state.menuAccount}
                  anchorEl={this.buttonAccount}
                  anchorReference={"anchorEl"}
                  anchorPosition={{ top: 200, left: 400 }}
                  onClose={()=>this.setState({menuAccount : !this.state.menuAccount})}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                >
                  <Paper>
                          <List className={classes.paperMenu}>
                              <ListItem>
                                  <ListItemText primary={this.userData.email} secondary={this.userData.role}/>
                              </ListItem>
                          </List>
                          <MenuList>
                              <MenuItem onClick={this.openAccount}>Account</MenuItem>
                              <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                          </MenuList>
                  </Paper>
                </Popover>
              </ClickAwayListener>
                {/*<Popper open={this.state.menuAccount} anchorEl={this.buttonAccount} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            id="menu-list-grow"
                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <List className={classes.paperMenu}>
                                        <ListItem>
                                            <ListItemText primary={this.userData.email} secondary={this.userData.role}/>
                                        </ListItem>
                                    </List>
                                    <MenuList>
                                        <MenuItem onClick={this.openAccount}>Account</MenuItem>
                                        <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
                */}
            </div>
        );
    }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
