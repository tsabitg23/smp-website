import React from 'react';
import TextField from '@material-ui/core/TextField';
import {get} from 'lodash';
import Select from 'react-select';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import {pick} from 'lodash';
import moment from 'moment';
import NoSsr from '@material-ui/core/NoSsr';
import FormLabel from '@material-ui/core/FormLabel';
import Chip from '@material-ui/core/Chip';
import CancelIcon from '@material-ui/icons/Cancel';
import classNames from 'classnames';
import { TimePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/moment';
import {Decimal} from 'decimal.js';
function NoOptionsMessage(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function inputComponent({ inputRef, ...props }) {
    return <div ref={inputRef} {...props} />;
}

function Control(props) {
    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: props.selectProps.classes.input,
                    inputRef: props.innerRef,
                    children: props.children,
                    ...props.innerProps,
                },
            }}
            {...props.selectProps.textFieldProps}
        />
    );
}

function Option(props) {
    return (
        <MenuItem
            buttonRef={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontWeight: props.isSelected ? 500 : 400,
            }}
            {...props.innerProps}
        >
            {props.children}
        </MenuItem>
    );
}

function Placeholder(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function SingleValue(props) {
    return (
        <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
            {props.children}
        </Typography>
    );
}

function ValueContainer(props) {
    return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function Menu(props) {
    return (
        <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

const components = {
    Control,
    Menu,
    MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
};
class FormBuilder extends React.Component{
	constructor(props){
		super(props);
		let data = this.props.value;
		Object.keys(data).map(it=>{
			let form = this.props.forms.find(fo=>fo.key === it);
			if(form && form.type === 'date'){
				data[it] = moment(data[it]).format('YYYY-MM-DD');
			}
			if(form && form.type === 'time'){
				data[it] = new Date();
			}
			else if(form && form.type.includes('autocomplete')){
				let type = form.type.includes('multi');
				if(type){
					data[`${it}-autocomplete`] = form.items.filter(item=>data[it].find(prop=>prop.project_id === item.value));
				}
				else{
					let autoCompleteData = undefined;
					if(data[it]){
						const filteredForm = form.items.find(item=>item.value === data[it]);
						if(filteredForm){
							autoCompleteData = filteredForm;
						}
						else if(form && form.label_key_if_empty){
							autoCompleteData = {
								label : get(this.props.value,`${form.label_key_if_empty}`,''),
								value : data[it]
							};
						} else {
							autoCompleteData = undefined;
						}

					}
					data[`${it}-autocomplete`] = autoCompleteData;
				}
			}

			if(data[it] == null){
				data[it] = '';
			}

			if(form && form.onChange){
				form.onChange(data[it]);
			}
		});
		this.state = Object.assign({},this.props.value);

		let valueArray = Object.keys(this.props.value);
		if(valueArray.length === 0 && this.props.forms.length > 0){
			this.props.forms.map(it=>{
				this.state[it.key] = '';
			});
		}
		this.formUpdate();
	}

	componentDidUpdate(prevProps){
		// if(Object.keys(prevProps.value).length !== Object.keys(this.props.value).length){
        //     let data = this.props.value;
        //     Object.keys(data).map(it=>{
        //         let form = this.props.forms.find(fo=>fo.key === it);
        //         if(form && form.type === 'date'){
        //             data[it] = moment(data[it]).format('YYYY-MM-DD');
        //         }
        //         else if(form && form.type.includes('autocomplete')){
        //             let type = form.type.includes('multi');
        //             if(type){
        //                 data[`${it}-autocomplete`] = form.items.filter(item=>data[it].find(prop=>prop.project_id === item.value));
        //             }
        //             else{
        //                 data[`${it}-autocomplete`] = form.items.find(item=>item.value === data[it]);
        //             }
        //         }
        //     });
        //     this.setState({...data});
		// }
		if(prevProps.reset !== this.props.reset && this.props.reset){
			let data = this.state;
			Object.keys(data).map(it=>{
				data[it] = '';
			});

			this.setState(data);
			this.props.afterReset(false);
		}
		if(prevProps.formErrors.length !== this.props.formErrors.length){
			if(this.props.formErrors.length === 0){
				let change = {};
				this.props.forms.map(it=>{
					change[`${it.key}-error`] = false;
					change[`${it.key}-error-message`] = null;
				});
				this.setState(change);
			}
			else{
				let change = {};
				this.props.forms.map(it=>{
					let errorKey = this.props.formErrors.find(err=>err.field === it.key);
					change[`${it.key}-error`] = !!errorKey;
					change[`${it.key}-error-message`] = errorKey ? errorKey.message : null;
				});
				// this.props.formErrors.map(it=>{
				// 	change[`${it.field}-error`] = true;
				// 	change[`${it.field}-error-message`] = it.message;
				// })
				this.setState(change);
			}
		}
	}

	formUpdate = (form)=>{
		let data = pick(this.state,['id',...this.props.forms.map(it=>it.key)]);
		Object.keys(data).map(it=>{
			if(it.includes('_id')){
				data[it] = data[it] || null;
			}
		});
		this.props.onFormUpdate(data);
	};

	changeText = (val,form,number=false)=>{
		let data = number ? parseInt(val) : val;
		this.setState({[form.key] : data},this.formUpdate);
		if(form.onChange){
			if(form.twin_form){
				if(this.state[form.twin_form]){
					form.onChange({
						[form.twin_form] : this.state[form.twin_form],
						[form.key] : data
					}, (newKey,newVal)=>{
						this.setState({
							[newKey] : newVal
						});
					});
				}
			} else {
				form.onChange({
					[form.key] : data
				});
			}
		}
	};

	changeTimePicker = (data, form) => {
		this.setState({
			[form.key] : data
		}, this.formUpdate);
	}

    changeAutocomplete = (val,form)=>{
    	let data = val;
    	if(!form.type.includes('multi')){
    		data = val.value;
    	}
        this.setState({[form.key] : data,[`${form.key}-autocomplete`] : val},()=>{
			this.formUpdate();
			if(form.onChange){
				if(form.empty_key && form.empty_key.length > 0){
					const data = this.state;
					form.empty_key.map(prop=>{
						data[prop] = '';
					});
					this.setState(data);
				}
				form.onChange(val.value);
			}
		});
    };

	getForm = (form)=>{
		const {classes,theme} = this.props;
        const selectStyles = {
            input: base => ({
                ...base,
                color: '#000',
                '& input': {
                    font: 'inherit',
                },
            }),
        };

        if(form.type === 'email' ||form.type === 'text' || form.type === 'password' || form.type === 'number' || form.type === 'percentage'){
			return (
				<TextField
					id={form.key}
					label={form.label}
					value={get(this.state,form.key,undefined)}
					onChange={(e)=>this.changeText(e.target.value,form,(form.type === 'percentage'))}
					margin="normal"
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
			);
		}
		else if(form.type === 'autofill'){
			return (
				<TextField
					id={form.key}
					label={form.label}
					value={get(this.props.forms[form.source_index].items.find(prop=>prop.value === this.state[form.source_key]),`${[form.data_key]}`,'-')}
					// onChange={(e)=>this.changeText(e.target.value,form,(form.type === 'percentage'))}
					margin="normal"
					required={false}
					disabled={true}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
			);
		}else if(form.type === 'autofill_operator'){
			// const source_one = +get(this.state,`${[form.source_index[0]]}`,0) || 0;
			// const source_two = +get(this.state,`${[form.source_index[1]]}`,0) || 0;
			const column = form.formula.map(prop=>(+get(this.state,prop.key,0) || 0) > 0);
			let value = 0;
			if(!column.includes(false)){
				let val = form.formula.reduce((total, val,index)=>{
					const n1 = +get(this.state,val.key,0) || 0;
					return total[val.operator](new Decimal(n1));
				},new Decimal(0));
				value = val.toFixed((form.fixed > -1) ? form.fixed : 2);
			}
			return (
				<TextField
					id={form.key}
					label={form.label}
					value={value}
					// onChange={(e)=>this.changeText(e.target.value,form,(form.type === 'percentage'))}
					margin="normal"
					required={false}
					disabled={true}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
				/>
			);
		}
		else if(form.type.includes('autocomplete')){
			let errorMessage = get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null;
			let errorLabelComponent = (errorMessage) ? <span className={classes.errorMessageFont}> ({errorMessage})</span> : <span/>;
			let isMulti = form.type.includes('multi');
			let options = (form.required === false) ?  [{
				value : null,
				label : "--EMPTY--"
			}] : [];
			return (
				<NoSsr>
                    <FormLabel error={get(this.state,`${form.key}-error`,false)} component="legend" className={classes.autocompleteMargin}>{form.label}{form.required ? '*' : ""}{errorLabelComponent}</FormLabel>
					<Select
						classes={classes}
						styles={selectStyles}
						options={options.concat(form.items)}
						components={components}
						value={get(this.state,form.key+'-autocomplete','')}
						onChange={(data)=>this.changeAutocomplete(data,form)}
						placeholder={get(form,'placeholder',null)}
						isMulti={isMulti}
					/>
				</NoSsr>
			);
		}
		else if(form.type === 'multiline'){
			return (
				<TextField
					id={form.key}
					label={form.label}
					value={get(this.state,form.key,undefined)}
					onChange={(e)=>this.changeText(e.target.value,form)}
					margin="normal"
					multiline
					fullWidth
					rows={get(form,'rows',4)}
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`) : null}
					// type={form.type}
				/>
			);
		}
		else if(form.type === 'select'){
			return (
				<TextField
					id={form.key}
					select
					label={form.label}
					// value={get(this.state,form.key,form.items[0].value)}
					value={get(this.state,form.key,'')}
					onChange={(e)=>this.changeText(e.target.value,form)}
					margin="normal"
					required={get(form,'required',true)}
					disabled={get(form,'disabled',false)}
					placeholder={get(form,'placeholder',null)}
					type={form.type}
					fullWidth
					error={get(this.state,`${form.key}-error`,false)}
					helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
					SelectProps={{
						MenuProps: {
							className: classes.menu,
						},
					}}
				>
					{form.items.map(option => (
						<MenuItem key={option.value} value={option.value}>
							{option.label}
							</MenuItem>
					))}
				</TextField>
			);
		}
		else if(form.type === 'date'){
			return (
                <TextField
                    id={form.key}
                    label={form.label}
                    value={get(this.state,form.key,undefined)}
                    onChange={(e)=>this.changeText(e.target.value,form)}
                    margin="normal"
                    required={get(form,'required',true)}
                    disabled={get(form,'disabled',false)}
                    placeholder={get(form,'placeholder',null)}
                    type={form.type}
                    error={get(this.state,`${form.key}-error`,false)}
                    InputLabelProps={{
                        shrink: true,
                        style : {minWidth : '400px'}
                    }}
                    helperText={get(this.state,`${form.key}-error`,false) ? get(this.state,`${form.key}-error-message`): null}
                />
			);
		}
		else if(form.type === 'time') {
			return (
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<TimePicker
						margin="normal"
						label="Time picker"
						value={get(this.state,form.key,new Date()) || new Date()}
						onChange={(date)=>this.changeTimePicker(date, form)}
					/>
				</MuiPickersUtilsProvider>
			);
		}
	};

	render(){
		return (
			<div>
				{
					this.props.forms.map((form,index)=>{
						return (
							<div key={index}>
								{
									this.getForm(form)
								}
							</div>
						);
					})
				}
			</div>
		);
	}
}
FormBuilder.defaultProps = {
  forms: [],
  value: {},
	reset : false,
	afterReset : ()=>{},
	formErrors : [],
};
export default withStyles(styles)(FormBuilder);
