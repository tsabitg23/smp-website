import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import dashboardStyle from '../../views/Dashboard/dashboardStyle.jsx';
import Grid from '@material-ui/core/Grid';
import Card from '../Card/Card.jsx';
import CardHeader from '../Card/CardHeader.jsx';
import CardIcon from '../Card/CardIcon.jsx';
import CardBody from '../Card/CardBody.jsx';
import CardFooter from '../Card/CardFooter.jsx';
import GridItem from '../Grid/GridItem.jsx';
import Icon from '@material-ui/core/Icon';
import {startCase,orderBy} from 'lodash';
import { PieChart, Pie,Tooltip,Cell,ResponsiveContainer } from 'Recharts';
import {toJS} from 'mobx';

class TopStatus extends React.Component{
	renderPieChart = (prop,key)=>{
	  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
	  const { classes } = this.props;

	  let dataPie = toJS(prop.value)
	  dataPie.map(it=>{
	    it.value = parseInt(it.value);
	    it.name = startCase(it.name)
	    return it;
	  })

	  return (
	    <GridItem xs={12} sm={6} md={3} key={key}>
	       <Card chart>
	        <CardHeader color="success">
	          <ResponsiveContainer  width={'100%'} height={200}>
	          <PieChart width={'100%'} height={'100%'}>
	                  <Pie
	                    data={dataPie} 
	                    dataKey={'value'}
	                    cx={'50%'} 
	                    cy={'50%'}
	                    outerRadius={80} 
	                    fill="#8884d8"
	                  >
	                  {
	                    dataPie.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
	                  }
	              </Pie>
	              <Tooltip/>
	            </PieChart>
	            </ResponsiveContainer>
	        </CardHeader>
	        <CardBody>
	          <h4 className={classes.cardTitle}>{prop.title}</h4>
	        </CardBody>
	        <CardFooter chart>
	          <div className={classes.stats}>
	            <Icon className={classes.iconSub}>{prop.subtitle_icon}</Icon> {" "+prop.subtitle}
	          </div>
	        </CardFooter>
	      </Card>
	    </GridItem>
	  )
	};

	renderCountCard = (prop,key)=>{
	  const { classes } = this.props;
	  return (
	    <GridItem xs={12} sm={6} md={3} key={key}>
	      <Card>
	        <CardHeader color="success" stats icon>
	          <CardIcon color="success">
	            <Icon className={classes.iconDash}>{prop.icon}</Icon>
	          </CardIcon>
	          <p className={classes.cardCategory}>{prop.title}</p>
	          <h3 className={classes.cardTitle}>{prop.value}</h3>
	        </CardHeader>
	        <CardFooter stats>
	          <div className={classes.stats}>
	            <Icon className={classes.iconSub}>{prop.subtitle_icon}</Icon> {" "+prop.subtitle}
	          </div>
	        </CardFooter>
	      </Card>
	    </GridItem>
	  )  
	}

	render(){
		return (
			<div>
				<Grid container>
		          {
		            this.props.data.filter(it=>it.type === 'count').map((prop,key)=>{
		                return this.renderCountCard(prop,key)
		            })
		          }
		        </Grid>
	        </div>
		)
	}
}
TopStatus.defaultProps = {
	data : []
}
export default withStyles(dashboardStyle)(TopStatus)

