import React from 'react';
import PropTypes from 'prop-types';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
// core components
import tableStyle from './tableStyle';
import ActionCell from './ActionCell';
import moment from 'moment';
import {get,startCase} from 'lodash';
import classNames from 'classnames';
import * as numeral from 'numeral';

const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
    TablePaginationActions,
);

export class CustomTable extends React.Component{
  state = {
    rowsPerPage : 15,
    page: 0,
  };

  handleChangePage = (event, page) =>{
      this.setState({ page });
  };

  handleChangeRowPerPage = (event) =>{
      this.setState({ rowsPerPage: event.target.value });
  };

  getColor = (index, data)=>{
      if(this.props.stripped){
          return { background: index % 2 ? '#f0f0f0' : 'white' };
      }

      if(this.props.dataHeaderActive && data.is_header){
          return { background: this.props.dataHeaderColor };
      }
      return {};
  };

  getValue = (data,column,index)=>{
      let value = get(data,column.key,'-');
      if(column.type === 'money'){
        let money = value;
        if(money === '-' || !money){
            money = 0;
        }
        return 'Rp. '+numeral(money).format('0,0');
    }
      if(column.type === 'range'){
        const ranges = column.key.split(',');
        return moment(get(data,ranges[0],'-')).format('dddd, DD-MM-YYYY') + " - " + moment(get(data,ranges[1],'-')).format('dddd, DD-MM-YYYY');
      }
      if(column.type === 'boolean'){
        if(value){
            return column.true_value;
        }  else {
            return column.false_value;
        }
      }
      if(column.type === 'branch'){
          const data = column.branch.find(prop=>value === prop.value)
          if(data){
              return data.text
          }
          return '-';
      }
      if(column.key === 'task.status' || column.key === 'status'){
        return value === 'created' ? 'Waiting' : startCase(value)
      }
      if(column.key === 'created_at' || column.key === 'survei_date' || column.key === 'join_date' || column.type === 'date'){
        return (value && value != 'Invalid date') ? moment(value).format('dddd, DD-MM-YYYY') : '-';
      }
      if(column.key === 'info_source' || column.key === 'status' || column.key === 'category'){
          return startCase(value);
      }
      if(column.key === 'no' && value === '-'){
          return index+1;
      }
      if(data.is_header && column.key ==='score'){
          return "";
      }
      if(column.type === 'number'){
          return value || '0';
      } else {
          return value || '-';
      }
  };

  render(){
      const {
          classes,
          theme,
          tableHead,
          tableData,
          tableHeaderColor,
          actionColumns,
          customWidth
      } = this.props;

      const {
        rowsPerPage,
          page
      } = this.state;
      const columnCount = (+tableHead.length) + (actionColumns.length > 0 ? 1 : 0);
      let data = (this.props.pagination) ? tableData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) : tableData;
      const containerStyle = (customWidth) ? {
        width: customWidth,
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto'
      } : {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto'
      };

      return (
          <div style={containerStyle}>
              <Table className={classes.table}>
                  {tableHead !== undefined ? (
                      <TableHead className={classes[tableHeaderColor + 'TableHeader']}>
                          <TableRow>
                              {tableHead.map((prop, key) => {
                                  return (
                                      <TableCell
                                          className={classes.tableCell + ' ' + classes.tableHeadCell}
                                          key={key}
                                      >
                                          {prop.label}
                                      </TableCell>
                                  );
                              })}
                              {actionColumns.length > 0 && (
                                  <TableCell
                                      className={
                                          classes.tableCell +
                                          ' ' +
                                          classes.tableHeadCell
                                      }
                                  >
                                      Action
                                  </TableCell>
                              )}
                          </TableRow>
                      </TableHead>
                  ) : null}
                  <TableBody>
                      {data.map((prop, index) => {
                          return (
                              <TableRow key={index} style={{...this.getColor(index, prop)}}>

                                  {tableHead.map((item, key) => {
                                      return (
                                          <TableCell className={ classes.tableCell} key={key}>

                                              {this.getValue(prop,item, index)}
                                          </TableCell>
                                      );
                                  })}
                                  <ActionCell actionColumns={actionColumns} rowData={prop} />
                              </TableRow>
                          );
                      })}
                      {(this.props.summary.length > 0) && this.props.summary.map((prop)=>{
                          return (
                              <TableRow key={prop.key}>
                                  {
                                      tableHead.map((col,index)=>{
                                          if(index === 0){
                                              return <TableCell key={index} className={ classes.tableCell} style={{width: '10%'}}>{prop.name}</TableCell>;
                                          }
                                          else if(prop.column_index.includes(index)){
                                              let values = data.reduce((total,val)=>{
                                                  return total+= (+val[col.key]);
                                              },0)
                                              if(prop.operation === 'mean' && data.length > 0){
                                                  values = values/data.length;
                                              }
                                              return <TableCell key={index} className={ classes.tableCell}>{values}</TableCell>;
                                          }else{
                                              return <TableCell key={index} className={ classes.tableCell}/>;
                                          }
                                      })
                                  }
                              </TableRow>
                          )})
                      }
                  </TableBody>

                  {
                      (this.props.pagination && data.length > 0) && (
                          <TableFooter>
                              <TableRow>
                                  <TablePagination
                                      rowsPerPageOptions={[15, 25, 50]}
                                      colSpan={columnCount}
                                      count={this.props.tableData.length}
                                      rowsPerPage={rowsPerPage}
                                      page={page}
                                      onChangePage={this.handleChangePage}
                                      onChangeRowsPerPage={this.handleChangeRowPerPage}
                                      ActionsComponent={TablePaginationActionsWrapped}
                                  />
                              </TableRow>
                          </TableFooter>
                      )
                  }
              </Table>
              {
                  (data.length === 0) && (
                      <div style={{textAlign:'center', marginTop:'20px', fontSize:'1.5em', height:100,display:'flex', justifyContent:'center',alignItems:'center', color:'#a8a8a8'}}>Data Kosong</div>
                  )
              }
          </div>
      );
  }
}

CustomTable.defaultProps = {
  tableHeaderColor: 'gray',
  actionColumns: [],
    tableData :[],
    pagination : true,
    stripped : false,
    group : false,
    customWidth: '',
    summary : [],
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    'warning',
    'primary',
    'danger',
    'success',
    'info',
    'rose',
    'gray'
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.object),
  tableData: PropTypes.arrayOf(PropTypes.object),

};

export default withStyles(tableStyle, { withTheme: true })(CustomTable);
