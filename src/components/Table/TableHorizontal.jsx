import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import tableStyle from './tableStyle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
import {startCase,get} from 'lodash';
import classNames from 'classnames';
import * as numeral from 'numeral';
import {Decimal} from 'decimal.js';

export class TableHorizontal extends  React.Component{

	constructor(props){
		super(props);
	}

	getColor = (index)=>{
	    if(this.props.stripped){
	        return { background: index % 2 ? '#f0f0f0' : 'white' };
	    }
	    return {};
	};

 	getValue = (data)=>{
      let value = data.value;
      if(data.type === 'autofill_operator'){
              let val = data.formula.reduce((total, val,index)=>{
                  const n1 = +(this.props.data.find(it=>it.key === val.key).value) || 0
                  return total[val.operator](new Decimal(n1));
              },new Decimal(0));
              return val.toFixed((data.fixed > -1) ? data.fixed : 2);
      }
      if(data.type === 'money'){
          return 'Rp. '+numeral(value).format('0,0');
      }
      if(data.key === 'created_at' || data.key === 'survei_date' || data.type === 'date'){
          return moment(value).format('dddd, DD-MM-YYYY');
      }
      if(data.key === 'info_source' || data.key === 'status' || data.key.includes('status') || data.key === 'gender'){
          return startCase(value);
      }
      return value;
  };

  render(){
      const {
          classes,
          caption
      } = this.props;

      return (
          <div className={classes.tableResponsive}>

              <Table className={classes.table}>
                  <TableBody>
                      {this.props.data.map((prop, key) => {
                          let labelClass = [classes.tableCell];
                          let valueClass = [classes.tableCell];
                          if(prop.labelBold){
                            labelClass.push(classes.bold);
                          }
                          if(prop.valueBold){
                            valueClass.push(classes.bold);
                          }
                          if(prop.valueColor){
                            valueClass.push(classes[`${prop.valueColor}TableHeader`]);
                          }
                          return (
                              <TableRow key={key} style={{...this.getColor(key)}}>
                                  {/*tableHead.map((item, key) => {
                                      return (
                                          <TableCell className={classes.tableCell} key={key}>

                                              {this.getValue(prop,item)}
                                          </TableCell>
                                      );
                                  })*/}
                                  <TableCell className={classNames(...labelClass)} key={key+'-label'}>
                                      {prop.label}
                                  </TableCell>
                                  <TableCell className={classNames(...valueClass)} >
                                      {this.getValue(prop)}
                                  </TableCell>
                              </TableRow>
                          );
                      })}
                  </TableBody>
              </Table>
          </div>
      );
  }
}
TableHorizontal.defaultProps = {
	data : [
		{
			key : 'test',
			label : 'Test',
			value : '123'
		},
		{
			key : 'test22',
			label : 'marco',
			value : 'polo'
		}
	],
  caption : false
}
export default withStyles(tableStyle)(TableHorizontal)
