import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import tableStyle from './tableStyle';
import {get} from 'lodash'
const getColor= ({Color,SecondaryColor,useSecondaryColor},data)=>{
  if(!SecondaryColor){
    return Color
  }
  else if(Array.isArray(useSecondaryColor) && useSecondaryColor.length >0){
    return !!data[useSecondaryColor[0]] == useSecondaryColor[1] ? SecondaryColor : Color;
  }
  else{
    return Color;
  }
};

const ActionCell = ({ actionColumns, classes, rowData }) => {
  let cell = null;
  if (actionColumns.length > 0) {
    cell = (
      <TableCell className={classNames(classes.tableCell)}>
        {actionColumns.filter(action=>{
          if(action.show_if_true && action.show_if_true.length > 0){
            const result = action.show_if_true.map(key=> !!rowData[key]);
            return result.indexOf(false) === -1
          } else {
            return true;
          }
        }).map((obj, key) => (
          <Tooltip placement="top" key={key} title={obj.Tooltip}>
            <IconButton onClick={() => obj.Callback(rowData)}>
              <obj.Icon
                className={classNames(
                  classes[`${getColor(obj,rowData)}TableHeader`],
                  classes.tableActionButtonIcon
                )}
              />
            </IconButton>
          </Tooltip>
        ))}
      </TableCell>
    );
  }
  return cell;
};
ActionCell.defaultProps = {
  actionColumns: []
};
ActionCell.propTypes = {
  actionColumns: PropTypes.arrayOf(
    PropTypes.shape({
      Callback: PropTypes.func,
      Icon: PropTypes.any.isRequired,
      Tooltip: PropTypes.string,
      Color: PropTypes.oneOf([
        'warning',
        'primary',
        'danger',
        'success',
        'info',
        'rose',
        'gray'
      ])
    })
  )
};
export default withStyles(tableStyle)(ActionCell);
