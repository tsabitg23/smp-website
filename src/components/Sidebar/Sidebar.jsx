import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from '@material-ui/core/Collapse';

// core components
import HeaderLinks from "../Header/HeaderLinks.jsx";
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import sidebarStyle from "./sidebarStyle.jsx";
import {inject, observer} from 'mobx-react';
import Select from '@material-ui/core/Select';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import {uniq,upperCase} from 'lodash';

@inject('appstate')
@observer 
class Sidebar extends React.Component{ 

  constructor(props){
    super(props);
    this.project = props.appstate.project;
    let objGroup = {}
    let group = uniq(props.routes.map(it=>it.role_key+"_open"));
    group.map(it=>{
      Object.assign(objGroup,{[it] : false});
      return it;
    })
    this.state = {
      ...objGroup
    }
  }

  async componentDidMount(){
    this.props.appstate.global_ui.openLoader();
    await this.project.getAll();
    this.props.appstate.global_ui.closeLoader();
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute = (routeName)=>{
    return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
  }

  collapseMenu = (key) => {
    this.setState(state => ({ [key]: !state[key] }));
  };

  render(){
  const { classes, color, logo, image, logoText, routes } = this.props;
  let grouped_routes = [];
  routes.map(prop=>{
    let indexGroup = grouped_routes.findIndex(group=>group.name === prop.role_key);
    if(indexGroup > -1){
      grouped_routes[indexGroup].routes.push(prop)
    }
    else{
      if(prop.role_key){
        grouped_routes.push({
          name : prop.role_key,
          routes : [prop]
        })
      }
    }
  });
  const links = (
    <List className={classes.list}>
      {routes.filter(it=>!it.role_key).map((prop, key) => {
        if (prop.redirect) return null;
        let activePro = " ";
        let listItemClasses;
        if (prop.path === "/upgrade-to-pro") {
          activePro = classes.activePro + " ";
          listItemClasses = classNames({
            [" " + classes[color]]: true
          });
        } else {
          listItemClasses = classNames({
            [" " + classes[color]]: this.activeRoute(prop.path)
          });
        }
        const whiteFontClasses = classNames({
          [" " + classes.whiteFont]: this.activeRoute(prop.path)
        });
        return (
          <NavLink
            to={prop.path}
            className={activePro + classes.item}
            activeClassName="active"
            key={key}
          >
            <ListItem button className={classes.itemLink + listItemClasses}>
              <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                <prop.icon />
              </ListItemIcon>
              <ListItemText
                primary={prop.sidebarName}
                className={classes.itemText + whiteFontClasses}
                disableTypography={true}
              />
            </ListItem>
          </NavLink>
        );
      })}
      {
        grouped_routes.map((group,key)=>(
          <div key={key}>
            <ListItem button className={classes.itemLink+" "+classes.flex} onClick={()=>this.collapseMenu(group.name+"_open")}>
              <ListItemText
                primary={upperCase(group.name)}
                className={classes.itemText}
                disableTypography={true}
              />
              <ListItemIcon className={classes.itemIcon + " " + classes.flexIcon}>
                {this.state[group.name+"_open"] ? <KeyboardArrowUp/> : <KeyboardArrowDown/>}
              </ListItemIcon>
            </ListItem>
            <Collapse in={this.state[group.name+"_open"]}>
              {group.routes.map((prop, key) => {
                if (prop.redirect) return null;
                let activePro = " ";
                let listItemClasses;
                if (prop.path === "/upgrade-to-pro") {
                  activePro = classes.activePro + " ";
                  listItemClasses = classNames({
                    [" " + classes[color]]: true
                  });
                } else {
                  listItemClasses = classNames({
                    [" " + classes[color]]: this.activeRoute(prop.path)
                  });
                }
                const whiteFontClasses = classNames({
                  [" " + classes.whiteFont]: this.activeRoute(prop.path)
                });
                return (
                  <NavLink
                    to={prop.path}
                    className={activePro + classes.item}
                    activeClassName="active"
                    key={key}
                  >
                    <ListItem button className={classes.itemLink + listItemClasses}>
                      <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                        <prop.icon />
                      </ListItemIcon>
                      <ListItemText
                        primary={prop.sidebarName}
                        className={classes.itemText + whiteFontClasses}
                        disableTypography={true}
                      />
                    </ListItem>
                  </NavLink>
                );
              })}
            </Collapse>
          </div>
        ))
      }
    </List>
  );
  const brand = (
    <div className={classes.logo}>
      {/*<a href="" className={classes.logoLink}>
        <div className={classes.logoImage}>
          <img src={logo} alt="logo" className={classes.img} />
        </div>
        {logoText}
      </a>*/}
        <div className={classes.logoContainer}>
        <div className={classes.logoImage}>
          <img src={logo} alt="logo" className={classes.img} />
        </div>
        <span style={{color : '#FFF',fontWeight : 'bold'}}>{logoText}</span>
        </div>
        <div className={classes.project_select}>
          <TextField
            id={'select-project'}
            select
            label={'Project'}
            value={this.project.selectedProject}
            onChange={(e)=>this.project.setProject(e.target.value)}
            margin="normal"
            fullWidth
            type={'select'}
            SelectProps={{
                    MenuProps: {
                        className: classes.menu,
                    },
                    displayEmpty : true
                  }}
              >
                {
                  this.project.data.map(option => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))
                }
                {
                  this.project.data.length === 0 && (
                    <MenuItem key={'not-found'} disabled value={'not-found'}>
                      No project found
                    </MenuItem>
                  )
                }
            </TextField>
        </div>

    </div>
  );
  return (
    <div>
      <Hidden mdUp>
        <Drawer
          variant="temporary"
          anchor="right"
          open={this.props.open}
          classes={{
            paper: classes.drawerPaper
          }}
          onClose={this.props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>
            <HeaderLinks />
            {links}
          </div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown>
        <Drawer
          anchor="left"
          variant="permanent"
          open
          classes={{
            paper: classes.drawerPaper
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  );
};
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(sidebarStyle)(Sidebar);
