import {
  warningColor,
  primaryColor,
  dangerColor,
  successColor,
  infoColor,
  roseColor,
  grayColor,
  defaultFont
} from '../../assets/jss/material-dashboard-react.jsx';

const userStyles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    textTransform : 'none'
  },
});

export default userStyles;
