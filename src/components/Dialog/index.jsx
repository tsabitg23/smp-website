import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import DialogContent from '@material-ui/core/DialogContent';
import FormBuilder from '../FormBuilder';
import moment from 'moment';
import {get} from 'lodash';
import schema from 'async-validator';

class GenericDialog extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			open : this.props.open,
			formData : Object.assign({},this.props.value),
			formErrors : []
		};
	}



	componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    let change = {};
    if (this.props.open !== prevProps.open) {
      change.open = this.props.open;
    }

    if (this.props.title !== prevProps.title) {
      change.title = this.props.title;
    }

    if(Object.keys(change).length > 0){
        this.setState(change);
    }
	}

	Transition(props) {
        return <Slide direction="up" {...props} />;
	}

	handleClose = () => {
		this.props.onClose();
	};

	handleSave = ()=>{
		let rules = {};


		this.props.forms.map(it=>{
			let type = 'string';
			if(it.type === 'multiline' || it.type === 'password' || it.type === 'select' || it.type === 'autocomplete' || it.type === 'text' || it.type === 'number' || it.type === 'date'){
				type = 'string';
			}
			else if(it.type === 'autocomplete_multi'){
				type = 'array';
			}
			else if(it.type === 'percentage'){
				type = 'number';
			} else if(it.type === 'time'){
				type = 'string';
			} else if(it.type.includes('autofill')){
				return;
			}
			else{
				type = it.type;
			}

			rules[it.key] = [
				{
					required : get(it,'required',true),
					message : `Enter valid ${it.label}${it.type ==='password' ? ', atleast 6 character' : ''}`,
					type : type
				}
			];
			if(it.type === 'password'){
				rules[it.key][0].min = 6;
			}

			if(it.type === 'percentage'){
				rules[it.key][0].min = 0;
				rules[it.key][0].max = 100;
			}
		});

		this.props.forms.map(prop=>{
			if(prop.type === 'time'){
				const isMoment = moment.isMoment(this.state.formData[prop.key]);
				if(isMoment){
					this.state.formData[prop.key] = this.state.formData[prop.key].format('HH:mm');
				} else {
					this.state.formData[prop.key] = moment().format('HH:mm');
				}
			}
		});

		console.log(this.state.formData, rules,'~~~~~~~');

		const validator = new schema(rules);
		validator.validate(this.state.formData, (errs, f) => {
			this.setState({formErrors : errs ? errs : []});
			if (errs) {
                console.log(errs);
            } else {
				const data = this.state.formData;
				Object.keys(data).map(it=>{
					if(data[it] == 'Invalid date'){
						data[it] = null;
					}
				})
                this.props.onSave(this.state.formData);
            }
		});
	};

	onFormUpdate = (formData)=>{
		this.setState({formData});
	};

	render(){
		const { classes } = this.props;

		return (
			<Dialog
                fullScreen
                open={this.state.open}
                onClose={this.handleClose}
                TransitionComponent={this.Transition}
                scroll={'paper'}
            >
              <AppBar className={classes.appBar}>
                <Toolbar>
                  <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                    <CloseIcon />
                  </IconButton>
                  <Typography variant="h6" color="inherit" className={classes.flex}>
                    {this.props.title}
                  </Typography>
					{
						!this.props.hidesafebutton && (
							<Button color="inherit" onClick={this.handleSave}>
								save
							</Button>
						)
					}
                </Toolbar>
              </AppBar>
              <DialogContent>
                <FormBuilder forms={this.props.forms} value={this.props.value} onFormUpdate={this.onFormUpdate} formErrors={this.state.formErrors}/>
                  {this.props.children}
              </DialogContent>
            </Dialog>
		);
	}
}
GenericDialog.defaultProps = {
  forms: [],
  value : {},
	hidesafebutton: false
};
export default withStyles(styles)(GenericDialog);
