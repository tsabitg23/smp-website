import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import DialogContent from '@material-ui/core/DialogContent';

class DialogOnly extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			open : this.props.open
		};
	}

	

	componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    let change = {};
    if (this.props.open !== prevProps.open) {
      change.open = this.props.open;
    }

    if (this.props.title !== prevProps.title) {
      change.title = this.props.title;
    }

    if(Object.keys(change).length > 0){
        this.setState(change);
    }
	}

	Transition(props) {
        return <Slide direction="up" {...props} />;
	}

	handleClose = () => {
		this.props.onClose();
	};

	render(){
		const { classes } = this.props;

		return (
			<Dialog
                fullScreen
                open={this.state.open}
                onClose={this.handleClose}
                TransitionComponent={this.Transition}
                scroll={'paper'}
            >
              <AppBar className={classes.appBar}>
                <Toolbar>
                  <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                    <CloseIcon />
                  </IconButton>
                  <Typography variant="h6" color="inherit" className={classes.flex}>
                    {this.props.title}
                  </Typography>
                  {this.props.actions.length > 0 &&
                    this.props.actions.map((it,index)=>(
                        <div key={index}>
                          {it}
                        </div>
                      ))
                  }
                </Toolbar>
              </AppBar>
              <DialogContent>
                  {this.props.children}
              </DialogContent>
            </Dialog>
		);
	}
}

DialogOnly.defaultProps = {
	actions : []
};

export default withStyles(styles)(DialogOnly);