import React from 'react';
import { DateRangePicker } from 'material-date-range-picker';
import {Button, Grid} from "@material-ui/core";
import Card from "../Card/Card";
import GridItem from "../Grid/GridItem";
import {clone} from 'lodash';
import * as moment from 'moment';
export class FilterData extends React.Component{
    state = {
        fromDate: null,
        toDate: null
    };

    _handleDateRangeChange = update => {
        this.setState(update);
    };

    sendData = () => {
        const data = clone(this.state);
        if(data.fromDate && data.toDate){
            data.start_date = moment(data.fromDate).format('YYYY-MM-DD');
            data.end_date = moment(data.toDate).format('YYYY-MM-DD');
        }
        delete data.fromDate;
        delete data.toDate;
        this.props.onFilter(data);
    }

    renderFilter = (prop) => {
        if(prop.type === 'range'){
            return (
                <div style={{width:'320px', padding: '20px'}}>
                    {/* 3. Add the material date range picker in your project */}
                    <DateRangePicker
                        fromDate={this.state.fromDate}
                        toDate={this.state.toDate}
                        onChange={this._handleDateRangeChange}
                        closeDialogOnSelection={false}
                    />
                </div>
            )
        } else {
            return <div/>
        }
    }

    render(){
        if(this.props.data.length === 0){
            return (
                <div/>
            )
        }
        return (
            <Grid container>
                <GridItem xs={12} sm={10} md={10}>
                    {
                        this.props.data.map(prop=>{
                            return (
                                <div>
                                    {this.renderFilter(prop)}
                                </div>
                            )
                        })
                    }
                </GridItem>
                <GridItem xs={12} sm={2} md={2}>
                    <div style={{padding:'20px', display:'flex', justifyContent:'flex-end'}}>
                        <Button onClick={this.sendData} variant={"contained"} color={"primary"}>Apply</Button>
                    </div>
                </GridItem>
            </Grid>
        )
    }
}

FilterData.defaultProps = {
    data : [],
    onFilter : ()=>{}
}