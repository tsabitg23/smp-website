import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from '../Grid/GridItem.jsx';
import Table from '../Table/index.jsx';
import Card from '../Card/Card.jsx';
import CardHeader from '../Card/CardHeader.jsx';
import CardBody from '../Card/CardBody.jsx';
import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import {FilterData} from "./Filter";

const styles = {
    cardCategoryWhite: {
        '&,& a,& a:hover,& a:focus': {
            color: 'rgba(255,255,255,.62)',
            margin: '0',
            fontSize: '14px',
            marginTop: '0',
            marginBottom: '0'
        },
        '& a,& a:hover,& a:focus': {
            color: '#FFFFFF'
        }
    },
    cardTitleWhite: {
        color: '#FFFFFF',
        marginTop: '0px',
        minHeight: 'auto',
        fontWeight: '300',
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: '3px',
        textDecoration: 'none',
        '& small': {
            color: '#777',
            fontSize: '65%',
            fontWeight: '400',
            lineHeight: '1'
        }
    }
};

export class GenericPage extends React.Component{
    render(){
        const { classes } = this.props;
        const onEditClick = rowData => {
            alert(JSON.stringify(rowData));
        };
        const onAddClick = rowData => {
            alert(JSON.stringify(rowData));
        };
        const configActionColumns = [
            { Icon: Add, Tooltip: 'Add', Color: 'success', Callback: onAddClick },
            { Icon: Edit, Tooltip: 'Edit', Color: 'primary', Callback: onEditClick }
        ];
        return (
            <Grid container>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="primary">
                            <Grid container>
                                <GridItem xs={this.props.tools.length > 0 ? 6 : 12}>
                                    <h4 className={classes.cardTitleWhite}>{this.props.title}</h4>
                                    <p className={classes.cardCategoryWhite}>
                                        {this.props.subtitle}
                                    </p>
                                </GridItem>
                                {
                                    this.props.tools.length > 0 && (
                                        <GridItem xs={6} style={{display: 'flex',justifyContent : 'flex-end'}}>
                                            {this.props.tools.map((component,index)=>{
                                                return (
                                                    <div key={index} style={{marginRight : '10px'}}>
                                                        {component}
                                                    </div>
                                                );
                                            })}                         
                                        </GridItem>
                                    )
                                }
                            </Grid>
                        </CardHeader>
                        <FilterData data={this.props.filter} onFilter={this.props.onFilter}/>
                        <CardBody style={{overflowX : 'scroll'}}>
                            <Table
                                actionColumns={this.props.actionColumn}
                                tableHeaderColor="primary"
                                tableHead={this.props.headerColumn}
                                tableData={this.props.data}
                                customWidth={this.props.customWidth}
                            />
                        </CardBody>
                    </Card>
                </GridItem>
            </Grid>
        );
    }

}

GenericPage.defaultProps = {
  tools: [],
};

export default withStyles(styles)(GenericPage);
