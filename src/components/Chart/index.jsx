import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
// import dashboardStyle from '../../views/Dashboard/dashboardStyle.jsx';
import Grid from '@material-ui/core/Grid';
import Card from '../Card/Card.jsx';
import CardHeader from '../Card/CardHeader.jsx';
import CardFooter from '../Card/CardFooter.jsx';
import GridItem from '../Grid/GridItem.jsx';
import Icon from '@material-ui/core/Icon';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';

import {
    PieChart,
    Pie,
    Tooltip,
    Cell,
    ResponsiveContainer,
    BarChart, Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Legend,
 } from 'Recharts';

const dashboardStyle = theme => ({
  chartGrid : {
    marginBottom : 20,
    paddingRight: 20
  },
  chartLabel : {
    alignItems:'center'
  },
  formControlChart: {
    // margin: theme.spacing.unit,
    minWidth:120,
    width: '100%'
  },
});

class DefaultChart extends React.Component{
  constructor(props){
    super(props);
    const defaultExtraFilter = {};
    props.extraFilter.map(filter=>{
      defaultExtraFilter[filter.key] = "";
    })
    this.state = {
      numberOfMonth : 4,
      ...defaultExtraFilter
    }
  }

  handleChange = (key, val, onSelected) => {
    this.setState({
      [key] : val
    },()=>{
      let closeLoading = true;
      if(onSelected){
        onSelected(val);
        closeLoading = false;
      }
      this.props.changeFilter(this.state, closeLoading)
    })

  }

  renderFilter = () => {
    return (
      <Grid container spacing={3}>
          {
              this.props.showMonth && (
                  <Grid item xs={12} className={this.props.classes.chartGrid}>
                      <FormControl>
                          <InputLabel id="demo-simple-select-helper-label">Jumlah Bulan</InputLabel>
                          <Select
                              labelId="demo-simple-select-helper-label"
                              id="demo-simple-select-helper"
                              value={this.state.numberOfMonth}
                              onChange={(e)=>this.handleChange('numberOfMonth', e.target.value)}
                          >
                              <MenuItem value={3}>3</MenuItem>
                              <MenuItem value={4}>4</MenuItem>
                              <MenuItem value={5}>5</MenuItem>
                              <MenuItem value={6}>6</MenuItem>
                          </Select>
                          <FormHelperText style={{marginTop:5}}>Tampilkan data jumlah bulan terakhir</FormHelperText>
                      </FormControl>
                  </Grid>
              )
          }

        {
          this.props.extraFilter.map(prop=>{
            const isDisabled =  false
            return (
              <Grid item xs={6} className={this.props.classes.chartGrid}>
                <FormControl className={this.props.classes.formControlChart} disabled={isDisabled}>
                  <InputLabel id="demo-simple-select-helper-label">{prop.label}</InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={this.state[prop.key]}
                    onChange={(e)=>this.handleChange(prop.key, e.target.value,prop.onSelected)}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {prop.option.map(it=>{
                      return (
                        <MenuItem value={it.value}>{it.label}</MenuItem>
                      )
                    })}
                  </Select>
                  {prop.helperText && (<FormHelperText style={{marginTop:5}}>{prop.helperText}</FormHelperText>)}
                </FormControl>
              </Grid>
            )
          })
        }
      </Grid>
    )
  }
	renderGraph = ()=>{
	  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042','#8884d8','#82ca9d'];
	  const { classes } = this.props;

	//   let dataPie = toJS(prop.value)
	//   dataPie.map(it=>{
	//     it.value = parseInt(it.value);
	//     it.name = startCase(it.name)
	//     return it;
    //   })
    const data = this.props.data
      if(data.length === 0){
          return <div/>;
      }
	  return (
	    <GridItem xs={12} sm={12} md={12}>
	       <Card chart>
	        <CardHeader>
            {
              this.renderFilter()
            }
	          <ResponsiveContainer  width={'100%'} height={200}>
                <BarChart
                    width={500}
                    height={300}
                    data={data}
                    margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {
                        Object.keys(data[0]).filter(prop=>prop!== 'name').map((prop, index)=>{
                            return (
                                <Bar dataKey={prop} fill={COLORS[index]} />
                            )
                        })
                    }
                </BarChart>
	            </ResponsiveContainer>
	        </CardHeader>
	        {/* <CardBody>
	          <h4 className={classes.cardTitle}>BRUH</h4>
	        </CardBody> */}
          {
            this.props.children
          }
	        <CardFooter chart>
	          <div className={classes.stats}>
	            <Icon className={classes.iconSub}>show_chart</Icon> {" "+this.props.subtitle}
	          </div>
	        </CardFooter>
	      </Card>
	    </GridItem>
	  )
	};


	render(){
		return (
				<Grid container>
          {this.renderGraph()}
        </Grid>
		)
	}
}
DefaultChart.defaultProps = {
    data : [],
    subtitle : '',
    changeFilter : ()=>{},
    extraFilter : [],
    showMonth: true
}
export default withStyles(dashboardStyle)(DefaultChart)

