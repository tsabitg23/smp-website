import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Card from '../Card/Card.jsx';
import CardHeader from '../Card/CardHeader.jsx';
import CardFooter from '../Card/CardFooter.jsx';
import GridItem from '../Grid/GridItem.jsx';
import Icon from '@material-ui/core/Icon';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';

import { 
    PieChart, 
    Pie,
    Tooltip,
    Cell,
    ResponsiveContainer,
    BarChart, Bar,  
    XAxis, 
    YAxis, 
    CartesianGrid, 
    Legend,
 } from 'Recharts';

const style = theme => ({
  chartGrid : {
    marginBottom : 20,
    paddingRight: 20
  },
  chartLabel : {
    alignItems:'center'
  },
  formControlChart: {
    // margin: theme.spacing.unit,
    minWidth:120,
    width: '100%'
  },
});

class StackedBar extends React.Component{
  constructor(props){
    super(props);
    const defaultExtraFilter = {};
    props.extraFilter.map(filter=>{
      defaultExtraFilter[filter.key] = "";
    })
    this.state = {
      numberOfMonth : 4,
      ...defaultExtraFilter
    }
  }

  handleChange = (key, val, onSelected) => {
    this.setState({
      [key] : val
    },()=>{
      let closeLoading = true;
      if(onSelected){
        onSelected(val);
        closeLoading = false;
      }
      this.props.changeFilter(this.state, closeLoading)
    })
    
  }
  
	render(){
    const COLORS = ['#e53935','#e57373', '#8e24aa', '#ba68c8', '#0d47a1', '#42a5f5','#00695c', '#4db6ac', '#2e7d32', '#66bb6a', '#f57f17', '#ffee58','#3e2723', '#795548', '#263238', '#607d8b'];
    return (
			<div>
                <ResponsiveContainer  width={'100%'} height={200}>
                <BarChart
                    width={500}
                    height={300}
                    data={this.props.data}
                    margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    {/* <Legend /> */}

                    {
                        this.props.data.slice(0,1).map((bars, index)=>{
                          return Object.keys(bars).filter(prop=>prop!== 'name').map((bar, index)=>{
                            const stackId = bar.split('-');
                            return (
                              <Bar stackId={`${stackId[0]}`} dataKey={bar} fill={COLORS[index]}/>
                            )
                          })
                        })
                    }
                    {/* <Bar stackId="pv" dataKey="booking" fill={COLORS[1]}/>
                    <Bar stackId="db" dataKey="akad" fill={COLORS[0]}/>
                    <Bar stackId="db" dataKey="booking" fill={COLORS[1]}/> */}
                </BarChart>
	            </ResponsiveContainer>
            </div>
		)
	}
}
StackedBar.defaultProps = {
    data : [],
    subtitle : '',
    changeFilter : ()=>{},
    extraFilter : []
}
export default withStyles(style)(StackedBar)

